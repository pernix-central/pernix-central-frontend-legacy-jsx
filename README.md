# Pernix Central Frontend
Pernix Central Description

### Running the app
**Install dependencies**
```shell
yarn install
```

**Run the app**
```shell
yarn start
```

Go to [http://localhost:3000](http://localhost:3000)

### Storybook
If you wan to see all the components in the application, you can load the storybook:
```shell
$ yarn storybook
```

Go to [http://localhost:9009](http://localhost:9009)

### Git Management
0. Create a new branch based off of develop: `git checkout -b [story-number]-name-of-the-story`. Example: `54312-application-header`
0. Every commit should have the story number, like `[54312] Added button component`
0. Create a Pull Request and add the link of the story to the Pull Request description

### Releasing
Deployments will happen automatically when we use Github:

- `develop` updates will release to `staging`
- `master` updates will release to `production`

Both apps will run on Heroku, request for access if needed.

*In the meantime we should release manually*

**NEVER EVER** push directly to either staging or production.
