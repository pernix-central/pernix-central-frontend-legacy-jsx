import React from 'react'
import { Router } from 'react-router-dom'
import ApplicationRouter from './routers/ApplicationRouter'
import { history } from './helpers/history'
import Footer from './components/partials/Footer'
import Header from './components/partials/Header'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import { createMuiTheme, withStyles } from '@material-ui/core/styles'
import commonStyles, { PERNIX_GREEN, PERNIX_BLUE, PERNIX_ORANGE, ERROR } from './assets/styles/commonStyles'

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat'
  },
  overrides: {
    MuiAppBar: {
      root: {
        fontFamily: 'Montserrat'
      }
    }
  },
  palette: {
    primary: {
      main: PERNIX_GREEN
    },
    secondary: {
      main: PERNIX_BLUE
    },
    ternary: {
      main: PERNIX_ORANGE
    },
    danger: {
      main: ERROR
    },

  },
})

class Application extends React.Component {

  render() {
    return (
      <Router history={history}>
        <MuiThemeProvider theme={theme}>
          <Header/>
          <div>
            <ApplicationRouter />
          </div>
          <Footer/>
        </MuiThemeProvider>
      </Router>
    )
  }
}

export default withStyles(commonStyles)(Application)
