import React from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import setupStore from './redux/setupStore'
import Application from './Application'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/lib/integration/react'
import registerServiceWorker from './helpers/registerServiceWorker';

const store = setupStore()
let persistor = persistStore(store)

class RootContainer extends React.Component {
  render() {
    return (
      <PersistGate persistor={persistor}>
        <Provider store={store}>
          <Application/>
        </Provider>
      </PersistGate>
    )
  }
}

export default RootContainer

ReactDOM.render(<RootContainer/>, document.getElementById('root'));
registerServiceWorker();
