import axios from 'axios'
import humps from 'humps'

export function getAllActivitiesRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'activities', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function createActivityRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'activities',
    data: humps.decamelizeKeys(params.newActivity),
    headers: {
      Authorization: params.token
    }
  }).then((response) => response.request.status)
}

export function editActivityRequest(params) {
  const { editedActivity } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'activities/'+ editedActivity.activity.id,
    data: humps.decamelizeKeys(editedActivity),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}

export function getActivityRequest(params) {
  return axios.get( process.env.REACT_APP_API_URL + 'activities/' + params.activityId,{
      headers: {
      Authorization: params.authToken
    }
  }).then((response) => humps.camelizeKeys(response.data))
}
