import axios from 'axios'
import humps from 'humps'

export function getAllProjectsRequest(authToken) {
  return axios.get(process.env.REACT_APP_API_URL + 'projects', {
    headers: {
      Authorization: authToken,
      contentType: 'application/json'
    }
  })
    .then(response => humps.camelizeKeys(response.data))
}

export function getProjectRequest(params) {
  return axios.get(process.env.REACT_APP_API_URL + 'projects/' + params.projectId, {
    headers: {
      Authorization: params.authToken
    }
  }).then((response) => humps.camelizeKeys(response.data))
}

export function createProjectRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'projects',
    data: humps.decamelizeKeys(params.newProject),
    headers: {
      Authorization: params.token
    }
  }).then((response) => response.request.status)
}

export function editProjectRequest(params) {
  const { editedProject } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'projects/' + editedProject.id,
    data: humps.decamelizeKeys(editedProject),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}
