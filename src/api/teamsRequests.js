import axios from 'axios'
import humps from 'humps'

export function getTeamRequest(params) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'teams/' + params.teamId, {
      headers: {
        Authorization: params.authToken,
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function getAllTeamsRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'teams', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function editTeamRequest(params) {
  const { editedTeam } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'teams/' + editedTeam.team.id,
    data: humps.decamelizeKeys(editedTeam),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}

export function createTeamRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'teams',
    data: humps.decamelizeKeys(params.teamData),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}

export function destroyTeamRequest(params) {
  const { teamToDelete } = params
  return axios({
    method: 'delete',
    url: process.env.REACT_APP_API_URL + 'teams/' + teamToDelete.id,
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.data)
}
