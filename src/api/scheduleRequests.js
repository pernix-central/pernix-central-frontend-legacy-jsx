import axios from 'axios'
import humps from 'humps'

export function getScheduleRequest(params) {
  const { scheduleId, employeeId } = params
  return axios.get(
    process.env.REACT_APP_API_URL + `employees/${employeeId}/schedules/${scheduleId}`, {
      headers: {
        Authorization: params.authToken,
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function editScheduleRequest(params) {
  const { editedSchedule, employeeId } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + `employees/${employeeId}/schedules/${editedSchedule.id}`,
    data: humps.decamelizeKeys(editedSchedule),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}
