import axios from 'axios'
import humps from 'humps'

export function loginRequest(credentials) {
  return axios.post(
    process.env.REACT_APP_API_URL + 'auth/login', credentials).then((response) => {
      return {
        authToken: response.data.auth_token,
        user: humps.camelizeKeys(response.data.employee)
      }
    });
}
