import axios from 'axios'
import humps from 'humps'

export function createSkillRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'skills',
    data: humps.decamelizeKeys(params.newSkill),
    headers: {
      Authorization: params.token
    } 
  }).then((response) => response.request.status)
}

export function getAllSkillsRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + '/skills', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function getSkillRequest(params) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'skills/' + params.skillId, {
      headers: {
        Authorization: params.authToken,
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function editSkillRequest(params) {
  const { editedSkill } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'skills/' + editedSkill.id,
    data: humps.decamelizeKeys(editedSkill),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}