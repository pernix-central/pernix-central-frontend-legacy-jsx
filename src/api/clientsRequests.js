import axios from 'axios'
import humps from 'humps'

export function getAllClientsRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'clients', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function createClientRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'clients',
    data: humps.decamelizeKeys(params.newClient),
    headers: {
      Authorization: params.token
    } 
  }).then((response) => response.request.status)
}

export function editClientRequest(params) {
  const { editedClient } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'clients/'+ editedClient.id,
    data: humps.decamelizeKeys(editedClient),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => response.request.status)
}

export function getClientRequest(params) {
  return axios.get( process.env.REACT_APP_API_URL + 'clients/' + params.clientId,{
      headers: {
      Authorization: params.authToken
    }
  }).then((response) => humps.camelizeKeys(response.data))
}
