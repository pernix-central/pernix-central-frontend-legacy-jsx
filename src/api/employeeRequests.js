import axios from 'axios'
import humps from 'humps'

export function getEmployeeRequest(params) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'employees/' + params.employeeId, {
      headers: {
        Authorization: params.authToken,
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function getAllEmployeesRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'employees', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function editEmployeeRequest(params) {
  const { editedEmployee, employeeId } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'employees/' + employeeId,
    data: editedEmployee,
    headers: {
      ContentType: "multipart/form-data",
      type: "formData",
      Authorization: params.authToken
    }
  }).then(response => humps.camelizeKeys(response))
}

export function signUpRequest(params) {
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + 'employees',
    data: params.userData,
    headers: {
      ContentType: "multipart/form-data",
      type: "formData",
      Authorization: params.authToken
    }
  }).then((response) => response.request.status)
}

export function changeStatusRequest(params) {
  const { statusChanged } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + 'employees/' + statusChanged.id + '/change_status',
    data: humps.decamelizeKeys(statusChanged),
    headers: {
      Authorization: params.authToken
    }
  }).then(response => humps.camelizeKeys(response.data))
}

export function editEmployeeTcpSkillsRequest(params) {
  const { employeeId, editedTcpSkills, authToken } = params
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + `employees/${employeeId}/update_tcp_skills`,
    data: humps.decamelizeKeys(editedTcpSkills),
    headers: {
      Authorization: authToken
    }
  }).then(response => response.request.status)
}

