import axios from 'axios'
import humps from 'humps'

export function getAllCommentsRequest(authToken) {
  return axios.get(
    process.env.REACT_APP_API_URL + 'projects', {
      headers: {
        Authorization: authToken,
        contentType: 'application/json'
      }
    }
  ).then(response => humps.camelizeKeys(response.data))
}

export function addNewCommentRequest(params) {
  const { newComment: { projectId, teamId }, newComment} = params
  let url = projectId ? `projects/${projectId}` : `teams/${teamId}`
  return axios({
    method: 'post',
    url: process.env.REACT_APP_API_URL + url + '/comments/',
    data: humps.decamelizeKeys({ comment: newComment}),
    headers: {
      Authorization: params.token
    } 
  }).then((response) => response.request.status)
}

export function editCommentsRequest(params) {
  const { comment, projectId, teamId, authToken } = params
  let url = projectId ? `projects/${projectId}` : `teams/${teamId}`
  return axios({
    method: 'put',
    url: process.env.REACT_APP_API_URL + url + '/comments/'+ comment.id,
    data: humps.decamelizeKeys({ comment }),
    headers: {
      Authorization: authToken
    }
  }).then((response) => response.request.status)
}
