import React from 'react'
import { connect } from 'react-redux';
import ProjectForm from '../partials/ProjectForm';
import Container from '../partials/Container'
import { createProject } from '../../redux/modules/projects'
import { getAllClients } from '../../redux/modules/clients'
import { ValidatorForm } from 'react-material-ui-form-validator';
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';
import moment from 'moment'
import { getAllTeams } from '../../redux/modules/teams'

class CreateProjectPage extends React.Component {

  today = new Date().toJSON().slice(0, 10).replace(/-/g, '-');

  state = {
    project: {
      name: '',
      description: '',
      startDate: this.today,
      endDate: this.today,
      status: '',
      team: null,
      clients: [],
    },
    displayAlert: false,
    model: 'project',
    action: 'created',
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps
    if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  onChange = (event) => {
    const { name, value } = event.target
    const { project } = this.state
    this.setState(() => ({
      project: { ...project, [name]: value }
    }))
  }

  onSelect = name => value => {
    const { project } = this.state
    this.setState(() => ({
      project: { ...project, [name]: value }
    }))
  }

  onSubmit = (e) => {
    const { createProject } = this.props
    const { project } = this.state
    const { name, description, startDate, endDate, status, clients, team } = project
    let currentProject = {
      project: {
        name: name,
        description: description,
        startDate: startDate,
        endDate: endDate,
        status: status,
        client_ids: clients ? clients.map(client => client.key) : [],
        teamId: team.value
      }
    }
    e.preventDefault();
    createProject(currentProject)
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/projects')
  }

  componentWillMount() {
    const { clientList, getAllClients } = this.props
    ValidatorForm.addValidationRule('validateEndDate', () => {
      const { project } = this.state
      const { startDate, endDate } = project

      const formatedStartDate = (startDate) ? moment(startDate) : null
      const formatedEndDate = moment(endDate)
      return formatedStartDate ? formatedEndDate > formatedStartDate : formatedEndDate >= new Date()
    });
    if (!clientList) {
      getAllClients();
    }
    const { teamList, getAllTeams } = this.props
    if (!teamList) {
      getAllTeams()
    }
  }

  render() {
    const { teamList } = this.props
    const { project, displayAlert, model, action } = this.state
    const { status, clientList } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title="Add new project"
        body=''>
        <ProjectForm
          project={project}
          changeData={this.onChange}
          selectTeam={this.onSelect('team')}
          selectClient={this.onSelect('clients')}
          sendData={this.onSubmit}
          toReset={this.onReset}
          teams={teamList ? teamList.map((team) =>
            ({
              label: team.name,
              value: team.id,
              key: team.id
            })) : []}
          clientsList={clientList}
        />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createProject,
    getAllTeams,
    getAllClients
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    status: state.projects.status,
    teamList: state.teams.all,
    clientList: state.clients.all,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProjectPage)

