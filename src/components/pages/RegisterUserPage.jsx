import React from 'react'
import { connect } from 'react-redux';
import EmployeeForm from '../partials/EmployeeForm';
import Container from '../partials/Container'
import { registerRequest } from '../../redux/modules/employee'
import { ValidatorForm } from 'react-material-ui-form-validator';
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';
import { insertPayloadInform } from '../../helpers/imagesHelper';

class RegisterUserPage extends React.Component {

  today = new Date().toJSON().slice(0, 10).replace(/-/g, '-');

  state = {
    userData: {
      firstName: '',
      lastName: '',
      birthdate: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      hasCar: false,
      blogUrl: '',
      computerSerialNumber: '',
      macAddress: '',
      hireDate: this.today,
      onVacation: false,
      canBeMentor: false,
      role: '',
      imagesAttributes: null
    },
    imagePerview: null,
    displayAlert: false,
    model:'employee',
    action: 'created',
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps
    if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  onChange = (event) => {
    const { name, value, type, checked, files } = event.target
    const { userData } = this.state
    switch (type) {
      case 'checkbox':
        this.setState(() => ({
          userData: { ...userData, [name]: checked }
        }))
        break;
      case 'file':
        if (files[0]) {
          this.setState(() => ({
            userData: { ...userData, imagesAttributes: files[0] },
            imagePreview: URL.createObjectURL(files[0])
          }))
        }
        break;

      default:
        this.setState(() => ({
          userData: { ...userData, [name]: value }
        }))
        break;
    }

  }

  onSubmit = (e) => {
    const { registerRequest } = this.props
    let { userData} = this.state
    var data = insertPayloadInform(userData, false)
    e.preventDefault();
    registerRequest(data)
  }

  onReset = (e) => {
    this.setState({
      userData: {
        firstName: '',
        lastName: '',
        birthdate: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        hasCar: false,
        blogUrl: '',
        computerSerialNumber: '',
        macAddress: '',
        hireDate: '',
        onVacation: false,
        canBeMentor: false,
        role: '',
      }
    })
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.push('/employees')
  }

  componentWillMount() {
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      return this.state.userData.password === value;
    });
    ValidatorForm.addValidationRule('isValidDate', (value) => {
      var today = new Date()
      var val = new Date(value)
      return (val < today);
    });
  }

  render() {
    const { userData, displayAlert, model, action, imagePreview } = this.state
    const { status } = this.props
    const {title, message, variant} = messageHandler(status, { model, action })
    return (
      <Container
        title="Register user"
        body=''>
        <EmployeeForm
          user={userData}
          changeData={this.onChange}
          sendData={this.onSubmit}
          toReset={this.onReset}
          editable={true}
          imagePreview = {imagePreview}
        />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    registerRequest
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    currentEmployee: state.employee.currentEmployee,
    status: state.employee.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterUserPage)

