import React from 'react'
import {
  withStyles, CardContent, IconButton, Icon, Grid, TextField,
  InputAdornment, Snackbar, Tooltip, Paper
} from '@material-ui/core';
import { connect } from 'react-redux'
import { getAllEmployees } from '../../redux/modules/employee'
import { getActivity, editActivity } from '../../redux/modules/activities'
import { bindActionCreators } from 'redux';
import Label from '../partials/Label';
import SimpleTable from '../partials/SimpleTable'
import ParticipantsTable from '../partials/ParticipantsTable'
import { history } from '../../helpers/history';
import { subtractLists } from '../../helpers/listsHandler';
import activityStyles from '../../assets/styles/activityStyles';
import { messageHandler } from '../../helpers/alertsHandler';
import MessageContent from '../partials/MessageContent';
import ActionsButtons from '../partials/ActionsButtons';
import Container from '../partials/Container';


class ShowActivityPage extends React.Component {

  state = {
    currentActivity: null,
    filter: '',
    employees: null,
    participants: [],
    displayAlert: false,
    willAddParticipants: false,
    model: 'participants',
    action: 'added'
  }

  static getDerivedStateFromProps(props, state) {
    const { currentActivity } = state
    const { getActivity, getAllEmployees, match: { params: { activityId } }, employeeList, status, activity } = props
    if (status) {
      return {
        ...state,
        displayAlert: true,
      }
    }
    if (!employeeList) {
      getAllEmployees();
      return state
    }
    if (!activity) {
      getActivity(activityId);
      return state
    }
    if (activity.id === parseInt(activityId, 10)) {
      if (!currentActivity) {
        return {
          ...state,
          currentActivity: activity,
          employees: employeeList,
        }
      }
      return state
    } else {
      getActivity(activityId)
      return state
    }
  }


  permitAddParticipants = () => {
    this.setState({
      willAddParticipants: true,
    })
  }

  setFilter = (event) => {
    const { value } = event.target
    const { participants } = this.state
    const { employeeList } = this.props
    this.setState({
      filter: value,
      employees: this.filterData(value, subtractLists(participants, employeeList))
    })
  }

  filterData = (filter, employees) => {
    return filter !== '' ? employees.filter(employee =>
      employee.firstName.toLowerCase().includes(filter.toLowerCase())
    ) : (employees ? employees : [])
  }

  addParticipant = (event) => {
    const { value } = event.currentTarget
    const { participants, employees } = this.state
    let index = employees.map(employee => employee.id).indexOf(parseInt(value, 10))
    let employee = employees.splice(index, 1)[0]
    participants.push({
      id: employee.id,
      firstName: employee.firstName,
      lastName: employee.lastName,
      images: employee.images,
      isOwner: false,
    })
    this.setState({
      participants: participants
    })
  }

  removeParticipant = (event) => {
    const { value } = event.currentTarget
    const { participants, employees } = this.state
    var index = participants.map(participant => participant.id).indexOf(parseInt(value, 10))
    employees.push(participants.splice(index, 1)[0])
    this.setState({
      employees: employees
    })
  }

  setIsOwner = (event) => {
    const { checked, value } = event.target
    const { participants } = this.state
    let index = participants.map(participant => participant.id).indexOf(parseInt(value, 10))
    participants[index].isOwner = checked
    this.setState({
      participants: participants
    })
  }

  addParticipants = (e) => {
    const { editActivity, activity } = this.props
    let currentActivity = {
      activity: {
        id: activity.id,
        participantsAttributes: this.state.participants.map(participant => {
          return ({
            employeeId: participant.id,
            date: new Date(),
            isOwner: participant.isOwner,
          })
        })
      }
    }
    e.preventDefault();
    editActivity(currentActivity)
  }

  closeAlert = () => {
    const { getAllEmployees } = this.props
    this.setState(() => ({
      displayAlert: false
    }))
    getAllEmployees()
    history.push('/activities');
  }

  render() {
    const { classes, status } = this.props
    const { currentActivity, filter, employees, participants, model, action, displayAlert, willAddParticipants } = this.state
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container>
        {currentActivity &&
          <CardContent>
            <Paper className={classes.root} elevation={1}>
              <div className={classes.details} >
                <Grid container>
                  <Grid item md={10} xs={12}>
                    <Label variant='display3' rowName={currentActivity.name} value='' />
                  </Grid>
                  <Grid item md={2} xs={12}>
                    <Tooltip title='Edit'>
                      <IconButton className={classes.editBtn} onClick={() => history.push(`/activities/${currentActivity.id}/update`)}>
                        <Icon>edit_icon</Icon>
                      </IconButton>
                    </Tooltip>
                    <Tooltip title='Add participants'>
                      <IconButton className={classes.editBtn} onClick={this.permitAddParticipants}>
                        <Icon>person_add_icon</Icon>
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
                <CardContent className={classes.content}>
                  <Grid container >
                    <Grid item xs={12} md={10}>
                      <Label variant='subheading' rowName='Name: ' value={currentActivity.name} />
                      <Label variant='subheading' rowName='Frequency: ' value={currentActivity.frequency} />
                      <Label variant='subheading' rowName='Points: ' value={currentActivity.points} />
                    </Grid>
                  </Grid>
                </CardContent>
              </div>
              {willAddParticipants &&
                <form onSubmit={this.addParticipants} className={classes.form}>
                  <Grid container className={classes.tablesMargin}>
                    <Grid item md={5} className={classes.tablesMargin}>
                      <TextField
                        label='Search participants'
                        name="employeeIds"
                        value={filter}
                        className={classes.textField}
                        onChange={this.setFilter}
                        margin='normal'
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="start">
                              <Icon >search</Icon>
                            </InputAdornment>
                          ),
                        }} />
                      <SimpleTable
                        objectList={employees ? employees : []}
                        isToAdd={true}
                        isEditing={true}
                        onItem={this.addParticipant}
                      />
                    </Grid>
                    <Grid item md={1}></Grid>
                    <Grid item md={5} className={classes.secondTable}>
                      <ParticipantsTable
                        objectList={participants ? participants : []}
                        isToAdd={false}
                        isEditing={true}
                        onItem={this.removeParticipant}
                        onCheck={this.setIsOwner}
                      />
                    </Grid>
                  </Grid>
                  <div className={classes.actions}>
                    <ActionsButtons previewPage='/activities' />
                  </div>
                </form>
              }
              <Snackbar
                color={variant}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={displayAlert}
                onClose={this.closeAlert}
                autoHideDuration={1500}>
                <MessageContent
                  onClose={this.handleClose}
                  variant={variant}
                  title={title}
                  message={message} />
              </Snackbar>
            </Paper>
          </CardContent>
        }
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllEmployees,
    getActivity,
    editActivity,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    activity: state.activities.currentActivity,
    employeeList: state.employee.all,
    status: state.activities.status
  }
}

const StyledActivityPage = withStyles(activityStyles)(ShowActivityPage)
export default connect(mapStateToProps, mapDispatchToProps)(StyledActivityPage)

