import React from 'react'
import { connect } from 'react-redux';
import Container from '../partials/Container'
import { createClient, getAllClients, } from '../../redux/modules/clients'
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';
import ClientForm from '../partials/ClientForm';

class CreateClientPage extends React.Component {

  state = {
    client: {
      name: '',
      lastName: '',
      contactEmail: '',
      companyName: '',
      phoneNumber: '',
      country: ''
    },
    displayAlert: false,
    model: 'client',
    action: 'created',
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps
    if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    const { getAllClients } = this.props
    getAllClients()
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/clients')
  }

  render() {
    const { client, displayAlert, model, action } = this.state
    const { status, createClient } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title="Add new client"
        body=''>
        <ClientForm
          client={client}
          currentClient={null}
          submitAction={createClient} />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createClient,
    getAllClients
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    status: state.clients.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateClientPage)

