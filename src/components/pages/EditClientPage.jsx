import React from 'react'
import Container from '../partials/Container';
import { bindActionCreators } from 'redux';
import { getClient, editClient } from '../../redux/modules/clients'
import { connect } from 'react-redux';
import { history } from '../../helpers/history'
import { messageHandler } from '../../helpers/alertsHandler'
import { Snackbar } from '@material-ui/core';
import MessageContent from '../partials/MessageContent';
import ClientForm from '../partials/ClientForm';

class EditClientPage extends React.Component {

  state = {
    client: {
      name: '',
      lastName: '',
      contactEmail: '',
      companyName: '',
      phoneNumber: '',
      country: ''
    },
    displayAlert: false,
    model: 'project',
    action: 'edited',
  }

  componentWillMount() {
    const { getClient, match: { params: { clientId } }, status } = this.props
    if (!status) {
      getClient(clientId)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentClient, status } = nextProps
    if (currentClient && !status) {
      this.setState(() => ({
        client: currentClient
      }))
    } else if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/clients')
  }

  render() {
    const { displayAlert, model, action, client} = this.state
    const { status, editClient} = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title='Edit Client'
        body=''>
        {client &&
          <ClientForm
          client={client}
          currentClient={null}
          submitAction={editClient} />
        }
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getClient,
    editClient
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    currentClient: state.clients.currentClient,
    status: state.clients.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditClientPage)
