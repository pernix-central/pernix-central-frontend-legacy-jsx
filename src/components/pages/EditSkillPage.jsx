import React from 'react'
import { connect } from 'react-redux';
import SkillForm from '../partials/SkillForm';
import Container from '../partials/Container'
import { editSkill, getSkill } from '../../redux/modules/skills'
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';

class EditSkill extends React.Component {

  state = {
    skillData: null,
    displayAlert: false,
    model: 'skill',
    action: 'created',
  }

  componentWillMount(){
    const { getSkill, match: { params: { skillId } } } = this.props
    const { skillData } = this.state
    if (!skillData) {
      getSkill(skillId)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { status, currentSkill} = nextProps
    if (currentSkill && !status) {
      this.setState(() => ({
        skillData: currentSkill
      }))
    } else if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/skills')
  }

  render() {
    const { skill, displayAlert, model, action } = this.state
    const { status, editSkill, currentSkill } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title="Edit skill"
        body=''>
        <SkillForm
          skill={skill}
          currentSkill={currentSkill}
          submitAction={editSkill} />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    editSkill,
    getSkill
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    status: state.skills.status,
    currentSkill: state.skills.currentSkill
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditSkill)

