import React from 'react'
import Container from '../partials/Container';
import { bindActionCreators } from 'redux';
import { getAllTeams } from '../../redux/modules/teams'
import { getProject, editProject } from '../../redux/modules/projects'
import { getAllClients } from '../../redux/modules/clients'
import { connect } from 'react-redux';
import { history } from '../../helpers/history'
import { messageHandler } from '../../helpers/alertsHandler'
import { Snackbar } from '@material-ui/core';
import { ValidatorForm } from 'react-material-ui-form-validator';
import ProjectForm from '../partials/ProjectForm';
import MessageContent from '../partials/MessageContent';
import moment from 'moment'

class EditProject extends React.Component {

  state = {
    projectData: {
      id: '',
      name: '',
      description: '',
      startDate: '',
      endDate: '',
      status: '',
      clients: null,
      team: null
    },
    displayAlert: false,
    model: 'project',
    action: 'edited',
  }

  componentWillMount() {
    const { getAllTeams, getProject, match: { params: { projectId } }, status, currentProject, teamList, getAllClients, clientList } = this.props
    if (!currentProject) {
      getProject(projectId)
    }
    ValidatorForm.addValidationRule('validateEndDate', () => {
      const { projectData } = this.state
      const { startDate, endDate } = projectData
      const formatedStartDate = (startDate) ? moment(startDate) : null
      const formatedEndDate = moment(endDate)
      return formatedStartDate ? formatedEndDate > formatedStartDate : formatedEndDate >= new Date()
    });
    if (!teamList) {
      getAllTeams()
    }
    if (!status) {
      getProject(projectId)
    }
    if (!clientList) {
      getAllClients();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentProject, status } = nextProps
    if (currentProject && !status) {
      const { id, name, description, startDate, endDate, status, team, clients } = currentProject
      this.setState(() => ({
        projectData: {
          id: id,
          name: name,
          description: description,
          startDate: startDate,
          endDate: endDate,
          status: status,
          team: team ? {
            key: team.id,
            value: team.id,
            label: team.name
          } : null,
          clients: clients ? clients.map((client) =>
            ({
              label: client.name + ' ' + client.lastName,
              value: client.id,
              key: client.id
            })) : []
        }
      }))
    } else if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  onChangeInputValues = (event) => {
    const { name, value } = event.target
    const { projectData } = this.state
    this.setState(() => ({
      projectData: { ...projectData, [name]: value }
    }))
  }

  onSelect = name => value => {
    const { projectData } = this.state
    this.setState(() => ({
      projectData: { ...projectData, [name]: value }
    }))
  }

  onSubmitForm = (e) => {
    const { editProject } = this.props
    const { projectData } = this.state
    e.preventDefault();

    const { id, name, description, startDate, endDate, status, clients, team } = projectData
    let currentProject = {
      id: id,
      project: {
        name: name,
        description: description,
        startDate: startDate,
        endDate: endDate,
        status: status,
        clientIds: clients ? clients.map(client => client.key) : [],
        teamId: team.value,
      }
    }
    editProject(currentProject)
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/projects')
  }

  render() {
    const { projectData, displayAlert, model, action } = this.state
    const { status, teamList, clientList } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title='Edit Project'
        body=''>
        {projectData &&
          <ProjectForm
            project={projectData}
            changeData={this.onChangeInputValues}
            sendData={this.onSubmitForm}
            editable={false}
            selectClient={this.onSelect('clients')}
            selectTeam={this.onSelect('team')}
            teams={teamList ? teamList.map((team) =>
              ({
                label: team.name,
                value: team.id,
                key: team.id
              })) : []}
            clientsList={clientList}
          />
        }
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllTeams,
    getProject,
    editProject,
    getAllClients
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    teamList: state.teams.all,
    clientList: state.clients.all,
    currentProject: state.projects.currentProject,
    status: state.projects.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProject)
