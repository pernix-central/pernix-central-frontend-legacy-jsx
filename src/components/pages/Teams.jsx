import React from 'react'
import Container from '../partials/Container'
import { bindActionCreators } from 'redux'
import { getAllTeams } from '../../redux/modules/teams'
import { connect } from 'react-redux'
import TeamList from '../partials/TeamList'
import commonStyles from '../../assets/styles/commonStyles'
import { withStyles } from '@material-ui/core'
import { Button, Typography } from '@material-ui/core';
import permit from '../../helpers/permit'
import { history } from '../../helpers/history';

class Teams extends React.Component {
  state={
    ready: false
  }

  componentWillMount() {
    const { getAllTeams, teamList } = this.props;
    if (!teamList) {
      getAllTeams();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { teamList } = nextProps;
    if (teamList) {
      this.setState(() => ({ ready: true }));
    }
  }
  onButonAddPress = () => {
    history.push('/teams/create');
  };

  render() {
    const { teamList, user, classes } = this.props;
    return (
      <Container
        title='Teams'
        body=''
        button={ permit(user, 'Team', 'create') &&
          <Button variant='outlined' color='primary' className={classes.buttonAdd} onClick={this.onButonAddPress}>
            <Typography variant='body2'>
              New Team
            </Typography>
          </Button> }>
        <TeamList teams={teamList} user={user} ready={this.state.ready} />
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllTeams,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    teamList: state.teams.all,
    status: state.teams.status,
    user: state.auth.user
  }
}

const StyledTeams = withStyles(commonStyles)(Teams)
export default connect(mapStateToProps, mapDispatchToProps)(StyledTeams)
