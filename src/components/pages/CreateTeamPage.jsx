import React from 'react'
import { connect } from 'react-redux'
import TeamForm from '../partials/TeamForm'
import { createTeam, getAllTeams } from '../../redux/modules/teams'
import { getAllEmployees } from '../../redux/modules/employee'
import { messageHandler } from '../../helpers/alertsHandler';
import { Snackbar } from '@material-ui/core';
import { bindActionCreators } from 'redux';
import { history } from '../../helpers/history';
import { subtractLists } from "../../helpers/listsHandler"
import MessageContent from '../partials/MessageContent';

class CreateTeamPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      team: {
        name: '',
        employeeIds: []
      },
      filter: '',
      allEmployees: [],
      selectedEmployees: [],
      selectedSize: 0,
      availableEmployees: null,
      availableSize: 0,
      displayAlert: false,
      model: 'team',
      action: 'created'
    }
  }
  componentDidMount() {
    const { getAllEmployees, employeeList } = this.props
    if (!employeeList) {
      getAllEmployees()
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { employeeList, status } = props;
    const { availableEmployees, filter } = state;
    if (employeeList && !availableEmployees && filter === '') {
      return {
        availableEmployees: employeeList,
        allEmployees: employeeList,
        availableSize: employeeList ? employeeList.length : 0,
      }
    }
    if(status){
      return {
        displayAlert: true,
      }
    }
    return null;
  }

  addEmployee = (event) => {
    const { value } = event.currentTarget
    const { selectedEmployees, availableEmployees, team, availableSize, selectedSize } = this.state
    let newSelecteds = selectedEmployees
    let newList = []
    const { employeeList } = this.props
    availableEmployees.forEach(employee => {
      String(employee.id) === String(value) ? newSelecteds.push(employee) : newList.push(employee)
    });
    this.setState(() => ({
      filter: '',
      availableEmployees: subtractLists(newSelecteds, employeeList),
      selectedEmployees: newSelecteds,
      allEmployees: subtractLists(newSelecteds, employeeList),
      availableSize: availableSize - 1,
      selectedSize: selectedSize + 1,
      team: { ...team, employeeIds: selectedEmployees.map(se => se.id) }
    }))
  }

  removeEmployee = (event) => {
    const { value } = event.currentTarget
    const { employeeList } = this.props
    const { allEmployees, selectedEmployees, team, availableSize, selectedSize } = this.state
    let newSelecteds = selectedEmployees
    let index = selectedEmployees.map(element => String(element.id)).indexOf(value)
    allEmployees.push(newSelecteds.splice(index, 1)[0])
    this.setState(() => ({
      filter: '',
      availableEmployees: subtractLists(newSelecteds, employeeList),
      selectedEmployees: newSelecteds,
      allEmployees: subtractLists(newSelecteds, employeeList),
      availableSize: availableSize + 1,
      selectedSize: selectedSize - 1,
      team: { ...team, employeeIds: selectedEmployees.map(se => se.id) }
    }));
  }

  onChange = (event) => {
    const { name, value } = event.target
    const { team } = this.state
    this.setState(() => ({
      team: { ...team, [name]: value }
    }))
  }

  setFilter = (event) => {
    const { value } = event.target
    const { employeeList } = this.props
    const { selectedEmployees } = this.state
    this.setState({
      filter: value,
      availableEmployees: this.filterData(value, subtractLists(selectedEmployees, employeeList))
    })
  }

  filterData = (filter, employees) => {
    return filter !== '' ? employees.filter(employee =>
      employee.firstName.toLowerCase().includes(filter.toLowerCase())
    ) : (employees ? employees : [])
  }

  onSubmit = (e) => {
    const { createTeam } = this.props
    let { team } = this.state
    e.preventDefault();
    team = { team }
    createTeam(team)
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    getAllTeams()
    history.replace('/teams');
  }
  render() {
    const { status } = this.props
    const teamState = this.state
    const { displayAlert, model, action } = this.state
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <div>
        <TeamForm
          teamData={teamState}
          title='Create team'
          changeData={this.onChange}
          sendData={this.onSubmit}
          onFilter={this.setFilter}
          editing={true}
          addItem={this.addEmployee}
          deleteItem={this.removeEmployee}
        />
         <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={750}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createTeam,
    getAllEmployees,
    getAllTeams
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    employeeList: state.employee.all,
    status: state.teams.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTeamPage)
