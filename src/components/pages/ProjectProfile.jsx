import React from 'react'
import { withStyles, CardContent, Icon, Button, Grid, Paper, List, ListItem, ListItemText, Divider, Tooltip } from '@material-ui/core';
import { connect } from 'react-redux'
import projectsProfileStyles from '../../assets/styles/projectsProfileStyles';
import { getProject, editProject } from '../../redux/modules/projects'
import { bindActionCreators } from 'redux';
import Label from '../partials/Label';
import LinkLabel from '../partials/LinkLabel';
import Container from '../partials/Container';
import { history } from '../../helpers/history';
import { PERNIX_GREEN, ERROR } from '../../assets/styles/commonStyles';
import CommentList from '../partials/CommentList';
import { Link } from 'react-router-dom'


class ProjectProfile extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      project: null,
      active: '',
      isDialogOpen: false,
    }

  }

  static getDerivedStateFromProps(props, state) {
    const { getProject, match: { params: { projectId } }, project } = props
    if (!project) {
      getProject(projectId);
      return {
        ...state
      }
    }
    return {
      project: project,
      active: project ? project.active ? 'Inactive' : 'Activate' : ''
    }
  }

  handleChange = status => {
    const { editProject } = this.props
    const { project } = this.state
    const active = status ? 'Inactive' : 'Activate'
    this.setState({
      project: { ...project, active: status },
      active
    })
    editProject({ id: project.id, active: status })
  }

  render() {
    const { classes } = this.props
    const { project, active } = this.state
    return (
      <div>
        {project &&
          <Container>
            <Paper className={classes.root} elevation={1}>
              <div className={classes.details} >
                <CardContent className={classes.content}>
                  <Grid container>
                    <Grid item md={9}>
                      <Label variant='display3' rowName={project.name} value='' />
                    </Grid>
                    <Grid item xs={12} md={2} className={classes.profileActions}>
                      <div className={classes.actionsContainer}>
                        <Tooltip title='Edit'>
                          <Button variant='fab' color='secondary' mini onClick={() => history.push(`/projects/${project.id}/update`)} >
                            <Icon >edit_icon</Icon>
                          </Button>
                        </Tooltip>
                        <Tooltip disableFocusListener title={active}>
                          <Button variant='fab'
                            style={{
                              backgroundColor: project.active ? PERNIX_GREEN : ERROR,
                              color: 'white'
                            }}
                            mini onClick={() => this.handleChange(!project.active)} >
                            <Icon >settings_power</Icon>
                          </Button>
                        </Tooltip>
                      </div>
                    </ Grid>
                  </Grid>
                </CardContent>
                <CardContent className={classes.content}>
                  <Grid container >
                    <Grid item xs={12} md={6}>
                      <Label variant='subheading' rowName='Description:  ' value={project.description} />
                      <Label variant='subheading' rowName='Status:  ' value={project.status} />
                      <Label variant='subheading' rowName='Start Date:  ' value={project.startDate} />
                      <Label variant='subheading' rowName='End Date:  ' value={project.endDate} />
                      {project.team ?
                        <LinkLabel variant='subheading' rowName='Team: ' value={project.team ? project.team.name : 'No assigned project'}
                          to={'/teams/' + project.team.id}
                        /> : <Label variant='subheading' rowName='Team: ' value='No assigned team' />}
                      <Grid container>
                        <Grid item md={2}>
                          <Label variant='subheading' rowName='Clients: ' />
                        </Grid>
                        <Grid item md={10}>
                          <List className={classes.clientsList}>
                            {project.clients.map(client => {
                              return (
                                <ListItem key={client.id} button onClick={() => history.replace('/clients/' + client.id)} className={classes.clientsItemsDiv}>
                                  <ListItemText
                                    className={classes.clientsItemsText}
                                    primary={
                                      <Link to={''}>{client.name + ' ' + client.lastName}</Link>
                                    }
                                    secondary={client.companyName}
                                  />
                                </ListItem>
                              )
                            })}
                          </List>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>

                </CardContent>
              </div>
              <Divider></Divider>
              <CommentList
                projectId={project.id}
                teamId={null}
                comments={project.comments || []} />
            </Paper>
          </Container>
        }
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getProject,
    editProject,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    project: state.projects.currentProject,
  }
}


const StyledProjectPage = withStyles(projectsProfileStyles)(ProjectProfile)
export default connect(mapStateToProps, mapDispatchToProps)(StyledProjectPage)

