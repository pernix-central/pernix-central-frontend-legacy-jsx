import React from 'react';
import Container from '../partials/Container';
import { bindActionCreators } from 'redux';
import { getAllActivities } from '../../redux/modules/activities';
import { connect } from 'react-redux';
import { Button, withStyles, Typography } from '@material-ui/core';
import permit from '../../helpers/permit';
import { history } from '../../helpers/history';
import activityStyles from '../../assets/styles/activityStyles';
import ActivityList from '../partials/ActivityList';

const HEADER = [
  { id: 'name', numeric: false, disablePadding: false, label: 'NAME' },
  { id: 'points', numeric: false, disablePadding: false, label: 'POINTS' },
  { id: 'participants', numeric: false, disablePadding: false, label: 'PARTICIPANTS' },
  { id: 'frequency', numeric: false, disablePadding: false, label: 'FREQUENCY' }
]

class Activities extends React.Component {
  state = {
    ready: false
  };
  onButonAddPress = () => {
    history.replace('/activities/create');
  };

  componentWillMount() {
    const { getAllActivities, activitiesList } = this.props;
    if (!activitiesList) {
      getAllActivities();
    }
  }
  componentWillReceiveProps(nextProps) {
    const { activitiesList } = nextProps;
    if (activitiesList) {
      this.setState(() => ({
        ready: true
      }));
    }
  }

  render() {
    const { activitiesList, user } = this.props;
    return (
      <Container
        title='Activities'
        body=''
        button={permit(user, 'Activity', 'create') &&
          <Button variant='outlined' color='primary' onClick={this.onButonAddPress}>
            <Typography variant='body2'>
              New Activity
            </Typography>
          </Button>}>
        <ActivityList activityList={activitiesList} header={HEADER} ready={this.state.ready} />
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllActivities
  }, dispatch);
}

function mapStateToProps(state) {
  return {
    activitiesList: state.activities.all,
    status: state.activities.status,
    user: state.auth.user
  };
}

const StyledActivities = withStyles(activityStyles)(Activities);
export default connect(mapStateToProps, mapDispatchToProps)(StyledActivities);
