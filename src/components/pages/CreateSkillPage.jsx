import React from 'react'
import { connect } from 'react-redux';
import SkillForm from '../partials/SkillForm';
import Container from '../partials/Container'
import { createSkill } from '../../redux/modules/skills'
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';

class CreateSkillPage extends React.Component {

  state = {
    skill: {
      name: '',
      skillType: '',
      level: '0'
    },
    displayAlert: false,
    model: 'skill',
    action: 'created',
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps
    if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/skills')
  }

  render() {
    const { skill, displayAlert, model, action } = this.state
    const { status, createSkill } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title="Add new skill"
        body=''>
        <SkillForm
          skill={skill}
          currentSkill={null}
          submitAction={createSkill} />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createSkill
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    status: state.skills.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSkillPage)

