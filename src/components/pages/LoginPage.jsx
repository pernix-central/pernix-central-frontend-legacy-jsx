import React from 'react'
import LoginForm from '../partials/LoginForm'
import { connect } from 'react-redux'
import { authRequest } from '../../redux/modules/auth'
import { history } from '../../helpers/history';
import Container from '../partials/Container';

class LoginPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      error: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.error) {
      return nextProps.authToken && history.push('/')
    } else {
      this.setState({
        error: true
      })
    }
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      error: false
    })
  }

  onSubmit = (e) => {
    e.preventDefault();
    let { email, password } = this.state
    this.props.sendRequest(email, password)
  }

  render() {
    const loginInfo = this.state
    return (
      <Container>
        <LoginForm
          credentials={loginInfo}
          setCredentials={this.onChange}
          sendData={this.onSubmit}
        />
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return ({
    sendRequest: (email, password) => { dispatch(authRequest(email, password)) }
  })
}

function mapStateToProps(state) {
  return {
    authToken: state.auth.authToken,
    error: state.auth.error
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
