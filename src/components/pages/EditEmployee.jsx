import React from 'react'
import Container from '../partials/Container';
import EmployeeForm from '../partials/EmployeeForm';
import { bindActionCreators } from 'redux';
import { getEmployee, editEmployee, setStatusCode } from '../../redux/modules/employee'
import { updateUser } from '../../redux/modules/auth'
import { connect } from 'react-redux';
import { messageHandler } from '../../helpers/alertsHandler'
import { Snackbar } from '@material-ui/core';
import { ValidatorForm } from 'react-material-ui-form-validator';
import MessageContent from '../partials/MessageContent';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import ScheduleForm from '../partials/ScheduleForm';
import { insertPayloadInform } from '../../helpers/imagesHelper';
import EmployeeSkills from '../partials/EmployeeSkills';
import Loader from 'react-loader'

class EditEmployee extends React.Component {

  state = {
    userData: null,
    schedule: null,
    tcpSkills: null,
    displayAlert: false,
    notLoading: true,
    imagePreview: null,
    model: 'of employee',
    action: 'modification',
    value: 0,
  }

  changeTabs = (event, value) => {
    this.setState(() => ({ value }))
  }

  componentWillMount() {
    const { getEmployee, match: { params: { employeeId } } } = this.props
    const { userData } = this.state
    if (!userData) {
      getEmployee(employeeId)
    }
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      return value === this.state.userData.password;
    });
    ValidatorForm.addValidationRule('isValidDate', (value) => {
      var today = new Date()
      var val = new Date(value)
      return (val < today);
    });
  }

  componentWillReceiveProps(nextProps) {
    const { currentEmployee, statusEmployee, statusSchedule } = nextProps
    if (currentEmployee && !statusEmployee && !statusSchedule) {
      const { images, schedule, tcpSkills } = currentEmployee
      this.setState(() => ({
        userData: currentEmployee,
        imagePreview: images.length > 0 ? images[0].medium : null,
        schedule: schedule,
        tcpSkills: tcpSkills
      }))
    } else if (statusEmployee || statusSchedule) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  onChangeInputValues = (event) => {
    const { name, value, type, checked, files } = event.target
    const { userData } = this.state
    switch (type) {
      case 'checkbox':
        this.setState(() => ({
          userData: { ...userData, [name]: checked }
        }))
        break;
      case 'file':
        if (files[0]) {
          this.setState(() => ({
            userData: { ...userData, imagesAttributes: files[0] },
            imagePreview: URL.createObjectURL(files[0])
          }))
        }
        break;

      default:
        this.setState(() => ({
          userData: { ...userData, [name]: value }
        }))
        break;
    }
  }

  onReset = (e) => {
    const { currentEmployee } = this.props
    this.setState({
      userData: currentEmployee
    })
  }


  onSubmitForm = (e) => {
    e.stopPropagation();
    const { editEmployee } = this.props
    const { userData } = this.state
    var data = insertPayloadInform(userData, true)
    editEmployee(userData.id, data)
    this.setState((state) => ({
      ...state,
      notLoading: false
    }))
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false,
      notLoading: true
    }))
  }

  render() {
    const { userData, schedule, tcpSkills, displayAlert, model, action, value, imagePreview } = this.state
    const { statusEmployee, statusSchedule } = this.props
    const { title, message, variant } = messageHandler(
      statusEmployee ? statusEmployee : statusSchedule,
      { model, action }
    )
    return (
      <Container
        title=''
        body=''
        elevation={3}>
        <Loader loaded={this.state.notLoading} options={{
          width: 7,
          radius: 30,
          corners: 1,
          speed: 1,
          fps: 20,
          zIndex: 2e9,
          top: '50px',
        }} >
          <AppBar position="static" color='inherit' elevation={0}>
            <Tabs value={value} onChange={this.changeTabs}>
              <Tab label="Basic info" value={0} />
              <Tab label="Schedule" value={1} />
              <Tab label="Skills" value={2} />
            </Tabs>
          </AppBar>
          {(userData && value === 0) &&
            <EmployeeForm
              user={userData}
              changeData={this.onChangeInputValues}
              sendData={this.onSubmitForm}
              toReset={this.onReset}
              editable={false}
              withSchedule={true}
              imagePreview={imagePreview}
            />
          }
          {(schedule && value === 1) &&
            <ScheduleForm scheduleId={schedule.id} userId={userData.id} />
          }
          {(tcpSkills && value === 2) &&
            <EmployeeSkills employeeSkillList={tcpSkills} employeeId={userData.id} />
          }
        </Loader>
        {displayAlert && <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.closeAlert}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>}
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getEmployee,
    editEmployee,
    updateUser,
    setStatusCode
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    currentEmployee: state.employee.currentEmployee,
    statusEmployee: state.employee.status,
    statusSchedule: state.schedule.status,
    currentUser: state.auth.user
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEmployee)
