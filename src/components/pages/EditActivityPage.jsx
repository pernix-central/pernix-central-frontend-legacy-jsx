import React from 'react'
import Container from '../partials/Container';
import { bindActionCreators } from 'redux';
import { getActivity, editActivity } from '../../redux/modules/activities'
import { connect } from 'react-redux';
import { history } from '../../helpers/history'
import { messageHandler } from '../../helpers/alertsHandler'
import { Snackbar } from '@material-ui/core';
import MessageContent from '../partials/MessageContent';
import ActivityForm from '../partials/ActivityForm';

class EditActivityPage extends React.Component {

  state = {
    activity: null,
    displayAlert: false,
    model: 'Activity',
    action: 'edited',
  }

  componentWillMount() {
    const { getActivity, match: { params: { activityId } }, status, currentActivity} = this.props
    if (!status && !currentActivity) {
      getActivity(activityId)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentActivity, status } = nextProps
    if (currentActivity && !status) {
      this.setState(() => ({
        activity: currentActivity
      }))
    } else if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/activities')
  }

  render() {
    const { displayAlert, model, action } = this.state
    const { status, editActivity, currentActivity } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title='Edit Activity'
        body=''>
        <ActivityForm
          activity={currentActivity}
          submitAction={editActivity} />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getActivity,
    editActivity
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    currentActivity: state.activities.currentActivity,
    status: state.activities.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditActivityPage)
