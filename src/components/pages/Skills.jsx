import React from 'react'
import Container from '../partials/Container'
import { bindActionCreators } from 'redux'
import { getAllSkills } from '../../redux/modules/skills'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import commonStyles from '../../assets/styles/commonStyles'
import SkillList from '../partials/SkillList'
import { history } from '../../helpers/history';
import { formatSkill } from '../../helpers/rowFormatters';
import { Button, Typography } from '@material-ui/core';
import permit from '../../helpers/permit';

class Skills extends React.Component {
  state = {
    ready: false
  };

  onButonAddPress = () => {
    history.replace('/skills/create');
  };

  onEditSkillButtonPress = id => {
    history.push(`skills/${id}/update`);
  };

  componentWillMount() {
    const { getAllSkills, skillList } = this.props;
    if (!skillList) {
      getAllSkills();
    }
  }
  componentWillReceiveProps(nextProps) {
    const { skillList } = nextProps;
    if (skillList) {
      this.setState(() => ({ ready: true }));
    }
  }

  render() {
    const { skillList, user, classes } = this.props;
    return (
      <Container
        title='Skills'
        body=''
        button={
          permit(user, 'Skill', 'create') && (
            <div className={classes.newSkill}>
              <Button
                variant='outlined'
                color='primary'
                className={classes.buttonAdd}
                onClick={this.onButonAddPress}>
                <Typography variant='body2'>New Skill</Typography>
              </Button>
            </div>
          )
        }>
        <SkillList formatter={formatSkill} skills={skillList} buttonIcon='edit_icon' itemAction={this.onEditSkillButtonPress} user={user} elevation={1} ready={this.state.ready} />
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllSkills,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    skillList: state.skills.all,
    status: state.skills.status,
    user: state.auth.user
  }
}

const StyledSkills = withStyles(commonStyles)(Skills)
export default connect(mapStateToProps, mapDispatchToProps)(StyledSkills)
