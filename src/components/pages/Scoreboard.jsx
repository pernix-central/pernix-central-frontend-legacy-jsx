import React from 'react'
import Container from '../partials/Container'
import { bindActionCreators } from 'redux'
import { getAllEmployees } from '../../redux/modules/employee'
import { connect } from 'react-redux'
import ScoreboardPresentation from '../partials/ScoreboardPresentation';
import { history } from '../../helpers/history';
import ScoreboardList from '../partials/ScoreboardList';

class Scoreboard extends React.Component {
  
  state = {
    ready: false
  }

  componentWillMount() {
    const { getAllEmployees, employeeList } = this.props
    if (!employeeList) {
      getAllEmployees()
    } else {
      this.setData(employeeList)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { employeeList } = nextProps
    this.setData(employeeList)
  }

  setData = (employeeList) => {
    this.setState(() => ({
      employees: employeeList,
      ready: true
    }))
  }
  
  getContent = () => {
    const { location: { pathname } } = history
    const { employees } = this.state
    switch (pathname) {
      case '/scoreboard/presentation':
        return (
          <ScoreboardPresentation
            leaders={employees.slice(0, 3)}
            employees={employees.slice(3, 10)} />
        )
      case '/scoreboard/list':
        return (
          <Container
            title='Scoreboard'
            body=''>
            <ScoreboardList employeeList={employees ? employees : []} />
          </Container>
        )
      default:
        break;
    }
  }
  render() {
    const { ready } = this.state
    return (
      <div>
        {ready && this.getContent()}
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllEmployees,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    employeeList: state.employee.all,
    status: state.employee.status,
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Scoreboard)