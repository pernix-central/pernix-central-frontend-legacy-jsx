import React from 'react'
import { withStyles, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';
import { connect } from 'react-redux'
import profileStyles from '../../assets/styles/profileStyles';
import { bindActionCreators } from 'redux';
import { getTeam, deleteTeam } from '../../redux/modules/teams';
import { TeamProfile } from '../partials/TeamProfile';
import { history } from '../../helpers/history';
import Container from '../partials/Container';

class ShowTeamPage extends React.Component {

  state = {
    open: false
  }

  static getDerivedStateFromProps(props, state) {
    const { getTeam, match: { params: { teamId } }, team } = props
    if (!team) {
      getTeam(teamId);
      return {
        ...state
      }
    }
    return {
      team: team
    }
  }

  deletingTeam = () => {
    const { team, deleteTeam } = this.props;
    deleteTeam(team);
    history.push('/teams')
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, team } = this.props
    return (
      <Container>
        {team &&
          <TeamProfile team={team} classes={classes} onDelete={this.handleClickOpen} />
        }
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Are you sure to delete this team?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              If you delete this team you won't be able to recover this information.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" className={classes.cancelButton}>
              Disagree
            </Button>
            <Button onClick={this.deletingTeam} color="primary" className={classes.submitButton} autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTeam,
    deleteTeam,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    team: state.teams.currentTeam,
  }
}


const StyledShowTeamPage = withStyles(profileStyles)(ShowTeamPage)
export default connect(mapStateToProps, mapDispatchToProps)(StyledShowTeamPage)
