import React from 'react'
import { connect } from 'react-redux'
import { UserProfile } from '../partials/UserProfile'
import { getEmployee } from '../../redux/modules/employee'
import profileStyles from '../../assets/styles/profileStyles';
import { withStyles } from '@material-ui/core'
import { bindActionCreators } from 'redux';
import Container from '../partials/Container';

class HomePage extends React.Component {
  state = {
    tab: 0
  }

  componentWillMount() {
    const { getEmployee, user } = this.props
    getEmployee(user.id);
    
  }

  onChangeAttributeToShow = (event, value) => {
    this.setState((state)=> ({
      ...state,
      tab: value
    }))
  }

  render() {
    const { classes, currentEmployee, user} = this.props
    const { tab } = this.state

    return (
      <Container>
        {currentEmployee &&
          <UserProfile user={currentEmployee} classes={classes} isCurrentUser={true} tab={tab} userInSession={user} onChangeAttribute={this.onChangeAttributeToShow} />
        }
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getEmployee
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    currentEmployee: state.employee.currentEmployee,
    user: state.auth.user,
  }
}

const StyledHomePage = withStyles(profileStyles)(HomePage)
export default connect(mapStateToProps, mapDispatchToProps)(StyledHomePage)
