import React from 'react'
import Container from '../partials/Container'
import { bindActionCreators } from 'redux'
import { getAllClients } from '../../redux/modules/clients'
import { connect } from 'react-redux'
import { Button, withStyles, Typography } from '@material-ui/core'
import permit from '../../helpers/permit'
import ClientList from '../partials/ClientList'
import { history } from '../../helpers/history'
import clientStyle from '../../assets/styles/clientStyle'

const HEADER = [
  { id: 'name', numeric: false, disablePadding: false, label: 'NAME' },
  { id: 'contactEmail', numeric: false, disablePadding: false, label: 'EMAIL' },
  { id: 'phoneNumber', numeric: false, disablePadding: false, label: 'PHONE NUMBER' },
  { id: 'companyName', numeric: false, disablePadding: false, label: 'COMPANY' },
  { id: 'country', numeric: false, disablePadding: false, label: 'COUNTRY' },
]

class Clients extends React.Component {
  state={
    ready: false
  }

  onButonAddPress = () => {
    history.push('/clients/create')
  }

  componentWillMount() {
    const { getAllClients, clientList } = this.props
    if (!clientList) {
      getAllClients()
    }
  }

  componentWillReceiveProps(nextProps) {
    const { clientList } = nextProps;
    if (clientList) {
      this.setState(() => ({ ready: true }));
    }
  }

  render() {
    const { classes, clientList, user } = this.props
    return (
      <Container
        title='Clients'
        body=''
        button={permit(user, "Client", "create") &&
          <Button variant="outlined" color="primary" className={classes.buttonAdd} onClick={this.onButonAddPress}>
            <Typography variant='body2'>
              New client
            </Typography>
          </Button>} >
        <ClientList clientList={clientList} header={HEADER} ready={this.state.ready}/>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllClients,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    clientList: state.clients.all,
    status: state.clients.status,
    user: state.auth.user
  }
}

const StyledClients = withStyles(clientStyle)(Clients)
export default connect(mapStateToProps, mapDispatchToProps)(StyledClients)
