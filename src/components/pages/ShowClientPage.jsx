import React from 'react'
import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux'
import profileStyles from '../../assets/styles/profileStyles';
import { bindActionCreators } from 'redux';
import { getClient } from '../../redux/modules/clients';
import ClientProfile from '../partials/ClientProfile';
import Container from '../partials/Container';

class ShowClientPage extends React.Component {

  state = {
    open: false
  }

  componentWillMount() {
    const { getClient, match: { params: { clientId } } } = this.props
    getClient(clientId);
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, client } = this.props
    return (
      <Container>
        {client &&
          <ClientProfile client={client} classes={classes} onDelete={this.handleClickOpen} />
        }
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getClient,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    client: state.clients.currentClient,
  }
}


const StyledShowTeamPage = withStyles(profileStyles)(ShowClientPage)
export default connect(mapStateToProps, mapDispatchToProps)(StyledShowTeamPage)
