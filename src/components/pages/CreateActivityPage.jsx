import React from 'react'
import { connect } from 'react-redux';
import Container from '../partials/Container'
import { createActivity } from '../../redux/modules/activities'
import { bindActionCreators } from 'redux';
import { Snackbar } from '@material-ui/core';
import { messageHandler } from '../../helpers/alertsHandler';
import { history } from '../../helpers/history';
import MessageContent from '../partials/MessageContent';
import ActivityForm from '../partials/ActivityForm';

class CreateActivityPage extends React.Component {

  state = {
    activity: {
      name:'',
      frequency:'',
      points:''
    },
    displayAlert: false,
    model: 'activity',
    action: 'created',
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps
    if (status) {
      this.setState(() => ({
        displayAlert: true
      }))
    }
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    history.replace('/activities')
  }

  render() {
    const { displayAlert, model, action } = this.state
    const { status, createActivity } = this.props
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <Container
        title="Add new activity"
        body=''>
        <ActivityForm
          activity={null}
          submitAction={createActivity} />
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={1500}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message} />
        </Snackbar>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createActivity
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    status: state.activities.status
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateActivityPage)

