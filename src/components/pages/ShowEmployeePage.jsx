import React from 'react'
import { connect } from 'react-redux'
import { UserProfile } from '../partials/UserProfile'
import profileStyles from '../../assets/styles/profileStyles';
import { withStyles } from '@material-ui/core'
import { getEmployee, changeStatusEmployee } from '../../redux/modules/employee'
import { bindActionCreators } from 'redux';
import Container from '../partials/Container';

class ShowEmployeePage extends React.Component {

  state = {
    tab: 0,
    userData: null,
    active: '',
    isCurrentUser: false
  }

  componentWillMount() {
    const { getEmployee, match: { params: { employeeId } } } = this.props
    getEmployee(employeeId);
  }

  componentWillReceiveProps(nextProps) {
    const { currentEmployee, userInSession } = nextProps;
    if (!this.state.userData) {
      this.setState({
        userData: currentEmployee,
        isCurrentUser: Boolean(userInSession.id === currentEmployee.id),
        active: currentEmployee ? currentEmployee.active ? 'Inactivate' : 'Activate' : ''
      })
    }
  }

  onChangeAttributeToShow = (event, value) => {
    this.setState((state) => ({
      ...state,
      tab: value
    }))
  }

  onChangeStatus = (name) => (event) => {
    const { userData } = this.state;
    const { changeStatusEmployee } = this.props;
    let value = !userData.active
    this.setState({
      userData: { ...userData, [name]: value },
      active: value ? 'Inactivate' : 'Activate'
    })
    changeStatusEmployee({ active: value, id: userData.id });
  };

  render() {
    const { classes, userInSession } = this.props
    const { active, userData, tab, isCurrentUser } = this.state
    return (
      <Container>
        {userData &&
          <UserProfile
            user={userData}
            classes={classes}
            handleChange={this.onChangeStatus('active')}
            isActive={active}
            isCurrentUser={isCurrentUser}
            userInSession={userInSession}
            onChangeAttribute={this.onChangeAttributeToShow}
            tab={tab}
          />
        }
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getEmployee,
    changeStatusEmployee
  }, dispatch)
}


function mapStateToProps(state) {
  return {
    currentEmployee: state.employee.currentEmployee,
    userInSession: state.auth.user
  }
}

const StyledHomePage = withStyles(profileStyles)(ShowEmployeePage)
export default connect(mapStateToProps, mapDispatchToProps)(StyledHomePage)
