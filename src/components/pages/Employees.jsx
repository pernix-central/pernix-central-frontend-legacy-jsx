import React from 'react'
import Container from '../partials/Container'
import { bindActionCreators } from 'redux'
import { getAllEmployees } from '../../redux/modules/employee'
import { getAllSkills } from '../../redux/modules/skills'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core';
import EmployeeList from '../partials/EmployeeList';
import commonStyles from '../../assets/styles/commonStyles';

class Employees extends React.Component {

  state = {
    employeeList: [],
    skillList: []
  }

  static getDerivedStateFromProps(props, state) {
    const { getAllEmployees, getAllSkills, employeeList, skillList } = props
    if (!employeeList) {
      getAllEmployees()
      return {
        ...state
      }
    } else if (!skillList) {
      getAllSkills()
      return {
        ...state,
        employeeList
      }
    } else {
      return {
        skillList,
        employeeList
      }
    }
  }

  render() {
    const { employeeList, user, skillList } = this.props
    return (
      <Container title=''>
        <EmployeeList objectList={employeeList ? employeeList : []} skills={skillList} user={user} />
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllEmployees,
    getAllSkills
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    employeeList: state.employee.all,
    status: state.employee.status,
    skillList: state.skills.all,
    user: state.auth.user
  }
}

const StyledEmployees = withStyles(commonStyles)(Employees)
export default connect(mapStateToProps, mapDispatchToProps)(StyledEmployees)
