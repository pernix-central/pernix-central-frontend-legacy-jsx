import React from 'react';
import { withStyles, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { getAllProjects } from '../../redux/modules/projects';
import { bindActionCreators } from 'redux';
import Container from '../partials/Container';
import ProjectList from '../partials/ProjectList';
import permit from '../../helpers/permit';
import Button from '@material-ui/core/Button';
import { history } from '../../helpers/history';

const projectStyles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: '3% 5%'
  }
});

const HEADER = [
  { id: 'name', numeric: false, disablePadding: false, label: 'NAME' },
  { id: 'status', numeric: false, disablePadding: false, label: 'STATUS' },
  { id: 'startDate', numeric: false, disablePadding: false, label: 'START DATE' },
  { id: 'endDate', numeric: false, disablePadding: false, label: 'END DATE' }
];

class Projects extends React.Component {
  state = {
    ready: false
  };

  componentWillMount() {
    const { getAllProjects, projectList } = this.props;
    if (!projectList) {
      getAllProjects();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { projectList } = nextProps;
    if (projectList) {
      this.setState(() => ({ ready: true }));
    }
  }
  onButonAddPress = () => {
    history.push('/projects/create');
  };

  render() {
    const { projectList, user } = this.props;
    return (
      <div>
        <Container
          title='Projects'
          body=''
          button={ permit(user, 'Project', 'create') && 
            <Button variant='outlined' color='primary' onClick={this.onButonAddPress}>
              <Typography variant='body2'>
                New Project
              </Typography>
            </Button> }>
          <ProjectList header={HEADER} projectList={projectList} user={user} ready={this.state.ready} />
        </Container>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllProjects 
  }, dispatch);
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    projectList: state.projects.all
  };
}

const StyledProjects = withStyles(projectStyles)(Projects);

export default connect(mapStateToProps, mapDispatchToProps)(StyledProjects);
