import React from 'react'
import TeamForm from '../partials/TeamForm';
import { bindActionCreators } from 'redux';
import { getTeam, editTeam } from '../../redux/modules/teams'
import { getAllEmployees } from '../../redux/modules/employee'
import { connect } from 'react-redux';
import { history } from "../../helpers/history"
import { messageHandler } from '../../helpers/alertsHandler'
import { Snackbar } from '@material-ui/core';
import { subtractLists } from "../../helpers/listsHandler"
import MessageContent from '../partials/MessageContent';

class EditTeamPage extends React.Component {
  state = {
    team: null,
    selectedEmployees: null,
    availableEmployees: null,
    allEmployees: null,
    availableSize: 0,
    selectedSize: 0,
    filter: '',
    displayAlert: false,
    model: 'team',
    action: 'edited',
  }

  componentDidMount() {
    const { getAllEmployees, employeeList, getTeam, match: { params: { teamId } } } = this.props
    const { team } = this.state
    if (!employeeList) {
      getAllEmployees()
    }
    if (!team) {
      getTeam(teamId)
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { employeeList, currentTeam, status } = props;
    const { filter, availableEmployees, selectedEmployees } = state;
    if (currentTeam && employeeList && !availableEmployees && !selectedEmployees && filter === '') {
      const { employees, name, id } = currentTeam
      let availableEmployeesList = currentTeam.employees.length > 0 ?
          subtractLists(employees, employeeList) : employeeList;
      return {
        availableEmployees: availableEmployeesList,
        allEmployees: availableEmployeesList,
        selectedEmployees: employees,
        availableSize: availableEmployeesList.length,
        selectedSize: employees.length,
        team: {
          id: id,
          name: name,
          employeeIds: employees.map(e => e.id)
        }
      }
    }
    if(status){
      return {
        displayAlert: true,
      }
    }
    return null;
  }

  addEmployee = (event) => {
    const { value } = event.currentTarget
    const { selectedEmployees, availableEmployees, team, availableSize, selectedSize } = this.state
    let newSelecteds = selectedEmployees
    let newList = []
    const { employeeList } = this.props
    availableEmployees.forEach(employee => {
      String(employee.id) === String(value) ? newSelecteds.push(employee) : newList.push(employee)
    });
    this.setState(() => ({
      filter: '',
      availableEmployees: subtractLists(newSelecteds, employeeList),
      selectedEmployees: newSelecteds,
      allEmployees: employeeList,
      availableSize: availableSize - 1,
      selectedSize: selectedSize + 1,
      team: { ...team, employeeIds: selectedEmployees.map(se => se.id) }
    }))
  }

  removeEmployee = (event) => {
    const { value } = event.currentTarget
    const { employeeList } = this.props
    const { allEmployees, selectedEmployees, team, availableSize, selectedSize } = this.state
    let newSelecteds = selectedEmployees
    let index = selectedEmployees.map(element => String(element.id)).indexOf(value)
    allEmployees.push(newSelecteds.splice(index, 1)[0])
    this.setState(() => ({
      filter: '',
      availableEmployees: subtractLists(newSelecteds, employeeList),
      selectedEmployees: newSelecteds,
      allEmployees: subtractLists(newSelecteds, employeeList),
      availableSize: availableSize + 1,
      selectedSize: selectedSize - 1,
      team: { ...team, employeeIds: selectedEmployees.map(se => se.id) }
    }));
  }

  onChange = (event) => {
    const { name, value } = event.target
    const { team } = this.state
    this.setState(() => ({
      team: { ...team, [name]: value }
    }))

  }

  onSubmit = (e) => {
    const { editTeam } = this.props
    let { team } = this.state
    e.preventDefault();
    editTeam({ team });
  }

  closeAlert = () => {
    this.setState(() => ({
      displayAlert: false
    }))
    getAllEmployees();
    history.replace('/teams')
  }

  filterData = (filter, employees) => {
    return filter !== '' ? employees.filter(employee =>
      employee.firstName.toLowerCase().includes(filter.toLowerCase())
    ) : (employees ? employees : [])
  }

  setFilter = (event) => {
    const { value } = event.target
    const { employeeList } = this.props
    const { selectedEmployees } = this.state
    this.setState({
      filter: value,
      availableEmployees: this.filterData(value, subtractLists(selectedEmployees, employeeList))
    })
  }

  render() {
    const { status } = this.props
    const teamState = this.state
    const { team, displayAlert, model, action } = this.state
    const { title, message, variant } = messageHandler(status, { model, action })
    return (
      <div>
        {team &&
          <TeamForm
            teamData={teamState}
            title='Edit team'
            changeData={this.onChange}
            sendData={this.onSubmit}
            onFilter={this.setFilter}
            editing={true}
            addItem={this.addEmployee}
            deleteItem={this.removeEmployee}
          />
        }
        <Snackbar
          color={variant}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={displayAlert}
          onClose={this.closeAlert}
          autoHideDuration={750}>
          <MessageContent
            onClose={this.handleClose}
            variant={variant}
            title={title}
            message={message}/>
        </Snackbar>
      </div>
    )
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTeam,
    editTeam,
    getAllEmployees,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    currentTeam: state.teams.currentTeam,
    status: state.teams.status,
    employeeList: state.employee.all,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTeamPage)
