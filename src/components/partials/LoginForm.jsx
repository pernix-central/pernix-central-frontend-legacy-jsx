import React from 'react'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles, Typography } from '@material-ui/core';
import loginFormStyle from '../../assets/styles/loginFormStyles'

const LoginForm = (props) => {
  const { classes, setCredentials, sendData, credentials } = props
  const { email, password, error } = credentials
  return (
    <Card className={classes.card}>
      <img className={classes.media}
        src='./images/pernix_central_logo.png'
        alt='Pernix software crafters' />
      <ValidatorForm onSubmit={sendData}>
        <CardContent>
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <TextValidator
                name='email'
                label='Email'
                type="email"
                value={email}
                className={classes.formControl}
                validators={['required', 'isEmail']}
                errorMessages={['This field is required', 'Email is not valid']}
                onChange={setCredentials}
              />
            </Grid>
            <Grid item xs={12}>
              <TextValidator
                name='password'
                label='Password'
                type='password'
                value={password}
                className={classes.formControl}
                validators={['required']}
                errorMessages={['This field is required']}
                onChange={setCredentials}
              />
              {error && <Typography align='center' color='error' variant='subheading'>Invalid credentials</Typography>}
            </Grid>
          </Grid>
        </CardContent>
        <Grid container className={classes.centerRows} >
          <Grid item xs={12}>
            <Button className={classes.loginButton} type='submit' size='large'>
              Sign In
            </Button>
          </Grid>
         
        </Grid>
      </ValidatorForm>
    </Card>
  )
}

export default withStyles(loginFormStyle)(LoginForm)
