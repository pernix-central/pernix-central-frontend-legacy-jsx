import React from 'react'
import PropTypes from 'prop-types'
import { TableHead, TableRow, TableCell, TableSortLabel } from '@material-ui/core';

class PagedTableHead extends React.Component {

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property)
  }

  render() {
    const { order, orderBy, columns } = this.props
    return (
      <TableHead>
        <TableRow>
          {columns.map(row => {
            return (
              <TableCell
                key={row.id}
                numeric={row.numeric}
                padding={row.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === row.id ? order : false}
                onClick={this.createSortHandler(row.id)}>
                <TableSortLabel
                  active={orderBy === row.id}
                  direction={order}>
                  {row.label}
                </TableSortLabel>
              </TableCell>
            )
          })}
        </TableRow>
      </TableHead>
    )
  }
}

PagedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
}

export default PagedTableHead
