import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import CardHeader from '@material-ui/core/CardHeader';
import sidebarStyles from '../../assets/styles/sidebarStyles'
import { connect } from 'react-redux'
import SideList from './SideList';
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux';
import { getEmployee } from '../../redux/modules/employee'
import { updateUser } from '../../redux/modules/auth'
import { getImage } from '../../helpers/imagesHelper';

class SideBar extends React.Component {

  state = {
    open: false,
    currentUser: null
  }

  static getDerivedStateFromProps(props, state) {
    const { getEmployee, updateUser, employee, user } = props
    if (!user) {
      return {
        ...state
      }
    } else if (!user.images) {
      if (!employee) {
        getEmployee(user.id);
      } else {
        updateUser(employee)
      }
      return {
        ...state
      }
    } else {
      return {
        currentUser: user,
      }
    }
  }

  toggleDrawer = (open) => () => {
    this.setState({
      open
    })
  }

  render() {
    const { classes, user } = this.props;
    const { currentUser } = this.state
    return (
      <div>
        <AppBar position='static' color='inherit' className={classes.sticky}>
          <Toolbar>
            <IconButton className={classes.menuButton} color='inherit' aria-label='Menu'>
              <MenuIcon onClick={this.toggleDrawer(true)} />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer open={this.state.open} onClose={this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role='button'
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}>
            {currentUser && <CardHeader
              className={classes.sideBarHeader}
              avatar={
                <Avatar aria-label='Recipe' className={classes.avatar}>
                  <img src={getImage(currentUser.images)} alt='' width='50' height='50' />
                </Avatar>
              }
              title={user.role}
              subheader={user.email} />
            }
            <SideList />
          </div>
        </Drawer>
      </div>
    )
  }
}

SideBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getEmployee,
    updateUser
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    employee: state.employee.currentEmployee
  }
}

const StyledSidebar = withStyles(sidebarStyles)(SideBar)
const SidebarWithRedux = connect(mapStateToProps, mapDispatchToProps)(StyledSidebar)
export default withRouter(SidebarWithRedux)

