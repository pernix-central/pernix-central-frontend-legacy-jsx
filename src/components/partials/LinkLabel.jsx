import React from 'react'
import { Typography, withStyles } from '@material-ui/core';
import { Link } from 'react-router-dom'

const labelStyles = theme => ({
  label: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
})

const LinkLabel = (props) => {
  const { classes, rowName, value, variant, to } = props
  return (
    <Typography variant={variant} color='textSecondary' noWrap={true} className={classes.label}>
      <b>{rowName}</b>
      <Link to={to}>{value}</Link>
    </Typography>
  )
}

export default withStyles(labelStyles)(LinkLabel)
