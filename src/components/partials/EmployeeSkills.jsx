import React from 'react'
import { Grid, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getAllSkills } from '../../redux/modules/skills'
import { editEmployeeTcpSkills } from '../../redux/modules/employee'
import { subtractSkillLists } from '../../helpers/listsHandler';
import { ValidatorForm } from 'react-form-validator-core';
import Label from './Label';
import SkillList from './SkillList';
import EmployeeSkillLevel from './EmployeeSkillLevel';
import { formatTcpSkill, formatSkill } from '../../helpers/rowFormatters';
import employeeSkillStyles from '../../assets/styles/employeeSkillStyles';
import ActionsButtons from './ActionsButtons';

class EmployeeSkills extends React.Component {
  state = {
    availableSkills: [],
    employeeSkills: [],
    selectedSkill: null,
    displayDialog: false
  }

  componentWillMount() {
    const { skillList, getAllSkills, employeeSkillList } = this.props
    if (!skillList) {
      getAllSkills()
    }
    else {
      var availableSkills = subtractSkillLists(employeeSkillList, skillList)
      this.setDataInState(null, false, availableSkills, employeeSkillList)
    }
  }

  setDataInState(selectedSkill, displayDialog, availableSkills, employeeSkills) {
    this.setState(() => ({
      selectedSkill,
      availableSkills,
      displayDialog,
      employeeSkills
    }))
  }

  componentWillReceiveProps(nextProps) {
    const { skillList, employeeSkillList } = nextProps
    var availableSkills = subtractSkillLists(employeeSkillList, skillList)
    this.setDataInState(null, false, availableSkills, employeeSkillList)
  }

  onCancelSkillSelection = (skill) => {
    const { availableSkills, employeeSkills } = this.state
    skill.level ? employeeSkills.push(skill) : availableSkills.push(skill)
    this.setDataInState(null, false, availableSkills, employeeSkills)
  }

  onAcceptlSkillSelection = (skill) => {
    const { employeeSkills, availableSkills } = this.state
    employeeSkills.push(skill)
    this.setDataInState(null, false, availableSkills, employeeSkills)
  }

  selectSkillToAddLevel = (id) => {
    const { availableSkills, employeeSkills } = this.state
    availableSkills.forEach((skill, i) => {
      if (skill.id === id) {
        availableSkills.splice(i, 1)
        this.setDataInState(skill, true, availableSkills, employeeSkills)
      }
    })
  }

  editCurrentSkillToAddLevel = (id) => {
    const { availableSkills, employeeSkills } = this.state
    employeeSkills.forEach((tcpSkill, i) => {
      if (tcpSkill.skill.id === id) {
        employeeSkills.splice(i, 1)
        this.setDataInState(tcpSkill, true, availableSkills, employeeSkills)
      }
    })
  }

  deleteEmployeeTcpSkill = (id) => {
    const { availableSkills, employeeSkills } = this.state
    employeeSkills.forEach((tcpSkill, i) => {
      const { skill } = tcpSkill
      if (skill.id === id) {
        employeeSkills.splice(i, 1)
        availableSkills.push(skill)
        this.setDataInState(null, false, availableSkills, employeeSkills)
      }
    })
  }

  onSubmitTcpSkills = () => {
    const { employeeId, editEmployeeTcpSkills } = this.props
    const { employeeSkills } = this.state
    var tcpSkills = employeeSkills.map(tcpSkill => {
      const { id, level, skill } = tcpSkill
      var requestItem = { level, skillId: skill.id, employeeId }
      return id ? { id, ...requestItem } : requestItem
    })
    var data = {
      employee: {
        tcpSkillsAttributes: tcpSkills
      }
    }
    editEmployeeTcpSkills(employeeId, data)
  }

  render() {
    const { availableSkills, employeeSkills, selectedSkill, displayDialog } = this.state
    const { classes } = this.props
    return (
      <ValidatorForm onSubmit={this.onSubmitTcpSkills} className={classes.formContainer}>
        <Grid className={classes.skillsContainer} container>
          <Grid item md={5} xs={12} className={classes.skillListContainer}>
            <Label variant='headline' rowName='Available skills' value='' />
            {availableSkills &&
              <SkillList skills={availableSkills} formatter={formatSkill} buttonIcon='add_circle' itemAction={this.selectSkillToAddLevel} elevation={0} />
            }
          </Grid>
          <Grid item md={1} xs={12}></Grid>
          <Grid item md={5} xs={12} className={classes.skillListContainer}>
            <Label variant='headline' rowName='Career path skills' value='' />
            {employeeSkills &&
              <SkillList skills={employeeSkills} formatter={formatTcpSkill} buttonIcon='delete_circle' itemAction={this.deleteEmployeeTcpSkill} secondItemAction={this.editCurrentSkillToAddLevel} elevation={0} />
            }
          </Grid>
          <EmployeeSkillLevel isOpen={displayDialog} closeFunction={this.onCancelSkillSelection} confirmFunction={this.onAcceptlSkillSelection} skill={selectedSkill} />
        </Grid>
        <ActionsButtons />
      </ValidatorForm>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllSkills,
    editEmployeeTcpSkills
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    skillList: state.skills.all,
  }
}

const StyledEmployeeSkills = withStyles(employeeSkillStyles)(EmployeeSkills)
export default connect(mapStateToProps, mapDispatchToProps)(StyledEmployeeSkills)
