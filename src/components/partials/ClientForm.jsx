import { Grid, withStyles } from '@material-ui/core'
import formStyles from '../../assets/styles/formStyles'
import React from 'react'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import countryList from 'react-select-country-list'
import PropTypes from 'prop-types'
import AutocompleteSelect from './AutocompleteSelect'
import ActionsButtons from './ActionsButtons'
import classNames from 'classnames'

class ClientForm extends React.Component {

  options = countryList().getData()

  state = {
    client: {
      name: '',
      lastName: '',
      contactEmail: '',
      companyName: '',
      phoneNumber: '',
      country: ''
    },
    countries: this.options

  }

  componentWillReceiveProps(nextProps) {
    const { client } = nextProps
    if (client) {
      const { id, name, lastName, contactEmail, companyName, phoneNumber, country } = client
      this.setState(() => ({
        client: {
          id: id,
          name: name,
          lastName: lastName,
          contactEmail: contactEmail,
          companyName: companyName,
          phoneNumber: phoneNumber,
          country: {
            label: country,
            value: countryList().getValue(country)
          }
        }
      }))
    }
  }

  onChangeClientValues = (event) => {
    const { name, value } = event.target
    const { client } = this.state
    this.setState(() => ({
      client: { ...client, [name]: value }
    }))
  }

  onSubmitClient = (e) => {
    const { submitAction } = this.props
    let { client } = this.state
    client.country = client.country.label
    e.preventDefault()
    submitAction(client)
  }

  selectCountry = (event) => {
    const { client } = this.state
    this.setState(() => ({
      client: {
        ...client,
        country: {
          label: event.label,
          value: event.value,
        }
      }
    }
    ))
  }

  render() {
    const { classes } = this.props
    const { client: { name, lastName, contactEmail, companyName, phoneNumber, country }, countries } = this.state
    return (
      <ValidatorForm className={classes.container} noValidate autoComplete="off" onSubmit={this.onSubmitClient}>
        <Grid className={classNames(classes.inputContainer, classes.blockFormContainer)} container>
          <TextValidator
            name="name"
            label="Name"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeClientValues}
            className={classes.fullWidthInput}
            value={name}
            margin="normal" />
          <TextValidator
            name="lastName"
            label="Last name"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeClientValues}
            className={classes.fullWidthInput}
            value={lastName}
            margin="normal" />
          <TextValidator
            name="contactEmail"
            label="Contact email"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeClientValues}
            className={classes.fullWidthInput}
            value={contactEmail}
            margin="normal" />
          <TextValidator
            name="companyName"
            label="Company name"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeClientValues}
            className={classes.fullWidthInput}
            value={companyName}
            margin="normal" />
          <TextValidator
            name="phoneNumber"
            label="Phone number"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeClientValues}
            className={classes.fullWidthInput}
            value={phoneNumber}
            margin="normal" />
          <Grid container>
            <Grid item md={12}>
              <AutocompleteSelect
                label='Country'
                name='country'
                placeholder=""
                options={countries}
                onChange={this.selectCountry}
                value={country ? country : ''}
              />
            </Grid>
          </Grid>
        </Grid>
        <ActionsButtons previewPage='/clients' />
      </ValidatorForm >
    )
  }
}

ClientForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(formStyles)(ClientForm)
