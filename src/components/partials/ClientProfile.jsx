import React from 'react'
import { Grid, Paper, Button, CardActions, Tooltip, } from '@material-ui/core'
import InfoCard from './InfoCard';
import EditIcon from '@material-ui/icons/Edit';
import { history } from '../../helpers/history';
import Label from './Label';
import ClientIcon from '../../assets/icons/client.jpeg'

class ClientProfile extends React.Component {
  render() {
    const { classes, client } = this.props
    return (
      <Paper className={classes.root}>
        <Grid container className={classes.mainContainer}>
          <Grid item xs={12} md={10}>
            <InfoCard item={{
              icon: ClientIcon,
              first: client.name + ' ' + client.lastName,
              second: client.companyName
            }}
              classes={classes}>
              <Label variant='subheading' rowName='Contact information  ' value='' />
              <Label variant='subheading' rowName='Email:  ' value={client.contactEmail} />
              <Label variant='subheading' rowName='Phone: ' value={client.phoneNumber} />
              <Label variant='subheading' rowName='Country: ' value={client.country} />
            </InfoCard>
          </Grid>
          <Grid item xs={12} md={2}>
            <CardActions className={classes.profileActions}>
              <div>
                <Tooltip title='Edit' >
                  <Button variant='fab' color='secondary' mini onClick={() => history.push(`/clients/${client.id}/update`)} >
                    <EditIcon />
                  </Button>
                </Tooltip>
              </div>
            </CardActions>
          </ Grid>
        </Grid>
      </Paper>
    )
  }
}

export default ClientProfile
