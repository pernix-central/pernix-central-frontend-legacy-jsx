import React from 'react'
import { Typography, withStyles } from '@material-ui/core';

const labelStyles = theme => ({
  label: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
})

const Label = (props) => {
  const { classes, rowName, value, variant } = props
  return (
    <Typography variant={variant} color='textSecondary' noWrap={true} className={classes.label}>
      <b>{rowName}</b>{value}
    </Typography>
  )
}

export default withStyles(labelStyles)(Label)
