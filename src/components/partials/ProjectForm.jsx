import React from 'react'
import classNames from 'classnames'
import ActionsButtons from './ActionsButtons'
import { Grid, withStyles, MenuItem } from '@material-ui/core'
import formStyles from '../../assets/styles/formStyles'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PropTypes from 'prop-types';
import SelectValidator from 'react-material-ui-form-validator/lib/SelectValidator';
import AutocompleteSelect from '../partials/AutocompleteSelect'
import MultipleSelect from './MultipleSelect';

const statusOptions = ['On track', 'A head', 'Behind', 'Finished']

const ProjectForm = (props) => {
  const { classes, project, changeData, sendData, clientsList, selectClient, selectTeam, teams } = props
  const { name, status, startDate, endDate, description, clients, team } = project

  return (
    <ValidatorForm className={classes.container} noValidate autoComplete="off" onSubmit={sendData}>
      <Grid className={classNames(classes.blockFormContainer, classes.inputContainer)} container>
        <TextValidator
          name="name"
          label="Name"
          validators={['required']}
          errorMessages={['This field is required']}
          onChange={changeData}
          className={classes.fullWidthInput}
          value={name}
          margin="normal"
        />
        <SelectValidator
          id="status"
          name="status"
          label="Status"
          className={classes.fullWidthInput}
          onChange={changeData}
          value={status}
          validators={['required']}
          errorMessages={['This field is required']}
          inputProps={{
            name: 'status',
            id: 'status'
          }}
          margin="normal">
          <MenuItem value='' key=''><em>None</em></MenuItem>
          {statusOptions.map(option => {
            return (<MenuItem value={option} key={option}>{option}</MenuItem>)
          })}
        </SelectValidator>
        <TextValidator
          name="description"
          label="Description"
          errorMessages={['This field is required', '']}
          onChange={changeData}
          className={classes.fullWidthInput}
          value={description}
          margin="normal" />
        <TextValidator
          name="startDate"
          label="Start date"
          value={startDate}
          type='date'
          className={classes.fullWidthInput}
          validators={['required']}
          errorMessages={['This field is required']}
          onChange={changeData}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }} />
        <TextValidator
          name="endDate"
          label="End date"
          value={endDate}
          type="date"
          validators={['required', 'validateEndDate']}
          errorMessages={['This field is required', 'End date has to be after start date']}
          className={classes.fullWidthInput}
          onChange={changeData}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }} />
        <Grid container>
          <Grid item md={12}>
            <AutocompleteSelect
              label='Team'
              placeholder=''
              options={teams}
              name='team'
              onChange={selectTeam}
              value={team ? team : null}
            />
          </Grid>
        </Grid>
        <MultipleSelect
          suggestions={clientsList ? clientsList.map(
            client => { return { key: client.id, value: client.id, label: client.name + ' ' + client.lastName } }) : []
          }
          name='clients'
          label='Clients'
          onChange={selectClient}
          value={clients}
        />
      </Grid>
      <ActionsButtons previewPage='/projects' />
    </ValidatorForm >
  )
}

ProjectForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(formStyles)(ProjectForm)
