import React from 'react'
import { Grid, Paper, Icon, Typography, Button, CardActions, Tooltip, Tabs, Tab } from '@material-ui/core'
import Schedule from '../partials/Schedule';
import UserInfoCard from '../partials/UserInfoCard';
import { PERNIX_GREEN, ERROR } from '../../assets/styles/commonStyles';
import { history } from '../../helpers/history';
import SkillList from './SkillList';
import { formatTcpSkillWithoutAction } from '../../helpers/rowFormatters';
import permit from '../../helpers/permit';

const editeButton = (id) => {
  return (<Tooltip disableFocusListener title='Edit'>
    <Button variant='fab' color='secondary' mini onClick={() => { history.push(`/employees/${id}/update`) }} >
      <Icon>edit</Icon>
    </Button>
  </Tooltip>)
}

export const UserProfile = (props) => {
  const { classes, user, userInSession, handleChange, isActive, isCurrentUser, tab, onChangeAttribute } = props
  const { schedule: { workDays }, id, active } = user
  return (
    <Paper className={classes.root}>
      <CardActions className={classes.profileActionsUser}>
        <div>
          {isCurrentUser ? editeButton(id) : permit(userInSession, "Employee", "update") && editeButton(id)}
        </div>
      </CardActions>
      {!isCurrentUser &&
        <Typography
          variant='body2'
          color={active ? 'primary' : 'error'}
          className={classes.switchPosition}>
          <div>
            <Tooltip disableFocusListener title={isActive}>
              <Button variant='fab'
                style={{
                  backgroundColor: active ? PERNIX_GREEN : ERROR,
                  color: 'white',
                }}
                mini onClick={handleChange}
              >
                <Icon >settings_power</Icon>
              </Button>
            </Tooltip>
          </div>
        </Typography>
      }
      <Grid container >
        <Grid item xs={12} md={6} >
          <UserInfoCard user={user} classes={classes} />
        </Grid>
        <Grid item xs={12} md={5}>
          <Tabs
          value={tab}
            onChange={onChangeAttribute}
            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
          >
            <Tab
              classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
              label='SCHEDULE'
            />
            <Tab
              classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
              label='SKILLS'
            />
          </Tabs>
          {tab === 0
            ?
            <Schedule classes={classes} schedule={workDays} />
            :
            <SkillList skills={user.tcpSkills} formatter={formatTcpSkillWithoutAction} buttonIcon='' itemAction={() => { }} elevation={0} ready={true}/>
          }
        </Grid>
      </Grid>
    </Paper>
  )
}

