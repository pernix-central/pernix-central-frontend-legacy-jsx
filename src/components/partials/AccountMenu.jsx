import React from 'react'
import { IconButton, Menu, MenuItem, } from '@material-ui/core'
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AccountCircle from "../../assets/icons/circle-placeholder.png";
import { closeSession, updateUser } from '../../redux/modules/auth'
import { toggleNavbarDisplay, getEmployee } from "../../redux/modules/employee"
import { history } from '../../helpers/history';
import { getImage } from '../../helpers/imagesHelper';

class AccountMenu extends React.Component {
  state = {
    anchorEl: null,
    currentUser: null
  }

  static getDerivedStateFromProps(props, state) {
    const { getEmployee, updateUser, employee, user } = props
    if (!user) {
      return {
        ...state
      }
    } else if (!user.images) {
      if (!employee) {
        getEmployee(user.id);
      } else {
        updateUser(employee)
      }
      return {
        ...state
      }
    } else {
      return {
        currentUser: user,
      }
    }
  }

  handleMenu = (event) => {
    const anchorEl = event.currentTarget
    this.setState(() => ({ anchorEl }))
  }

  handleClose = () => {
    this.setState(() => ({ anchorEl: null }))
  }

  logout = () => {
    const { closeSession } = this.props
    const { toggleNavbarDisplay } = this.props
    toggleNavbarDisplay(false)
    closeSession()
    history.push('/login')
  }

  render() {
    const { classes } = this.props
    const { anchorEl, currentUser } = this.state
    const open = Boolean(anchorEl);
    return (
      <div>
        <IconButton
          aria-owns={open ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit">
          {currentUser && <img alt='user' className={classes.accountIcon} src={getImage(currentUser.images) || AccountCircle} />}
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          open={open}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          onClose={this.handleClose}>
          <MenuItem onClick={this.handleClose} >
            <NavLink className={classes.navItem} activeClassName={classes.selected} to='/'>
              Profile
            </NavLink>
          </MenuItem>
          <MenuItem onClick={this.logout}>
            Logout
          </MenuItem>
        </Menu>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeSession,
    toggleNavbarDisplay,
    getEmployee,
    updateUser
  }, dispatch)
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    employee: state.employee.currentEmployee
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AccountMenu))
