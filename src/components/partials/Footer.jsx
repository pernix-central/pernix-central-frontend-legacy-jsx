import React from 'react'
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import footerStyles from '../../assets/styles/footerStyles';

const Footer = (props) => {
  const classes = props.classes
  return (
    <div className={classes.footer}>
      <Typography component='p' className={classes.footerText}>Copyright {new Date().getFullYear()}. Pernix Solutions.</Typography>
    </div>
  )
}

export default withStyles(footerStyles)(Footer)
