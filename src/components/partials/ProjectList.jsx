import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableCell from '@material-ui/core/TableCell'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import PagedTableHead from './PagedTableHead';
import projectListStyles from '../../assets/styles/projectListStyles'
import { handleOrderDirection, getSortingOrder } from '../../helpers/sortHelper'
import { history } from '../../helpers/history';
import ReactPlaceholder from 'react-placeholder/lib';
import { TablePlaceholder } from './CustomPlaceholder';

class PagedTable extends React.Component {
  state = {
    orderDirection: 'asc',
    orderByProperty: 'name',
    page: 0,
    rowsPerPage: 5,
    view: false
  }

  changeSortOrder = (event, incominProperty) => {
    this.setState((preventState) => {
      const { orderDirection, orderByProperty } = preventState
      return handleOrderDirection(orderDirection, orderByProperty, incominProperty)
    })
  }

  onProjectClick = (projectId) => {
    history.replace('projects/' + projectId)
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  render() {
    const { classes, header, projectList, ready } = this.props
    const listSize = projectList ? projectList.length : 0; 
    const { orderDirection, orderByProperty, rowsPerPage, page } = this.state
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, listSize - page * rowsPerPage)
    return (
      <Paper className={classes.paper}>
        <div className={classes.tableWrapper}>
          <Table aria-labelledby='tableTitle'>
            <PagedTableHead
              order={orderDirection}
              orderBy={orderByProperty}
              onRequestSort={this.changeSortOrder}
              rowCount={listSize}
              columns={header} />
            <ReactPlaceholder showLoadingAnimation={true} delay={100000} ready={ready || Boolean(projectList)} customPlaceholder={<TablePlaceholder propertiesCount={4}></TablePlaceholder>}> 
            <TableBody>
                {projectList && projectList
                .sort(getSortingOrder(orderDirection, orderByProperty))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(project => {
                  return (
                    <TableRow
                      hover
                      key={project.id}
                      tabIndex={-1}
                      id={project.id}
                      onClick={() => { this.onProjectClick(project.id) }}>
                      <TableCell> {project.name} </TableCell>
                      <TableCell> {project.status} </TableCell>
                      <TableCell> {project.startDate} </TableCell>
                      <TableCell> {project.endDate} </TableCell>
                    </TableRow>
                  )
                }, this)}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            </ReactPlaceholder>
          </Table>
        </div>
        <TablePagination
          component='div'
          count={listSize}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage} />
      </Paper>
    )
  }
}

PagedTable.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(projectListStyles)(PagedTable)
