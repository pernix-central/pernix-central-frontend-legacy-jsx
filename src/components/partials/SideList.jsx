import React from 'react'
import { List, withStyles, Divider } from '@material-ui/core';
import permit from '../../helpers/permit';
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import sideListStyles from '../../assets/styles/sideListSyles'

class SideList extends React.Component{

  render(){
    const { classes, user } = this.props
  return (
    <div className={classes.list}>
      <List>
        {permit(user, 'Employee', 'index') &&
        <NavLink className={classes.sideBarItem} activeClassName = {classes.selected} to='/employees'>
          <i className={classes.icon + ' material-icons'}>supervisor_account</i>
          <span className={classes.sidebarLabel}>Employees</span>
        </NavLink>}
        <Divider/>
        {permit(user, 'Scoreboard', 'index') &&
        <NavLink className={classes.sideBarItem} activeClassName = {classes.selected} to='/scoreboard/list'>
          <i className={classes.icon + ' material-icons'}>equalizer</i>
          <span className={classes.sidebarLabel}>Scoreboard</span>
        </NavLink>}
        <Divider/>
        {permit(user, 'Activity', 'index') &&
          <NavLink className={classes.sideBarItem} activeClassName={classes.selected} to='/activities'>
          <i className={classes.icon + ' material-icons'}>track_changes</i>
            <span className={classes.sidebarLabel}>Activities</span>
          </NavLink>}
          <Divider/>
        {permit(user, 'Skill', 'index') &&
          <NavLink className={classes.sideBarItem} activeClassName={classes.selected} to='/skills'>
          <i className={classes.icon + ' material-icons'}>assessment</i>
          <span className={classes.sidebarLabel}>Skills</span>
        </NavLink>}
        <Divider/>
        {permit(user, 'Client', 'index') &&
          <NavLink className={classes.sideBarItem} activeClassName={classes.selected} to='/clients'>
          <i className={classes.icon + ' material-icons'}>business</i>
            <span className={classes.sidebarLabel}>Clients</span>
          </NavLink>}
          <Divider/>
        {permit(user, 'Project', 'index') &&
        <NavLink className={classes.sideBarItem} activeClassName = {classes.selected} to='/projects'>
          <i className={classes.icon + ' material-icons'}>assignment</i>
          <span className={classes.sidebarLabel}>Projects</span>
        </NavLink>}
        <Divider/>
        {permit(user, 'Team', 'index') &&
        <NavLink className={classes.sideBarItem} activeClassName = {classes.selected} to='/teams'>
          <i className={classes.icon + ' material-icons'}>supervised_user_circle</i>
          <span className={classes.sidebarLabel}>Teams</span>
        </NavLink>}
        <Divider/>
      </List>
    </div>
  )
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
  }
}

const StyledSideList = withStyles(sideListStyles)(SideList)
const SideListWithRedux = connect(mapStateToProps)(StyledSideList)

export default withRouter(SideListWithRedux);