import { Grid, withStyles, Checkbox, MenuItem, FormControlLabel, TextField, Button, Icon } from '@material-ui/core';
import classNames from 'classnames';
import formStyles from '../../assets/styles/formStyles'
import React from 'react';
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';
import ActionsButtons from './ActionsButtons'
import AccountCircle from '../../assets/icons/circle-placeholder.png'

const EmployeeForm = (props) => {
  const { classes, user, changeData, sendData, editable, imagePreview } = props
  const { firstName, lastName, birthdate, email, password, passwordConfirmation, hasCar, blogUrl,
    canBeMentor, onVacation, computerSerialNumber, macAddress, hireDate, role } = user
  return (
    <ValidatorForm className={classes.container} noValidate autoComplete="off" onSubmit={sendData}>
      <Grid className={classes.inputContainer} container >
        <Grid item md={6} >
          <Grid container>
            <Grid item md={4}>
              <img src={imagePreview || AccountCircle} alt='Img preview' className={classes.imagePreview} />
            </Grid>
            <Grid item md={8}>
              <TextValidator
                id="firstName"
                name="firstName"
                required
                label="Name"
                validators={['required']}
                errorMessages={['This field is required']}
                className={classes.textField}
                onChange={changeData}
                value={firstName}
                margin="normal"
              />
              <TextValidator
                id="lastName"
                name="lastName"
                label="Last Name"
                required
                validators={['required']}
                errorMessages={['This field is required']}
                className={classes.textField}
                onChange={changeData}
                value={lastName}
                margin="normal"
              />
              <TextValidator
                id="email"
                name="email"
                label="Email"
                type="email"
                required
                validators={['required', 'isEmail']}
                errorMessages={['This field is required', 'Email is not valid']}
                className={classes.textField}
                onChange={changeData}
                value={email}
                margin="normal"
              />
            </Grid>
          </Grid>
          <TextValidator
            id="birthdate"
            name="birthdate"
            label="Birthdate"
            required
            type='date'
            validators={['required', 'isValidDate']}
            errorMessages={['This field is required', 'This date is no valid']}
            value={birthdate}
            className={classes.textField}
            onChange={changeData}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }} />
          <TextField
            id="macAddress"
            name="macAddress"
            label="MAC address"
            value={macAddress}
            className={classes.textField}
            onChange={changeData}
            margin="normal"
          />
          <TextField
            id="computerSerialNumber"
            name="computerSerialNumber"
            label="Computer Serial Number"
            value={computerSerialNumber}
            className={classes.textField}
            onChange={changeData}
            margin="normal"
          />

        </Grid>
        <Grid item md={6} >
          <SelectValidator
            name="role"
            label="Role"
            required
            className={classNames(classes.textField, classes.selectField)}
            onChange={changeData}
            value={role}
            validators={['required']}
            errorMessages={['This field is required']}
            inputProps={{
              name: 'role',
              id: 'role',
            }}
            margin="normal"
          >
            <MenuItem value=""><em>None</em></MenuItem>
            <MenuItem value="Admin">Admin</MenuItem>
            <MenuItem value="Apprentice">Apprentice</MenuItem>
            <MenuItem value="Crafter">Crafter</MenuItem>
          </SelectValidator>
          {editable &&
            <div>
              <TextValidator
                id="password"
                name="password"
                label="Password"
                type='password'
                required
                className={classes.textField}
                validators={['required']}
                errorMessages={['This field is required']}
                value={password}
                onChange={changeData}
                margin="normal"
              />
              <TextValidator
                id="passwordConfirmation"
                name="passwordConfirmation"
                label="Password Confirmation"
                type='password'
                required
                className={classes.textField}
                value={passwordConfirmation}
                validators={['required', 'isPasswordMatch']}
                errorMessages={['This field is required', 'Password mismatch']}
                onChange={changeData}
                margin="normal"
              />
            </div>
          }

          <Grid container >
            <Grid item md={8} xs={12}>
              <TextField
                id="blogUrl"
                name="blogUrl"
                label="Blog's url"
                className={classNames(classes.textFieldFix, classes.textField)}
                value={blogUrl}
                onChange={changeData}
                margin="normal"
              />
              <TextValidator
                id="hireDate"
                name="hireDate"
                label="Hire date"
                value={hireDate}
                type="date"
                required
                validators={['required', 'isValidDate']}
                errorMessages={['This field is required', 'This date is no valid']}
                className={classes.textField}
                onChange={changeData}
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <FormControlLabel
                className={classes.textField}
                control={
                  <Checkbox
                    id="hasCar"
                    name="hasCar"
                    checked={hasCar}
                    onChange={changeData}
                    color={'primary'}
                    classes={{
                      root: classes.checkbox,
                    }}
                  />
                }
                label="Has Car"
              />
              <FormControlLabel
                className={classNames(classes.textField, classes.checkbox)}
                control={
                  <Checkbox
                    id="canBeMentor"
                    name="canBeMentor"
                    checked={canBeMentor}
                    onChange={changeData}
                    color={'primary'}
                    classes={{
                      root: classes.checkbox,
                    }}
                  />
                }
                label="Mentor"
              />
              <FormControlLabel
                className={classes.textField}
                control={
                  <Checkbox
                    id="onVacation"
                    name="onVacation"
                    checked={onVacation}
                    onChange={changeData}
                    color={'primary'}
                    classes={{
                      root: classes.checkbox,
                    }}
                  />}
                label="On vacation"
              />
            </Grid>
            <Grid item md={12}>
              <TextField
                id="image"
                name="attachment"
                type='file'
                style={{ display: 'none' }}
                onChange={changeData}
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                  text: 'Chose your image'
                }}
              />
              <label htmlFor="image">
                <Button component='span' className={classes.fileChooser}>
                  <Icon>attachment</Icon> <p className={classes.mleft}>Choose image</p>
                </Button>
              </label>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <ActionsButtons />
    </ValidatorForm>
  )
}

EmployeeForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(formStyles)(EmployeeForm);
