import React from 'react'
import { connect } from 'react-redux'
import {
  Avatar, List, Divider, ListItemText, ListItem, ListItemSecondaryAction, Typography, Grid, Button,
  TextField, DialogActions, DialogContent, DialogTitle
} from '@material-ui/core'
import { addNewComment, editComment } from '../../redux/modules/comments'
import { getAllEmployees } from '../../redux/modules/employee'
import { bindActionCreators } from 'redux'
import CommentsMenu from './CommentsMenu';
import moment from 'moment'
class CommentList extends React.Component {
  state = {
    comment: {
      projectId: null,
      teamId: null,
      employeeId: null,
      text: ''
    },
    userWillEditComment: false,
    displayCancelBtn: false,
    currentEmployeeName: null,
    employees: null
  }

  resetComment = () => {
    const { comment } = this.state
    this.setState((state) => ({
      ...state,
      comment: { ...comment, text: '' },
      displayCancelBtn: false
    }))
  }

  onChangeComment = (event) => {
    const { comment } = this.state
    const { name, value } = event.target
    this.setState(() => ({
      comment: { ...comment, [name]: value },
      displayCancelBtn: Boolean(value.length > 0)
    }))
  }

  getAuthorAvatar = (name) => {
    const { employees } = this.state
    var image = null
    if (employees) {
      employees.forEach(employee => {
        if (name === `${employee.firstName} ${employee.lastName}`) {
          const { images } = employee
          image = images[0] ? images[0].medium : null
          return
        }
      })
    }
    return image
  }

  static getDerivedStateFromProps(props, state) {
    const { projectId, teamId, currentEmployeeName, employeeId, employees, getAllEmployees } = props;
    const { comment } = state
    if (!employees) {
      getAllEmployees()
    }
    return { ...state, comment: { ...comment, projectId, teamId, employeeId }, currentEmployeeName, employees }
  }

  sendComment = () => {
    const { addNewComment, editComment } = this.props
    const { comment, userWillEditComment } = this.state
    if (userWillEditComment) {
      editComment({ comment: { id: comment.id, text: comment.text }, projectId: comment.projectId, teamId: comment.teamId })
      this.setState((state) => ({
        ...state,
        comment: {
          projectId: null,
          teamId: null,
          employeeId: null,
          text: ''
        },
        userWillEditComment: false,
      }))
    } else {
      addNewComment(comment)
    }
    this.resetComment()
  }

  setCommentToEdit = (comment) => {
    this.setState(() => ({
      comment,
      displayCancelBtn: true,
      userWillEditComment: true
    }))
  }

  render() {
    const { comments, employees } = this.props
    const { comment: { text }, currentEmployeeName, displayCancelBtn, userWillEditComment } = this.state
    return (
      <div>
        <DialogTitle id='form-dialog-title'>Comments</DialogTitle>
        <DialogContent>
          <List aria-labelledby='tableTitle'>
            {employees && comments
              .map(commentItem => {
                return (
                  <div key={commentItem.id} >
                    <ListItem >
                      <Grid container>
                        <Grid item container md={12} xs={12}>
                          <Grid item md={1}>
                            <Avatar>
                              <img src={this.getAuthorAvatar(commentItem.author)} alt='' width='35' height='35' />
                            </Avatar>
                          </Grid>
                          <Grid item container md={11}>
                            <Grid item md={6} xs={12}>
                              <Typography variant="subheading" color='textSecondary' noWrap={true}>
                                <b>{commentItem.author}</b>
                              </Typography>
                            </Grid>
                            <Grid item md={12} xs={12}>
                              <ListItemText primary={commentItem.text} secondary={
                                <Typography variant="subheading" color='textSecondary' noWrap={true}>
                                  {moment(commentItem.createdAt).format('MM/DD/YYYY HH:mm')}
                                </Typography>
                              } />
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                      {currentEmployeeName === commentItem.author && <ListItemSecondaryAction>
                        <CommentsMenu comment={commentItem} setComment={this.setCommentToEdit}></CommentsMenu>
                      </ListItemSecondaryAction>}
                    </ListItem>
                    <li>
                      <Divider />
                    </li>
                  </div>
                )
              })}
          </List>
          <TextField
            margin='dense'
            name='text'
            value={text}
            label='Comment'
            multiline={true}
            onChange={this.onChangeComment}
            fullWidth />

        </DialogContent>
        <DialogActions>
          {displayCancelBtn && <Button onClick={this.resetComment} color='error'>
            Cancel
            </Button>}
          <Button onClick={this.sendComment} color='primary'>
            {userWillEditComment ? 'Edit' : 'Send'}
          </Button>
        </DialogActions>
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addNewComment,
    getAllEmployees,
    editComment
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    currentEmployeeName: state.auth.user.firstName + ' ' + state.auth.user.lastName,
    employees: state.employee.all,
    employeeId: state.auth.user.id
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentList)
