import React from 'react'
import { Paper, withStyles, Typography, Grid } from '@material-ui/core';
import commonStyles from '../../assets/styles/commonStyles';

class Container extends React.Component {
  render() {
    const { classes, title, body, children, button, elevation} = this.props;
    return (
      <div className={classes.containerPaper}>
        <Paper className={classes.root} elevation={elevation || 0}>
          <Grid container>
            <Grid item md={10} xs={6}>
              <Typography variant='display1' component='h3'>
                {title}
              </Typography>
            </Grid>
            <Grid item md={2} xs={6} container className={classes.flexEnd}>
              {button}
            </Grid>
          </Grid>
          <Typography component='p'>
            {body}
          </Typography>
          {children}
        </Paper>
      </div>
    )
  }
}

export default withStyles(commonStyles)(Container)
