import { Grid, withStyles, MenuItem } from '@material-ui/core'
import formStyles from '../../assets/styles/formStyles'
import React from 'react'
import classNames from 'classnames'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PropTypes from 'prop-types'
import ActionsButtons from './ActionsButtons'
import SelectValidator from 'react-material-ui-form-validator/lib/SelectValidator'

class SkillForm extends React.Component {

  state = {
    skill: {
      name: '',
      skillType: '',
      level: '0'
    },
  }

  componentWillReceiveProps(nextProps) {
    const { currentSkill } = nextProps
    if (currentSkill) {
      this.setState(() => ({
        skill: currentSkill
      }))
    }
  }

  onChangeSkillValues = (event) => {
    const { name, value } = event.target
    const { skill } = this.state
    this.setState(() => ({
      skill: { ...skill, [name]: value }
    }))
  }

  onSubmitSkill = (e) => {
    const { submitAction } = this.props
    let { skill } = this.state
    e.preventDefault()
    submitAction(skill)
  }

  render() {
    const { classes } = this.props
    const { skill: { name, skillType } } = this.state
    return (
      <ValidatorForm className={classes.container} noValidate autoComplete="off" onSubmit={this.onSubmitSkill}>
        <Grid className={classNames(classes.inputContainer, classes.blockFormContainer)} container>
          <TextValidator
            name="name"
            label="Name"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeSkillValues}
            className={classes.fullWidthInput}
            value={name}
            margin="normal" />
          <SelectValidator
            name="skillType"
            label="Type"
            className={classes.fullWidthInput}
            onChange={this.onChangeSkillValues}
            value={skillType}
            validators={['required']}
            errorMessages={['This field is required']}
            inputProps={{
              name: 'skillType',
              id: 'skillType'
            }}
            margin="normal">
            <MenuItem value='soft'>
              Soft Skill
            </MenuItem>
            <MenuItem value='tech'>
              Tech Skill
            </MenuItem>
          </SelectValidator>
        </Grid>
        <ActionsButtons previewPage='/skills' />
      </ValidatorForm >
    )
  }
}

SkillForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(formStyles)(SkillForm)
