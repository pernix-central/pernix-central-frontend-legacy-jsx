import React from 'react'
import PropTypes from 'prop-types'
import TablePagination from '@material-ui/core/TablePagination'
import Paper from '@material-ui/core/Paper'
import teamListStyles from '../../assets/styles/teamListStyles'
import GroupIcon from '../../assets/icons/group.png';
import { Avatar, ListItemText, ListItem, ListItemSecondaryAction, IconButton, Icon, List, Divider, Tooltip, withStyles } from '@material-ui/core';
import { history } from '../../helpers/history';
import { ListPlaceholder } from './CustomPlaceholder';
import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';

class TeamList extends React.Component {
  state = {
    page: 0,
    rowsPerPage: 5,
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  sendTo = (e, url) => {
    e.stopPropagation()
    history.push(url)
  }

  render() {
    const { classes, teams, ready } = this.props;
    const { rowsPerPage, page } = this.state
    const listSize = teams ? teams.length : 0;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, listSize - page * rowsPerPage)
    return (
      <Paper className={classes.paper}>
        <div className={classes.tableWrapper}>
          <List aria-labelledby='tableTitle'>
            <ReactPlaceholder showLoadingAnimation={true} delay={2000} ready={ready || Boolean(teams)} customPlaceholder={<ListPlaceholder />}>
              {teams && teams
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(team => {
                  return (
                    <div
                      id={team.id}
                      key={team.id}
                      onClick={e => {
                        this.sendTo(e, `/teams/${team.id}/`);
                      }}>
                      <ListItem>
                        <Avatar>
                          <img src={GroupIcon} alt='' width='35' height='35' />
                        </Avatar>
                        <ListItemText primary={team.name} />
                        <ListItemSecondaryAction>
                          <Tooltip title='Edit'>
                            <IconButton
                              aria-label='Comments'
                              className={classes.stylesLink}
                              onClick={e => {
                                this.sendTo(e, `/teams/${team.id}/update`);
                              }}>
                              <Icon color='primary'>edit_icon</Icon>
                            </IconButton>
                          </Tooltip>
                        </ListItemSecondaryAction>
                      </ListItem>
                      <li>
                        <Divider />
                      </li>
                    </div>
                  );
                }, this)}
              {emptyRows > 0 && <ListItem style={{ height: 49 * emptyRows }}>
                <span colSpan={6} />
              </ListItem>}
            </ReactPlaceholder>
          </List>
        </div>
        <TablePagination
          component='div'
          count={listSize}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    )
  }
}

TeamList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(teamListStyles)(TeamList)
