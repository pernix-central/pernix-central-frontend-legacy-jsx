import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames';
import { history } from '../../helpers/history'
import { withStyles, Button } from '@material-ui/core'
import actionsButtonsStyle from '../../assets/styles/actionsButtonsStyle'

class ActionsButtons extends React.Component {
  goBack = () => {
    history.goBack()
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.buttonsContainer}>
        <Button variant="outlined" className={classNames(classes.action, classes.back)} onClick={this.goBack}>
          Back
        </Button>
        <Button variant="outlined" type='submit' className={classNames(classes.action, classes.submit)}>
          Submit
        </Button>
      </div>
    )
  }
}

ActionsButtons.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(actionsButtonsStyle)(ActionsButtons);
