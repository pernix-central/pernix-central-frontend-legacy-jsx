import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import pagedTableStyles from '../../assets/styles/pagedTableStyles'
import AccountCircle from '../../assets/icons/circle-placeholder.png';
import { Avatar, ListItemText, ListItemSecondaryAction, IconButton, Icon, List, ListItem } from '@material-ui/core';
import { history } from '../../helpers/history';
import classNames from 'classnames'
import { getImage } from '../../helpers/imagesHelper';

class SimpleTable extends React.Component {

  showEmployeeProfile = (employeeId, isEditing) =>
    !isEditing ? history.push('/employees/' + employeeId) : null

  render() {
    const { classes, objectList, isEditing, isToAdd, onItem } = this.props
    return (
      <div>
        <div className={classNames(classes.simpleTable, classes.tableWrapper, classes.tableBorder)}>
          {(objectList.length > 0) &&
            <List aria-labelledby="tableTitle">
              {objectList.map(object => {
                return (
                  <ListItem id={object.id} key={object.id} >
                    <Avatar>
                      <img src={getImage(object.images) || AccountCircle} alt='' width='40' height='40' />
                    </Avatar>
                    <ListItemText primary={object.firstName + ' ' + object.lastName} />
                    {isEditing &&
                      <ListItemSecondaryAction>
                        <IconButton onClick={onItem} value={object.id} className={classes.button}>
                          {isToAdd ?
                            <Icon color='primary'>add_circle</Icon> :
                            <Icon color='error'>remove_circle</Icon>}
                        </IconButton>
                      </ListItemSecondaryAction>
                    }
                  </ListItem>
                )
              }, this)}
            </List>
          }
        </div>
      </div>
    )
  }
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(pagedTableStyles)(SimpleTable)
