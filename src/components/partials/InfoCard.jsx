import React from 'react'
import { Card, CardContent, CardMedia, withStyles } from '@material-ui/core';
import Label from './Label';
import profileStyles from '../../assets/styles/profileStyles'

const InfoCard = (props) => {
  const { classes, item, labels, children } = props
  return (
    <div>
      <Card className={classes.card}>
        <CardMedia
          className={classes.cover}
          image={item.icon}
          title='Team cover' />
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Label variant='headline' rowName={labels ? labels[0] : ''} value={item.first} />
            <Label variant='subheading' rowName={labels ? labels[1] : ''} value={item.second} />
          </CardContent>
        </div>
      </Card>
      <Card className={classes.cardInfo}>
        <CardContent className={classes.content}>
          {children}
        </CardContent>
      </Card>
    </div>
  )
}

export default withStyles(profileStyles)(InfoCard)
