import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class SchedulePeriodForm extends React.Component {
  state = {
    startTime: '08:00',
    endTime: '17:00'
  }

  onChangePeriod = (event) => {
    const { name, value } = event.target
    this.setState(() => ({
      [name]: value
    }))
  }

  render() {
    const { isOpen, closeFunction, confirmFunction } = this.props
    const { startTime, endTime } = this.state
    return (
      <div>
        <Dialog
          open={isOpen}
          onClose={closeFunction}
          aria-labelledby="form-dialog-title" >
          <DialogTitle id="form-dialog-title">Add new work period</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              name="startTime"
              value={startTime}
              label="Start"
              type="time"
              onChange={this.onChangePeriod}
              fullWidth />
            <TextField
              autoFocus
              margin="dense"
              name="endTime"
              value={endTime}
              label="End"
              type="time"
              onChange={this.onChangePeriod}
              fullWidth />
          </DialogContent>
          <DialogActions>
            <Button onClick={closeFunction} color="primary">
              Cancel
            </Button>
            <Button onClick={() => confirmFunction({ startTime, endTime })} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default SchedulePeriodForm
