import React from 'react'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PropTypes from 'prop-types'
import { withStyles, TextField, InputAdornment, Icon, Typography, Grid } from '@material-ui/core'
import formStyles from '../../assets/styles/formStyles'
import classNames from 'classnames'
import SimpleTable from './SimpleTable'
import Container from './Container'
import ActionsButtons from './ActionsButtons'

class TeamForm extends React.Component {

  render() {
    const { classes, title, teamData, changeData, sendData, onFilter, editing, deleteItem, addItem } = this.props
    const { team, availableEmployees, selectedEmployees, filter } = teamData
    const { name } = team
    return (
      <Container title={title}>
        <ValidatorForm className={classes.container} noValidate autoComplete='off' onSubmit={sendData}>
          <div className={classes.container}>
            <TextValidator
              id='teamName'
              name='name'
              label='Team name'
              validators={['required']}
              errorMessages={['This field is required']}
              value={name}
              className={classes.fullWidthInput}
              onChange={changeData}
              margin='normal'
            />

            <div className={classes.fullWidthInput}>
              <Grid container>
                <Grid item md={5}>
                  <TextField
                    label='Search members'
                    name="employeeIds"
                    value={filter}
                    className={classNames(classes.textField, classes.searchMembers)}
                    onChange={onFilter}
                    margin='normal'
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <Icon>search</Icon>
                        </InputAdornment>
                      ),
                    }} />
                </Grid>
                <Grid item md={7} />
                <Grid item md={5}>
                  <div className={classes.marginTable}>
                    <SimpleTable
                      objectList={availableEmployees ? availableEmployees : []} isEditing={editing} isToAdd={true}
                      onItem={addItem}
                    />
                  </div>
                </Grid>
                <Grid item md={2} className={classes.arrowsMargin}>
                  <Typography variant='display3' align='center'>
                    <Icon fontSize='inherit'>arrow_forward_icon</Icon>
                    <br />
                    <Icon fontSize='inherit'>arrow_back_icon</Icon>
                  </Typography>
                </Grid>
                <Grid item md={5} >
                  <div className={classNames(classes.secondTable)}>
                    <SimpleTable
                      objectList={selectedEmployees ? selectedEmployees : []}
                      isEditing={editing} isToAdd={false} onItem={deleteItem}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
          <ActionsButtons previewPage='/teams' />
        </ValidatorForm>
      </Container>

    )
  }
}

TeamForm.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}


export default withStyles(formStyles, { withTheme: true })(TeamForm)
