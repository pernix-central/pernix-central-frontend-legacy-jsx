import React from 'react' 
import avatar from '../../assets/icons/circle-placeholder-small.png' 
import classNames from 'classnames' 
import scoreboardEmployeeStyles from '../../assets/styles/scoreboardEmployeeStyles';
import { withStyles } from '@material-ui/core';
import { getImage } from '../../helpers/imagesHelper';

const ScoreboardEmployee = (props) => {
  const { medalClass, employee, position, classes, ribbon, size } = props
  const { totalScore, firstName, lastName, images } = employee
  return (
    <div className={classes.center}>
      <div>
        <img 
          className={classNames(classes.avatarBorder,medalClass)} 
          src={getImage(images) || avatar}  
          height={size}
          width={size}
          alt='avatars' />
        <p className={classes.avatarPosition}>{position}</p>
      </div>
        <p className={classes.avatarFullName}>{firstName + ' ' + lastName}</p>
        <img className={classes.avatarRibbon} alt='' src={ribbon} />
      <p className={classes.points}>{!!totalScore ?  totalScore :  0 } pts</p>
    </div>
  )
}
export default withStyles(scoreboardEmployeeStyles)(ScoreboardEmployee)