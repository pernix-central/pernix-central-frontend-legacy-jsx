import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { history } from '../../helpers/history';
import classNames from 'classnames';
import { ListItemText, Menu } from '@material-ui/core';
import ActivityIcon from '../../assets/icons/activities.png'
import PresentationViewIcon from '../../assets/icons/presentacion_focus.png'
import ListViewIcon from '../../assets/icons/normal_focus.png'

class ScoreboardMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  sendTo = (url) => {
    this.setState({ anchorEl: null });
    history.push(url)
  };

  getClass = () => {
    const { navItem, classActive } = this.props
    const { location: { pathname } } = history
    var className = null
    switch (pathname) {
      case '/scoreboard/presentation':
        className = classNames(navItem, classActive)
        break;
      case '/scoreboard/list':
        className = classNames(navItem, classActive)
        break;
      case '/activities':
        className = classNames(navItem, classActive)
        break;
      default:
        className = navItem
        break;
    }
    return className
  }
  render() {
    const { anchorEl } = this.state;
    return (
      <a className={this.getClass()}>
        <label
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup='true'
          onClick={this.handleClick}>
          Scoreboard
        </label>
        <Menu
          id='simple-menu'
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.sendTo}
        >
          <MenuItem onClick={() => this.sendTo('/activities')}>
            <img alt='activity icon' height='20' width='20' src={ActivityIcon} />
            <ListItemText primary='Activities' />
          </MenuItem>
          <MenuItem onClick={() => this.sendTo('/scoreboard/list')}>
            <img alt='activity icon' height='20' width='20' src={ListViewIcon} />
            <ListItemText primary='ScoreBoard' /> </MenuItem>
          <MenuItem onClick={() => this.sendTo('/scoreboard/presentation')}>
            <img alt='activity icon' height='20' width='20' src={PresentationViewIcon} />
            <ListItemText primary='ScoreBoard' /> </MenuItem>
        </Menu>
      </a>
    );
  }
}

export default ScoreboardMenu;
