import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import participantsTableStyles from '../../assets/styles/partcipantsTableStyles'
import AccountCircle from '../../assets/icons/circle-placeholder.png';
import { Avatar, ListItemText, IconButton, Icon, List, ListItem, ListItemSecondaryAction, Checkbox, FormControlLabel } from '@material-ui/core';
import classNames from 'classnames'
import { getImage } from '../../helpers/imagesHelper';

class ParticipantsTable extends React.Component {

  render() {
    const { classes, objectList, isEditing, isToAdd, onItem, onCheck } = this.props
    return (
      <div>
        <div className={classNames(classes.simpleTable, classes.tableWrapper, classes.tableBorder)}>
          {(objectList.length > 0) &&
            <List aria-labelledby="tableTitle">
              {objectList.map(object => {
                return (
                  <ListItem key={object.id} >
                    <Avatar>
                      <img src={getImage(object.images) || AccountCircle} alt='' width='40' height='40' />
                    </Avatar>
                    <ListItemText primary={object.firstName + ' ' + object.lastName} className={classes.itemHead} />
                    <div className={classes.item}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="isOwner"
                            value={String(object.id)}
                            color='primary'
                            onChange={onCheck}
                          />}
                        label="Is Owner?"
                      />
                    </div>
                    {isEditing &&
                      <ListItemSecondaryAction>
                        <IconButton onClick={onItem} value={object.id} className={classes.button}>
                          {isToAdd ?
                            <Icon color='primary'>add_circle</Icon> :
                            <Icon color='error'>remove_circle</Icon>}
                        </IconButton>
                      </ListItemSecondaryAction>
                    }
                  </ListItem>
                )
              })}
            </List>
          }
        </div>
      </div>
    )
  }
}

ParticipantsTable.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(participantsTableStyles)(ParticipantsTable)
