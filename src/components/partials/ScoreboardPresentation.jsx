import React from 'react'
import ScoreboardEmployee from './ScoreboardEmployee'
import Avatar from '../../assets/icons/circle-placeholder.png'
import { withStyles, Badge } from '@material-ui/core'
import scoreboardPresentationStyles from '../../assets/styles/scoreboardPresentationStyles'
import classNames from 'classnames'
import RibbonBronce from '../../assets/icons/ribon_bronce.png'
import RibbonGold from '../../assets/icons/ribon_gold.png'
import RibbonSilver from '../../assets/icons/ribon_silver.png'
import { getImage } from '../../helpers/imagesHelper';

const ViewHolder = {
  totalScore: 0,
  firstName: 'Participant',
  lastName: 'name',
  images: null 
}

const ScoreboardPresentation = (props) => {
  const { leaders, employees, classes } = props
  var cont = 4
  return (
    <div>
      <div className={classes.firstPositionsContainer}>
        <ScoreboardEmployee position='2' employee={leaders[1] || ViewHolder} medalClass={classes.avatarP2} ribbon={RibbonSilver}  />
        <ScoreboardEmployee position='1' employee={leaders[0] || ViewHolder} medalClass={classes.avatarP1} ribbon={RibbonGold} />
        <ScoreboardEmployee position='3' employee={leaders[2] || ViewHolder} medalClass={classes.avatarP3} ribbon={RibbonBronce} />
      </div>
      <div className={classes.lastPositionsContainer}>
        <ul className={classes.ul}>
          {employees.map((user, key) => {
            return (
              <li className={classNames(classes.avatarItem, classes.li)} key={key}>
                <Badge className={classes.root} badgeContent={
                  <span className={classes.positionBadge}>{cont++}</span>
                }>
                  <img src={getImage(user.images) || Avatar} className={classes.avatar} alt='avatars' />
                </Badge>
                <div className={classes.nameContainer}>
                  <p className={classes.avatarName}>{user.firstName + ' ' + user.lastName}<br />{user.totalScore + ' pts'}</p>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}

export default withStyles(scoreboardPresentationStyles)(ScoreboardPresentation)
