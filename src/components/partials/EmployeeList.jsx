import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TablePagination from '@material-ui/core/TablePagination'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Paper from '@material-ui/core/Paper'
import employeeListStyles from '../../assets/styles/employeeListStyles'
import AccountCircle from '../../assets/icons/circle-placeholder.png'
import {
  Avatar, ListItemText, ListItem, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails,
  ListItemSecondaryAction, IconButton, Icon, Card, MenuItem, Tooltip
} from '@material-ui/core'
import { history } from '../../helpers/history'
import CustomChips from './CustomChips'
import Label from './Label'
import { Button, Typography } from '@material-ui/core';
import permit from '../../helpers/permit';
import { filterEmployeesBySkills, searchSkill } from '../../helpers/listsHandler';
import EmployeeListFilters from './EmployeeListFilters';
import { getImage } from '../../helpers/imagesHelper';
import { ListPlaceholder } from '../partials/CustomPlaceholder';
import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';

class EmployeeList extends React.Component {

  state = {
    page: 0,
    rowsPerPage: 5,
    searchFilter: '',
    filterList: [],
    skillFilters: [],
    selectedEmployee: null
  }

  onButonAddPress = () => {
    history.push('/signup')
  }

  showEmployeeProfile = (employeeId) => {
    history.push('/employees/' + employeeId)
  }

  sendTo = (url) => {
    history.push(url)
  }

  setFilterName = (event) => {
    const { value } = event.target
    const { skills } = this.props
    var filterRequiredUpdate = Boolean(value.length > 0)
    this.setState((state) => ({
      ...state,
      searchFilter: value,
      applyFilter: filterRequiredUpdate,
      skillFilters: filterRequiredUpdate ? searchSkill(value, skills) : []
    }))
  }

  addFilter = (skill) => {
    this.setState((state) => {
      const { filterList } = state
      return {
        ...state,
        searchFilter: '',
        applyFilter: false,
        filterList: [...filterList, skill]
      }
    });
  };

  deleteFilter = (skillId) => {
    const { filterList } = this.state
    filterList.forEach((skill, i) => {
      if (skill.id === skillId) {
        filterList.splice(i, 1)
      }
    });
    this.setState((state) => ({
      ...state,
      filterList
    }));
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  render() {
    const { classes, objectList, user } = this.props
    const { rowsPerPage, page, searchFilter, filterList, skillFilters, applyFilter } = this.state
    var filteredList = filterEmployeesBySkills(objectList, filterList)
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredList.length - page * rowsPerPage)
    return (
      <div>
        <div className={classes.newEmployee}>
          <div className={classes.searchBar}>
            <Icon>search</Icon>
            <input className={classes.inputRoot} onChange={this.setFilterName} value={searchFilter} placeholder='Search by skills' />
          </div>
          {permit(user, 'Employee', 'create') &&
            <Button variant='outlined' color='primary' onClick={this.onButonAddPress}>
              <Typography variant='body2'>
                New Employee
              </Typography>
            </Button>}
        </div>
        <div className={classes.searchResultsContainer}>
          {applyFilter &&
            skillFilters.map((skill, i) =>
              <MenuItem key={i} onClick={() => this.addFilter(skill)}>{skill.name}</MenuItem>
            )}
        </div>
        <EmployeeListFilters skillFilters={filterList ? filterList : []} onDeleteFilter={this.deleteFilter}></EmployeeListFilters>
        <Paper className={classes.paper}>
          <div className={classes.tableWrapper}>
            <Card aria-labelledby='tableTitle'>
              <ReactPlaceholder showLoadingAnimation={true} ready={objectList.length > 0} customPlaceholder={<ListPlaceholder></ListPlaceholder>}> 
              {filteredList
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(object => {
                  return (
                    <ExpansionPanel id={object.id} key={object.id}>
                      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Avatar>
                          <img src={getImage(object.images) || AccountCircle} alt='' width='40' height='40' />
                        </Avatar>
                        <ListItemText primary={object.firstName + ' ' + object.lastName} secondary={object.email} />
                        <ListItemSecondaryAction>
                          <Tooltip title='Edit'>
                            <IconButton aria-label='Comments' className={classes.stylesLink} onClick={() => this.sendTo(`employees/${object.id}/update`)}>
                              <Icon color='primary'>edit_icon</Icon>
                            </IconButton>
                          </Tooltip>
                        </ListItemSecondaryAction>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails className={classes.panel} onClick={() => this.showEmployeeProfile(object.id)}>
                        <CustomChips label='Mentor' attribute={object.canBeMentor} />
                        <CustomChips label='Vacation' attribute={object.onVacation} />
                        <CustomChips label='Has car' attribute={object.hasCar} />
                        <Label variant='body2' rowName='Hire date:  ' value={object.hireDate} />
                        <Label variant='body2' rowName='Birthdate:  ' value={object.birthdate} />
                        <Label variant='body2' rowName='Mac Address:  ' value={object.macAddress} />
                        <Label variant='body2' rowName='Computer serial: ' value={object.computerSerialNumber} />
                        <Label variant='body2' rowName='Blog:  ' value={object.blogUrl} />
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  )
                }, this)}
              {emptyRows > 0 && (
                <ListItem style={{ height: 55 * emptyRows }}>
                  <span colSpan={6} />
                </ListItem>
              )}
              </ReactPlaceholder>
            </Card>
          </div>
          <TablePagination
            component='div'
            count={filteredList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    )
  }
}

EmployeeList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(employeeListStyles)(EmployeeList)
