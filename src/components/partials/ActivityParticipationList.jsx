import React from 'react'
import Activity from '../../assets/icons/activities.png'
import Retreat from '../../assets/icons/retreat.png'
import Clubs from '../../assets/icons/clubs.png'
import Workshop from '../../assets/icons/workshop.png'
import { List, ListItem, ListItemAvatar, ListItemText, Avatar } from '@material-ui/core';
import ActivityParticipationDetail from './ActivityParticipationDetail';

const ActivitiyParticipationList = (props) => {
  const { participations } = props

  const getIcon = (activityName) => {
    var variant = activityName ? activityName.toLowerCase() : ''
    if (variant.toLowerCase().includes('retreat')) {
      return Retreat
    } else if (variant.toLowerCase().includes('workshop')) {
      return Workshop
    } else if (variant.toLowerCase().includes('club')) {
      return Clubs
    } else {
      return Activity
    }
  }

  return (
    <List>
      {participations.map((activity, i) =>
        <ListItem key={i}>
          <ListItemAvatar>
            <Avatar src={getIcon(activity.name)} alt='activity icon' height='30' width='30' />
          </ListItemAvatar>
          <ListItemText
            primary={<ActivityParticipationDetail activity={activity} />} />
        </ListItem>
      )}
    </List>
  )
}
export default ActivitiyParticipationList
