import React from 'react'
import { withStyles } from '@material-ui/core';
import activityParticipationDetailStyles from '../../assets/styles/activityParticipationDetailStyles'

const ActivityParticipationDetail = (props) => {
  const { classes, activity } = props
  return (
    <div className={classes.detailContainer}>
      <span className={classes.activityName}>{activity.name}</span>
      <hr className={classes.dottedSparatorLine}></hr>
      <span className={classes.activityPoints}>{activity.points + ' pts'}</span>
    </div>
  )
}

export default withStyles(activityParticipationDetailStyles)(ActivityParticipationDetail)
