import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableCell from '@material-ui/core/TableCell'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import PagedTableHead from './PagedTableHead';
import pagedTableStyles from '../../assets/styles/pagedTableStyles'
import { handleOrderDirection, getSortingOrder } from '../../helpers/sortHelper'
import { history } from '../../helpers/history';
import ReactPlaceholder from 'react-placeholder/lib';
import { TablePlaceholder } from './CustomPlaceholder';

class ClientList extends React.Component {
  state = {
    orderDirection: 'asc',
    orderByProperty: 'name',
    page: 0,
    rowsPerPage: 5,
    view: false
  }

  changeSortOrder = (event, incominProperty) => {
    this.setState((preventState) => {
      const { orderDirection, orderByProperty } = preventState
      return handleOrderDirection(orderDirection, orderByProperty, incominProperty)
    })
  }

  onclientClick = (clientId) => {
    history.replace('clients/' + clientId)
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  render() {
    const { classes, header, clientList, ready } = this.props;
    const { orderDirection, orderByProperty, rowsPerPage, page } = this.state
    const listSize = clientList ? clientList.length : 0;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, listSize - page * rowsPerPage)
    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table aria-labelledby='tableTitle'>
            <PagedTableHead
              order={orderDirection}
              orderBy={orderByProperty}
              onRequestSort={this.changeSortOrder}
              rowCount={listSize}
              columns={header} />
            <ReactPlaceholder showLoadingAnimation={true} delay={100000} ready={ready || Boolean(clientList)} customPlaceholder={<TablePlaceholder propertiesCount={5} />}>
              <TableBody>
                {clientList && clientList
                  .sort(getSortingOrder(orderDirection, orderByProperty))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(client => {
                    return <TableRow hover key={client.id} tabIndex={-1} id={client.id} onClick={() => {
                      this.onclientClick(client.id);
                    }}>
                      <TableCell>
                        {' '}
                        {client.name + ' ' + client.lastName}{' '}
                      </TableCell>
                      <TableCell> {client.contactEmail} </TableCell>
                      <TableCell> {client.phoneNumber} </TableCell>
                      <TableCell> {client.companyName} </TableCell>
                      <TableCell> {client.country} </TableCell>
                    </TableRow>;
                  }, this)}
                {emptyRows > 0 && <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>}
              </TableBody>
            </ReactPlaceholder>
          </Table>
        </div>
        <TablePagination
          component='div'
          count={listSize}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{ 'aria-label': 'Previous Page' }}
          nextIconButtonProps={{ 'aria-label': 'Next Page' }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    )
  }
}

ClientList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(pagedTableStyles)(ClientList)
