import React from 'react'
import classNames from 'classnames'
import { Grid, withStyles, MenuItem } from '@material-ui/core'
import formStyles from '../../assets/styles/formStyles'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PropTypes from 'prop-types'
import SelectValidator from 'react-material-ui-form-validator/lib/SelectValidator'
import ActionsButtons from './ActionsButtons'

class ActivityForm extends React.Component {

  constructor(props) {
    super(props)
    const { activity } = props
    this.state = {
      currentActivity: activity ? activity : {
        name: '',
        frequency: '',
        points: '',
      },
      wasActivityLoad: false,
      frequencyOptions: ['Monthly', 'Weekly', 'Daily', 'Biweekly'],
    }
  }

  onChangeActivityValues = (event) => {
    const { name, value } = event.target
    const { currentActivity } = this.state
    this.setState(() => ({
      currentActivity: { ...currentActivity, [name]: value }
    }))
  }

  onSubmitActivity = (e) => {
    e.stopPropagation()
    const { submitAction } = this.props
    let { id, name, frequency, points } = this.state.currentActivity
    let activity = {
      activity: {
        id: id,
        name: name,
        frequency: frequency,
        points: points,
      }
    }
    submitAction(activity)
  }

  render() {
    const { classes } = this.props
    const { currentActivity, frequencyOptions } = this.state
    return (
      <ValidatorForm className={classes.container} noValidate autoComplete="off" onSubmit={this.onSubmitActivity}>
        <Grid className={classNames(classes.inputContainer, classes.blockFormContainer)} container>
          <TextValidator
            name="name"
            label="Name"
            validators={['required']}
            errorMessages={['This field is required']}
            onChange={this.onChangeActivityValues}
            className={classes.fullWidthInput}
            value={currentActivity.name}
            margin="normal" />
          <SelectValidator
            name="frequency"
            label="Frequency"
            className={classes.fullWidthInput}
            onChange={this.onChangeActivityValues}
            value={currentActivity.frequency}
            validators={['required']}
            errorMessages={['This field is required']}
            inputProps={{
              name: 'frequency',
              id: 'frequency'
            }}
            margin="normal">
            {
              frequencyOptions.map((option, i) =>
                <MenuItem key={i} value={option}>
                  {option}
                </MenuItem>
              )
            }
          </SelectValidator>
          <TextValidator
            name="points"
            label="Points"
            validators={['required', 'isNumber', 'minNumber:0', 'matchRegexp:^[0-9]*$']}
            errorMessages={['This field is required', 'It should be a number']}
            onChange={this.onChangeActivityValues}
            className={classes.fullWidthInput}
            value={currentActivity.points}
            margin="normal" />
        </Grid>
        <ActionsButtons previewPage='/activities' />
      </ValidatorForm >
    )
  }
}

ActivityForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(formStyles)(ActivityForm)
