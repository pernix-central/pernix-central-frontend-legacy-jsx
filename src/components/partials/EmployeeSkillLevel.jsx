import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Avatar, withStyles } from '@material-ui/core';
import { PERNIX_GREEN } from '../../assets/styles/commonStyles';

const employeeSkillLevelStyles = {
  levelsContainer: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  selected: {
    background: PERNIX_GREEN
  }
}

const addLevel = (level, skill) => {
  if (skill.name) {
    return {
      level,
      skill
    }
  } else {
    return {
      ...skill,
      level
    }
  }
}

const selectClass = (classes, level, skill) => {
  if (skill.name) {
    return {}
  }
  if (level === skill.level) {
    return classes.selected
  } else {
    return {}
  }


}

const EmployeeSkillLevel = (props) => {
  const { isOpen, closeFunction, confirmFunction, skill, classes } = props
  const availableLevels = ['1', '2', '3']
  return (
    <div>
      {skill && <Dialog
        open={isOpen}
        onClose={closeFunction}
        aria-labelledby='form-dialog-title' >
        <DialogTitle id='form-dialog-title'>Select domain level</DialogTitle>
        <DialogContent className={classes.levelsContainer}>
          {availableLevels.map(level =>
            <Avatar className={selectClass(classes, level, skill)}
              onClick={
                () => confirmFunction(addLevel(level, skill))
              }>
              {level}
            </Avatar>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={() => closeFunction(skill)} color='primary'>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>}
    </div>
  );

}

export default withStyles(employeeSkillLevelStyles)(EmployeeSkillLevel)
