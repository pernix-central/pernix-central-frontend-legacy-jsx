import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Paper from '@material-ui/core/Paper';
import { PERNIX_BLUE, PERNIX_GREEN } from '../../assets/styles/commonStyles';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'start',
    flexWrap: 'wrap',
    padding: theme.spacing.unit / 2,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
  softSkill: {
    margin: theme.spacing.unit / 2,
    color: 'white',
    fontWeight: 'bold',
    background: PERNIX_BLUE
  },
  techSkill: {
    color: 'white',
    fontWeight: 'bold',
    margin: theme.spacing.unit / 2,
    background: PERNIX_GREEN
  },
});

const EmployeeListFilters = (props) => {

  const { classes, onDeleteFilter, skillFilters } = props;
  return (
    <Paper className={classes.root} elevation={0}>
      {skillFilters.map((data, i) =>
        <Chip
          key={i}
          label={data.name}
          onDelete={() => onDeleteFilter(data.id)}
          className={data.skillType === 'soft' ? classes.softSkill : classes.techSkill} />
      )}
    </Paper>
  );

}

EmployeeListFilters.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EmployeeListFilters);
