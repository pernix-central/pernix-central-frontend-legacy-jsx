import React from 'react'
import { Grid, ListItem, Avatar, ListItemText, withStyles, Divider, FormControl, List, Icon, ListItemSecondaryAction, IconButton, Badge, } from '@material-ui/core';
import { TextValidator } from 'react-material-ui-form-validator';
import scheduleStyles from '../../assets/styles/scheduleStyles';
import SchedulePeriodForm from './SchedulePeriodForm';
import { ValidatorForm } from 'react-form-validator-core';
import { editSchedule, getSchedule } from '../../redux/modules/schedule';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ActionsButtons from './ActionsButtons';

class ScheduleForm extends React.Component {
  state = {
    schedule: null,
    isDialogOpen: false,
    currentWorkDayId: null
  }

  componentWillMount() {
    const { userId, scheduleId, getSchedule } = this.props
    const { schedule } = this.state
    if (!schedule) {
      getSchedule(userId, scheduleId)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentSchedule } = nextProps
    if (currentSchedule) {
      this.setState(() => ({
        schedule: currentSchedule
      }))
    }
  }

  updatePeriodsInWorkDay = (event, workDay, periodId) => {
    const { name, value } = event.target
    const { periods } = workDay
    const updatedPeriods = periods.map(period =>
      period.id === periodId ? { ...period, [name]: value } : period
    )
    return { ...workDay, periods: updatedPeriods }
  }

  onChangeScheduleValues = (event, workDayId, periodId) => {
    const { schedule } = this.state
    const { workDays } = schedule
    const updatedWorkDays = workDays.map(workDay => {
      return workDay.id === workDayId
        ? this.updatePeriodsInWorkDay(event, workDay, periodId)
        : workDay
    })
    this.setState(() => ({
      schedule: { ...schedule, workDays: updatedWorkDays }
    }))
  }

  onChangeWorkFromHome = (workDayId) => {
    const { schedule } = this.state
    const { workDays } = schedule
    const updatedWorkDays = workDays.map(workDay =>
      workDay.id === workDayId
        ? workDay.wfh ? { ...workDay, wfh: false } : { ...workDay, wfh: true }
        : workDay
    )
    this.setState(() => ({
      schedule: { ...schedule, workDays: updatedWorkDays }
    }))
  }

  setCurrentWorkDay = (currentWorkDayId) => {
    this.setState(() => ({
      currentWorkDayId
    }))
    this.openClosePeriodForm()
  }

  addNewPeriodToSchedule = (newPeriod) => {
    const { schedule, currentWorkDayId } = this.state
    const { workDays } = schedule
    const updatedWorkDays = workDays.map(workDay =>
      workDay.id === currentWorkDayId ? { ...workDay, periods: [...workDay.periods, newPeriod] } : workDay
    )
    this.setState(() => ({
      schedule: { ...schedule, workDays: updatedWorkDays }
    }))
    this.openClosePeriodForm()
  }

  getPeriodsWithoutDeletedElement = (periods, periodId) => {
    return periods.map(period =>
      period.id === periodId ? { ...period, _destroy: true } : period
    )
  }

  deletePeriodInSchedule = (workDayId, periodId) => {
    const { schedule } = this.state
    const { workDays } = schedule
    const updatedWorkDays = workDays.map(workDay =>
      workDay.id === workDayId
        ? {
          ...workDay, periods: this.getPeriodsWithoutDeletedElement(
            workDay.periods,
            periodId
          )
        }
        : workDay
    )
    this.setState(() => ({
      schedule: { ...schedule, workDays: updatedWorkDays }
    }))
  }

  onSubmitForm = (e) => {
    e.preventDefault();
    const { userId, editSchedule } = this.props
    var { schedule } = this.state
    schedule = JSON.parse(JSON.stringify(schedule).replace("\"workDays\":", "\"workDaysAttributes\":"))
    schedule.workDaysAttributes = schedule.workDaysAttributes.map(workDay =>
      JSON.parse(JSON.stringify(workDay).replace("\"periods\":", "\"periodsAttributes\":"))
    )
    editSchedule(userId, schedule)
  }

  openClosePeriodForm = () => {
    const { isDialogOpen } = this.state
    this.setState(() => ({
      isDialogOpen: !isDialogOpen
    }))
  }

  render() {
    const { classes } = this.props
    const { schedule, isDialogOpen } = this.state
    return (
      <ValidatorForm className={classes.mainContainer} noValidate autoComplete="off" onSubmit={this.onSubmitForm} >
        <Grid container className={classes.scheduleContainer} >
          <List className={classes.fullWith}>
            {schedule &&
              schedule.workDays.map(workDay =>
                <div key={workDay.weekDay}>
                  <ListItem>
                    <Avatar
                      className={workDay.wfh ? classes.active : ''}
                      onClick={() => this.onChangeWorkFromHome(workDay.id)}>
                      <Icon>home</Icon>
                    </Avatar>
                    <ListItemText
                      className={classes.noMinWidth}
                      primary={workDay.weekDay}
                      secondary={workDay.wfh ? 'In home' : 'In office'} />
                    <Grid container>
                      {workDay.periods ?
                        workDay.periods.map(period =>
                          !period._destroy && <Grid key={period.id} item md={4} className={classes.colunmGrid}>
                            <Badge
                              className={classes.badge}
                              badgeContent={
                                <Icon className={classes.deletePeriod} onClick={() => this.deletePeriodInSchedule(workDay.id, period.id)}>delete</Icon>
                              }
                              color="error">
                              <FormControl className={classes.periodInputs}>
                                <TextValidator
                                  name="startTime"
                                  label="Start"
                                  className={classes.periods}
                                  value={period.startTime}
                                  type='time'
                                  validators={['required']}
                                  errorMessages={['This field is required']}
                                  onChange={(event) => this.onChangeScheduleValues(event, workDay.id, period.id)} />
                                <TextValidator
                                  name="endTime"
                                  label='End'
                                  className={classes.periods}
                                  value={period.endTime}
                                  type='time'
                                  validators={['required']}
                                  errorMessages={['This field is required']}
                                  onChange={(event) => { this.onChangeScheduleValues(event, workDay.id, period.id) }} />
                              </FormControl>
                            </Badge>
                          </Grid>
                        )
                        : 'TBD'}
                    </Grid>
                    <ListItemSecondaryAction>
                      <IconButton onClick={() => this.setCurrentWorkDay(workDay.id)}>
                        <Icon color='primary'>add</Icon>
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                  <li>
                    <Divider inset />
                  </li>
                </div>
              )
            }
          </List>
          <SchedulePeriodForm
            isOpen={isDialogOpen}
            closeFunction={this.openClosePeriodForm}
            confirmFunction={this.addNewPeriodToSchedule} />
        </Grid>
        <ActionsButtons />
      </ValidatorForm>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    editSchedule,
    getSchedule
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    status: state.schedule.status,
    currentSchedule: state.schedule.currentSchedule
  }
}
const StyledScheduleForm = withStyles(scheduleStyles)(ScheduleForm)
export default connect(mapStateToProps, mapDispatchToProps)(StyledScheduleForm)
