import React from 'react'
import { Grid, Paper, List, Button, CardActions, Divider, Tooltip } from '@material-ui/core'
import InfoCard from './InfoCard';
import EmployeeListItem from './EmployeeListItem';
import GroupIcon from '../../assets/icons/group.png'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { history } from '../../helpers/history';
import CommentList from './CommentList';

export const TeamProfile = (props) => {
  const { classes, team, onDelete } = props
  return (
    <Paper className={classes.root} elevation={0}>
      <Grid container >
        <Grid item xs={12} md={10}>
          <InfoCard item={{
            icon: GroupIcon,
            first: team.name,
            second: team.employees.length
          }}
            labels={["Team: ", "Members: "]}
            classes={classes} />
        </Grid>
        <Grid item xs={12} md={2}>
          <CardActions className={classes.profileActions}>
            <div>
              <Tooltip title='Edit'>
                <Button variant='fab' color='secondary' mini onClick={() => { history.push(`/teams/${team.id}/update`) }}>
                  <EditIcon />
                </Button>
              </Tooltip>
            </div>
            <div>
              <Tooltip title='Delete'>
                <Button onClick={onDelete} variant='fab' className={classes.cancelButton} mini color='primary'>
                  <DeleteIcon />
                </Button>
              </Tooltip>
            </div>
          </CardActions>
        </Grid>
      </Grid>
      <div className={classes.teamMemberList}>
        <List>
          {team.employees.map(employee =>
            <EmployeeListItem key={employee.id} deletable={false} employeeItem={employee} />
          )}
        </List>
        <Divider></Divider>
        <CommentList
          teamId={team.id}
          projectId={null}
          comments={team.comments || []} />
      </div>
    </Paper>
  )
}

