import React from 'react'
import { TextRow, RoundShape } from 'react-placeholder/lib/placeholders';
import { Avatar, ListItem, Divider, TableCell, TableRow, TableBody } from '@material-ui/core';

export const ListPlaceholder = (props) => {
  const holders = []
  for (let i = 0; i < 5; i++) {
    holders.push(
    <div key={i}>
      <ListItem>
        <Avatar>
          <RoundShape color='#E0E0E0' style={{ width: 50, height: 50 }} />
        </Avatar>
        <div style={{display: 'flex', flexDirection: 'column',padding: 2 }}>
          <TextRow color='#E0E0E0' style={{ width: 100, margin: 2 }} />
          <TextRow color='#E0E0E0' style={{ width: 200, margin: 2 }} />
        </div>
        </ListItem>
      <Divider />
    </div>
    )
  }
  return (
    <div>
      {holders.map(child => child)}
    </div>)
}

export const TablePlaceholder = (props) => {
  const { propertiesCount } = props
  const properties = []
  for (let i = 0; i < propertiesCount; i++) {
    properties.push(
      <TableCell key={i}> <TextRow color='#E0E0E0' style={{ width: 100 }} /></TableCell>
    )
  }
  const holders = []
  for (var i = 0; i < 5; i++) {
    holders.push(
      <TableRow hover key={i} tabIndex={-1} >
        {properties.map(child => child)}
      </TableRow>
    )
  }
  return (
    <TableBody>
      {holders.map(child => child)}
    </TableBody>
  )
}