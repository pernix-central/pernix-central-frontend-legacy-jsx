import React from 'react'
import { IconButton, Menu, MenuItem } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert';

class CommentsMenu extends React.Component {
  state = {
    anchorEl: null,
    comment: null
  }

  handleMenu = (event) => {
    const anchorEl = event.currentTarget
    this.setState(() => ({ anchorEl }))
  }

  handleClose = () => {
    this.setState(() => ({ anchorEl: null }))
  }

  putCommentInEdition = () => {
    const { comment, setComment} = this.props
    setComment(comment)
    this.handleClose()
  }
  
  render() {
    
    const { anchorEl } = this.state
    const open = Boolean(anchorEl);
    return (
      <div>
        <IconButton
          aria-owns={open ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit">
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          open={open}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          onClose={this.handleClose}>
          <MenuItem onClick={this.putCommentInEdition} >
              Edit
          </MenuItem>
          <MenuItem >
            Delete
          </MenuItem>
        </Menu>
      </div>
    )
  }
}

export default CommentsMenu