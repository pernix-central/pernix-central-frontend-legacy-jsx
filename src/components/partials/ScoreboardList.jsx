import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TablePagination from '@material-ui/core/TablePagination'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Paper from '@material-ui/core/Paper'
import pagedTableStyles from '../../assets/styles/pagedTableStyles'
import AccountCircle from '../../assets/icons/circle-placeholder.png';
import { Avatar, ListItemText, ListItem, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Card } from '@material-ui/core';
import ActivitiyParticipationList from './ActivityParticipationList';
import classNames from 'classnames';
import { getImage } from '../../helpers/imagesHelper';

class ScoreboardList extends React.Component {
  state = {
    page: 0,
    rowsPerPage: 5,
    selectedEmployee: null
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  getPositionClassName = (position) => {
    const { classes } = this.props
    const { positionBorder, position1, position2, position3, otherPositions } = classes
    const { page } = this.state
    if( page > 0) return classNames(positionBorder, otherPositions)  
    switch (position) {
      case 0:
        return classNames(positionBorder, position1)
      case 1:
        return classNames(positionBorder, position2)
      case 2:
        return classNames(positionBorder, position3)
      default:
        return classNames(positionBorder, otherPositions)  
    }

  }
  render() {
    const { classes, employeeList } = this.props
    const { rowsPerPage, page } = this.state
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, employeeList.length - page * rowsPerPage)
    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Card aria-labelledby="tableTitle">
            {employeeList
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((employee, i) => {
                return (
                  <ExpansionPanel id={employee.id} key={employee.id} className={this.getPositionClassName(i)}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.noPadding}>
                      <span className={classes.scoreboardEmployeePosition}>{i + 1}</span>
                      <Avatar>
                        <img src={getImage(employee.images) || AccountCircle} alt='' width='40' height='40' />
                      </Avatar>
                      <ListItemText primary={employee.firstName + ' ' + employee.lastName} secondary={employee.role} className={classes.scoreboardEmployeeInfo} />
                      <div className={classes.scoreboardPoints}>
                        {(employee.totalScore || '0') + ' pts'}
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.panel}>
                      <ActivitiyParticipationList participations={employee.activities} />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                )
              }, this)}
            {emptyRows > 0 && (
              <ListItem style={{ height: 49 * emptyRows }}>
                <span colSpan={6} />
              </ListItem>
            )}
          </Card>
        </div>
        <TablePagination
          component="div"
          count={employeeList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    )
  }
}

ScoreboardList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(pagedTableStyles)(ScoreboardList)
