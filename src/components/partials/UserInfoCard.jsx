import React from 'react'
import { Card, CardContent, CardMedia, withStyles } from '@material-ui/core';
import Label from './Label';
import CustomChips from './CustomChips'
import AccountCircle from '../../assets/icons/circle-placeholder.png'
import profileStyles from '../../assets/styles/profileStyles'
import { getImage } from '../../helpers/imagesHelper';

const UserInfoCard = (props) => {
  const { classes, user } = props

  const { firstName, lastName, birthdate, email, hasCar, blogUrl,
    canBeMentor, onVacation, computerSerialNumber, macAddress, hireDate, role, images } = user
    return (
    <div>
      <Card className={classes.card}>
        <CardMedia
            className={classes.coverCircle}
          image={ getImage(images)|| AccountCircle}
          title='Acount cover' />
        <div>
          <CardContent className={classes.content}>
            <Label variant='headline' rowName={firstName + ' ' + lastName} value='' />
            <Label variant='subheading' rowName='' value={email} />
            <Label variant='subheading' rowName='' value={role} />
          </CardContent>
        </div>
      </Card>
      <Card className={classes.card}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <CustomChips label='Mentor' attribute={canBeMentor} />
            <CustomChips label='Vacation' attribute={onVacation} />
            <CustomChips label='Has car' attribute={hasCar} />
            <Label variant='subheading' rowName='Hire date:  ' value={hireDate} />
            <Label variant='subheading' rowName='Birthdate:  ' value={birthdate} />
            <Label variant='subheading' rowName='Mac Address:  ' value={macAddress} />
            <Label variant='subheading' rowName='Computer serial: ' value={computerSerialNumber} />
            <Label variant='subheading' rowName='Blog:  ' value={blogUrl} />
          </CardContent>
        </div>
      </Card>
    </div>
  )
}

export default withStyles(profileStyles)(UserInfoCard)
