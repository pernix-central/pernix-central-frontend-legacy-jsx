import React from 'react'
import PropTypes from 'prop-types'
import TablePagination from '@material-ui/core/TablePagination'
import Paper from '@material-ui/core/Paper'
import skillListStyles from '../../assets/styles/skillListStyles'
import { Avatar, ListItemText, ListItem, ListItemSecondaryAction, IconButton, Icon, List, Divider, Tooltip, withStyles } from '@material-ui/core';
import { history } from '../../helpers/history';
import { ListPlaceholder } from './CustomPlaceholder';
import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';

class SkillList extends React.Component {
  state = {
    page: 0,
    rowsPerPage: 5,
  }

  showTeamProfile = (skillId) => {
    history.push(`skills/${skillId}`)
  }

  handleChangePage = (event, page) => {
    this.setState(() => ({ page }))
  }

  handleChangeRowsPerPage = event => {
    this.setState(() => ({ rowsPerPage: event.target.value }))
  }

  render() {
    const { classes, skills, buttonIcon, itemAction, secondItemAction, formatter, elevation, ready } = this.props
    const { rowsPerPage, page } = this.state
    const listSize = skills ? skills.length : 0;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, listSize - page * rowsPerPage)
    return (
        <Paper className={elevation===0 ? classes.paper: null} elevation={elevation}>
          <div className={classes.tableWrapper}>
            <List aria-labelledby='tableTitle'>
            <ReactPlaceholder showLoadingAnimation={true} delay={2000} ready={ready || Boolean(skills)} customPlaceholder={<ListPlaceholder ></ListPlaceholder>}> 
              {skills && skills
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(skill => {
                  const { id, avatar, textPrimary, textSecondary, tcpSkillId } = formatter(skill)
                  return (
                    <div key={id}>
                      <ListItem key={id}>
                        {textSecondary === 'soft'
                          ? <Avatar className={classes.softSkill}>{avatar}</Avatar>
                          : <Avatar className={classes.techSkill}>{avatar}</Avatar>
                        }
                        <ListItemText primary={textPrimary} secondary={textSecondary} />
                        <ListItemSecondaryAction>
                          <Tooltip title='Edit'>
                            <IconButton aria-label='Comments' onClick={
                              () => tcpSkillId ? secondItemAction(id) : itemAction(id)}>
                              <Icon color={(tcpSkillId ? 'edit_icon' : buttonIcon) === 'delete_circle' ? 'error' : 'primary'}>{tcpSkillId ? 'edit_icon' : buttonIcon}</Icon>
                            </IconButton>
                          </Tooltip>
                        </ListItemSecondaryAction>
                      </ListItem>
                      <li>
                        <Divider />
                      </li>
                    </div>
                  )
                }, this)}
              {emptyRows > 0 && (
                <ListItem style={{ height: 70 * emptyRows }}>
                  <span colSpan={6} />
                </ListItem>
              )}
              </ReactPlaceholder>
            </List>
          </div>
          <TablePagination
            component='div'
            count={listSize}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
    )
  }
}

SkillList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(skillListStyles)(SkillList)
