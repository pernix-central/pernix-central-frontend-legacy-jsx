import React from 'react'
import { ListItem, Avatar, ListItemText, Divider } from '@material-ui/core';
import AccountCircle from '../../assets/icons/circle-placeholder.png';
import { getImage } from '../../helpers/imagesHelper';

const EmployeeListItem = (props) => {
  const { employeeItem } = props
  const { images, lastName, firstName, email } = employeeItem
  return (
    <div>
      <ListItem>
      <Avatar>
          <img src={getImage(images) || AccountCircle} alt='' width='40' height='40' />
      </Avatar>
      <ListItemText primary={firstName + ' ' + lastName} secondary={email} />
    </ListItem>
     <Divider/>
    </div>
    
  )
}

export default EmployeeListItem