import React from 'react'
import { Grid, List, ListItem, Avatar, ListItemText, Divider } from '@material-ui/core';

const Schedule = (props) => {
  const { classes, schedule } = props
  return (
    <Grid container >
      <List className={classes.scheduleContainer}>
        {
          schedule.map(workDay =>
            <div key={workDay.weekDay}>
              <ListItem>
                <Avatar className={workDay.wfh ? classes.active : ''}>
                  {workDay.weekDay[0]}
                </Avatar>
                <ListItemText
                  primary={workDay.weekDay}
                  secondary={
                    workDay.periods ?
                      workDay.periods.map(period =>
                        period.endTime + '-' + period.startTime + ' '
                      ) : 'TBD'
                  }
                />
              </ListItem>
              <li>
                <Divider inset />
              </li>
            </div>
          )
        }
      </List>
    </Grid>
  )
}

export default Schedule
