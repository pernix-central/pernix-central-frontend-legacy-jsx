import React from 'react'
import { Chip, Avatar, withStyles } from "@material-ui/core"

const chipsStyles = theme =>({
  chip: {
    margin: 5,
  },
  active: {
    background: theme.palette.primary.main,
    color: 'white'
  }
})

const CustomChips = (props) => {
  const { classes, attribute, label } = props
  return (
    <Chip
      avatar={
        <Avatar>
          {label[0]}
        </Avatar>
      }
      label={label}
      className={
        attribute ? classes.active : classes.chip
      }
    />
  )
}

export default withStyles(chipsStyles)(CustomChips)