import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { NavLink, withRouter } from 'react-router-dom';
import navBarStyle from '../../assets/styles/navBarStyles'
import logo from '../../assets/icons/pernix_central_logo.png'
import { connect } from 'react-redux'
import permit from '../../helpers/permit'
import AccountMenu from './AccountMenu';
import { history } from '../../helpers/history';
import classNames from 'classnames';
import ScoreboardMenu from './ScoreboardMenu';

class NavBar extends React.Component {

  toHome = (e) => {
    history.replace('/')
  }
  render() {
    const { classes, user, navBarDisplay } = this.props
    return (
      <div className={classes.root}>
        <AppBar position='static' color='inherit' className={navBarDisplay ? classNames(classes.sticky, classes.showToolbar) : classNames(classes.sticky, classes.hideToolbar)}>
          {navBarDisplay &&
            <Toolbar>
              <IconButton className={classes.menuButton} color='inherit' aria-label='Menu' onClick={this.toHome}>
                <img alt='pernix-logo' src={logo} className={classes.appBarIcon} />
              </IconButton>
              <div className={classes.navLiks}>
                {permit(user, 'Employee', 'index') && <NavLink className={classes.navItem} activeClassName={classes.selected} to='/employees'>Employees</NavLink>}
                {permit(user, 'Scoreboard', 'index') && <ScoreboardMenu navItem={classes.navItem} classActive={classes.selected} />}
                {permit(user, 'Project', 'index') && <NavLink className={classes.navItem} activeClassName={classes.selected} to='/projects'>Projects</NavLink>}
                {permit(user, 'Team', 'index') && <NavLink className={classes.navItem} activeClassName={classes.selected} to='/teams'>Teams</NavLink>}
                {permit(user, 'Skill', 'index') && <NavLink className={classes.navItem} activeClassName={classes.selected} to='/skills'>Skills</NavLink>}
                {permit(user, 'Client', 'index') && <NavLink className={classes.navItem} activeClassName={classes.selected} to='/clients'>Clients</NavLink>}
              </div>
              <div className={classes.iconLine}>
              </div>
              <AccountMenu classes={classes} />
            </Toolbar>
          }
        </AppBar>
      </div>
    )
  }
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    navBarDisplay: state.employee.navBarDisplay
  }
}

const StyledNavbar = withStyles(navBarStyle)(NavBar)
const NavbarWithRedux = connect(mapStateToProps)(StyledNavbar)

export default withRouter(NavbarWithRedux)
