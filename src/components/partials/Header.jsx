import React from 'react'
import SideBar from './SideBar'
import NavBar from './NavBar'
import MediaQuery from 'react-responsive'
import { history } from '../../helpers/history'
import { connect } from "react-redux"
import { toggleNavbarDisplay } from "../../redux/modules/employee"
import { bindActionCreators } from "redux"
import { withRouter } from 'react-router-dom';

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      
    }
    this.handleHoverOn = this.handleHoverOn.bind(this)
    this.handleHoverOff = this.handleHoverOff.bind(this)
  }

  componentWillReceiveProps(nextProps){
    const { navBarDisplay } = nextProps
    this.setState(()=>({
      navBarDisplay
    }))
  }

  handleHoverOn() {
    const { toggleNavbarDisplay } = this.props
    toggleNavbarDisplay(true)
  }

  handleHoverOff() {
    const { toggleNavbarDisplay } = this.props
    if (history.location.pathname === "/scoreboard/presentation") {
      toggleNavbarDisplay(false)
    }
  }

  render() {
    const inLogin = history.location.pathname === '/login'
    return (
      <div style={{ height: "60px" }}
        onMouseEnter={this.handleHoverOn}
        onMouseLeave={this.handleHoverOff}>
        {!inLogin &&
          <div>
            <MediaQuery minDeviceWidth={1024}>
              <NavBar />
            </MediaQuery>
            <MediaQuery maxDeviceWidth={768}>
              <SideBar />
            </MediaQuery>
          </div>
        }
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    toggleNavbarDisplay
  }, dispatch)
}

const mapStateToProps = (state) => ({
  navBarDisplay : state.employee.navBarDisplay
})

const HeaderWithRedux =  connect(mapStateToProps, mapDispatchToProps)(Header)
export default withRouter(HeaderWithRedux)
