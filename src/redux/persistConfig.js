import sessionStorage from 'redux-persist/es/storage/session';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

export const persistConfig = {
  key: 'session',
  storage: sessionStorage,
  whitelist: ['authToken','user'],
  stateReconciler: autoMergeLevel2
};
