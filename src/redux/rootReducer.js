import { combineReducers } from 'redux'
import auth from './modules/auth'
import projects from './modules/projects'
import employee from './modules/employee'
import teams from './modules/teams'
import schedule from './modules/schedule'
import skills from './modules/skills'
import clients from './modules/clients'
import comments from './modules/comments'
import activities from './modules/activities'

const rootReducer = combineReducers({
  auth,
  employee,
  projects,
  schedule,
  skills,
  teams,
  clients,
  comments,
  activities
})

export default rootReducer