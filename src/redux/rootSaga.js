import { all } from 'redux-saga/effects'
import { authSaga } from './modules/auth'
import { projectSaga } from './modules/projects'
import { employeeSaga } from './modules/employee';
import { teamSaga } from './modules/teams';
import { scheduleSaga } from './modules/schedule';
import { skillSaga } from './modules/skills';
import { clientSaga } from './modules/clients';
import { commentSaga } from './modules/comments';
import { activitySaga } from './modules/activities';

export default function* rootSaga() {
  yield all([
    authSaga(),
    projectSaga(),
    employeeSaga(),
    teamSaga(),
    scheduleSaga(),
    skillSaga(),
    clientSaga(),
    activitySaga(),
    commentSaga()
  ])
}
