/* istanbul ignore next */
export const getTokenFromGlobalState = state => state.auth.authToken

export const getUserInSessionFromGlobalState = state => state.auth.user

export const getUserInSessionIdFromGlobalState = state => state.auth.user.id