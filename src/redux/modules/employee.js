import { call, put, takeEvery, select, all } from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { getEmployeeRequest, editEmployeeRequest, getAllEmployeesRequest, signUpRequest, changeStatusRequest, editEmployeeTcpSkillsRequest } from '../../api/employeeRequests';
import { getTokenFromGlobalState, getUserInSessionIdFromGlobalState } from '../sagaHelpers';
import { closeSession, updateUser } from './auth';

// Actions
export const GET_EMPLOYEE = 'pernix-cental/Employee/GET_EMPLOYEE'
export const SET_EMPLOYEE = 'pernix-cental/Employee/SET_EMPLOYEE'
export const GET_ALL_EMPLOYEES = 'pernix-cental/Employee/GET_ALL_EMPLOYEE'
export const SET_ALL_EMPLOYEES = 'pernix-cental/Employee/SET_ALL_EMPLOYEES'
export const EDIT_EMPLOYEE = 'pernix-cental/Employee/EDIT_EMPLOYEE'
export const EDIT_EMPLOYEE_TCP_SKILLS = 'pernix-cental/Employee/EDIT_EMPLOYEE_TCP_SKILLS'
export const CHANGE_STATUS_EMPLOYEE = 'pernix-cental/Employee/CHANGE_STATUS_EMPLOYEE'
export const SET_STATUS = 'pernix-cental/Employee/SET_STATUS'
export const SIGNUP_REQUEST = 'pernix-cental/Employee/SIGNUP_REQUEST'
export const SIGNUP_SUCCESS = 'pernix-cental/Employee/SIGNUP_SUCCESS'
export const SIGNUP_FAILURE = 'pernix-cental/Employee/SIGNUP_FAILURE'
export const TOGGLE_NAVBAR_DISPLAY = 'pernix-centrl/Employee/TOGGLE_NAVBAR_DISPLAY'

export const getInitialState = {
  all: null,
  currentEmployee: null,
  status: null,
  navBarDisplay: true
}

// Reducer
const employeeReducer = handleActions(
  {
    [SET_ALL_EMPLOYEES](state, action) {
      const { all } = action.payload
      return {
        ...state,
        all
      }
    },
    [SET_EMPLOYEE](state, action) {
      const { currentEmployee } = action.payload
      return {
        ...state,
        currentEmployee
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        ...state,
        status,
      }
    },
    [TOGGLE_NAVBAR_DISPLAY](state, action) {
      const { navBarDisplay } = action.payload

      return {
        ...state,
        navBarDisplay
      }
    }
  },
  getInitialState,
)

export default employeeReducer

// Action Creators
export const getEmployee = (employeeId) => {
  return createAction(GET_EMPLOYEE)({
    employeeId
  })
}

export const getAllEmployees = () => {
  return createAction(GET_ALL_EMPLOYEES)({})
}

export const setAllEmployees = (response) => {
  const { all } = response
  return createAction(SET_ALL_EMPLOYEES)({
    all
  })
}

export const toggleNavbarDisplay = (navBarDisplay) => {
  return createAction(TOGGLE_NAVBAR_DISPLAY)({
    navBarDisplay
  })
}

export const setEmployee = (response) => {
  const { currentEmployee } = response
  return createAction(SET_EMPLOYEE)({
    currentEmployee
  })
}

export const setStatusCode = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const editEmployee = (employeeId, editedEmployee) => {
  return createAction(EDIT_EMPLOYEE)({
    employeeId,
    editedEmployee
  })
}

export const editEmployeeTcpSkills = (employeeId, editedTcpSkills) => {
  return createAction(EDIT_EMPLOYEE_TCP_SKILLS)({
    employeeId,
    editedTcpSkills
  })
}

export const changeStatusEmployee = (statusChanged) => {
  return createAction(CHANGE_STATUS_EMPLOYEE)({
    statusChanged
  })
}

// Action Creators
export const registerRequest = (userData) => {
  return createAction(SIGNUP_REQUEST)({
    userData
  })
}

export const registerSuccess = (message, authToken) => {
  return createAction(SIGNUP_SUCCESS)({
    message,
    authToken
  })
}

// Fetch API Sagas
export function* registerRequestSaga(action) {
  try {
    const { userData } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(signUpRequest, { userData, authToken })
    yield put(setStatusCode(status))
    yield put(getAllEmployees())
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}
// Fetch API Sagas
export function* getEmployeeRequestSaga(action) {
  try {
    yield put(setStatusCode(null))
    const { employeeId } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const currentEmployee = yield call(getEmployeeRequest, {
      employeeId, authToken
    })
    yield put(setEmployee({ currentEmployee }))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

export function* getAllEmployeesRequestSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const all = yield call(getAllEmployeesRequest, authToken)
    yield put(setAllEmployees({ all }))
  } catch (error) {
    const { response: { request: { status } } } = error
    yield put(setStatusCode(status))
  }
}

export function* editEmployeeRequestSaga(action) {
  try {
    const { payload: { editedEmployee, employeeId } } = action
    const authToken = yield select(getTokenFromGlobalState)
    const sessionUserId = yield select(getUserInSessionIdFromGlobalState)
    const response = yield call(editEmployeeRequest, {
      editedEmployee, authToken, employeeId
    })
    yield put(setStatusCode(200))
    if (sessionUserId === employeeId) {
      yield put(updateUser(response.data))
    } else {
      yield put(setEmployee({ currentEmployee: response.data }))
    }
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

export function* editEmployeeTcpSkillsRequestSaga(action) {
  try {
    const { payload: { employeeId, editedTcpSkills } } = action
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(editEmployeeTcpSkillsRequest, {
      employeeId, editedTcpSkills, authToken
    })
    yield put(setStatusCode(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

export function* changeStatusRequestSaga(action) {
  try {
    const { payload: { statusChanged } } = action
    const authToken = yield select(getTokenFromGlobalState)
    yield call(changeStatusRequest, { statusChanged, authToken })
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

// Saga whatcher
export function* employeeSaga() {
  yield all([
    takeEvery(GET_EMPLOYEE, getEmployeeRequestSaga),
    takeEvery(EDIT_EMPLOYEE, editEmployeeRequestSaga),
    takeEvery(EDIT_EMPLOYEE_TCP_SKILLS, editEmployeeTcpSkillsRequestSaga),
    takeEvery(GET_ALL_EMPLOYEES, getAllEmployeesRequestSaga),
    takeEvery(SIGNUP_REQUEST, registerRequestSaga),
    takeEvery(CHANGE_STATUS_EMPLOYEE, changeStatusRequestSaga),
  ])
}
