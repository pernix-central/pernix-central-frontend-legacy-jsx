import { call, put, takeEvery, select, all} from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { getAllActivitiesRequest, createActivityRequest, editActivityRequest, getActivityRequest } from '../../api/activitiesRequests';
import { getTokenFromGlobalState } from '../sagaHelpers';
import { closeSession } from "./auth"
// Actions
export const GET_ALL_ACTIVITIES = 'pernix-cental/Activities/GET_ALL_ACTIVITIES'
export const SET_ALL_ACTIVITIES = 'pernix-cental/Activities/SET_ALL_ACTIVITIES'
export const SET_STATUS = 'pernix-cental/Activities/SET_STATUS'
export const CREATE_ACTIVITY = 'pernix-cental/Activities/CREATE_ACTIVITY'
export const GET_ACTIVITY = 'pernix-cental/Activities/GET_ACTIVITY'
export const SET_ACTIVITY = 'pernix-cental/Activities/SET_ACTIVITY'
export const EDIT_ACTIVITY = 'pernix-cental/Activities/EDIT_ACTIVITY'


export const getInitialState = {
  all: null,
  currentActivity: null,
  status: null
}

// Reducer
const activityReducer = handleActions(
  {
    [SET_ALL_ACTIVITIES](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    },
    [SET_ACTIVITY](state, action) {
      const { currentActivity } = action.payload
      return {
        ...state,
        currentActivity
      }
    },
  },
  getInitialState,
)

export default activityReducer

// Action Creators
export const getAllActivities = () => {
  return createAction(GET_ALL_ACTIVITIES)({})
}

export const setAllActivities = (response) => {
  const { all } = response
  return createAction(SET_ALL_ACTIVITIES)({
    all
  })
}

export const setStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const createActivity = (newActivity) => {
  return createAction(CREATE_ACTIVITY)({
    newActivity
  })
}

export const setActivity = (currentActivity) => {
  return createAction(SET_ACTIVITY)({
    currentActivity
  })
}

export const editActivity = (editedActivity) => {
  return createAction(EDIT_ACTIVITY)({
    editedActivity
  })
}

export const getActivity = (activityId) => {
  return createAction(GET_ACTIVITY)({
    activityId
  })
}

// Fetch API Sagas
export function* getAllActivitiesRequestSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const all = yield call(getAllActivitiesRequest, authToken)
    yield put(setAllActivities({ all }))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

// Fetch API Sagas
export function* createActivitySaga(action) {
  try {
    const { newActivity } = action.payload
    const token = yield select(getTokenFromGlobalState)
    const status = yield call(createActivityRequest,{
      token,
      newActivity
    })
    yield put(setStatus(status))
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* getActivitySaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { activityId } = action.payload
    const activity = yield call(getActivityRequest, { authToken, activityId })
    if (activity) {
      yield put(setActivity(activity));
    }
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* editActivitySaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { editedActivity } = action.payload
    const status = yield call(editActivityRequest, { authToken, editedActivity })
    yield put(setStatus(status));
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

// Saga whatcher
export function* activitySaga() {
  yield all([
    takeEvery(GET_ALL_ACTIVITIES, getAllActivitiesRequestSaga),
    takeEvery(CREATE_ACTIVITY, createActivitySaga),
    takeEvery(GET_ACTIVITY, getActivitySaga),
    takeEvery(EDIT_ACTIVITY, editActivitySaga),
  ])
}
