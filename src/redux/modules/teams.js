import { call, put, takeEvery, select, all } from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { getTeamRequest, editTeamRequest, getAllTeamsRequest, createTeamRequest, destroyTeamRequest } from '../../api/teamsRequests';
import { getTokenFromGlobalState } from '../sagaHelpers';
import { closeSession } from './auth';

// Actions
export const GET_TEAM = 'pernix-cental/Team/GET_TEAM'
export const SET_TEAM = 'pernix-cental/Team/SET_TEAM'
export const GET_ALL_TEAMS = 'pernix-cental/Team/GET_ALL_TEAM'
export const SET_ALL_TEAMS = 'pernix-cental/Team/SET_ALL_TEAMS'
export const EDIT_TEAM = 'pernix-cental/Team/EDIT_TEAM'
export const DELETE_TEAM = 'pernix-cental/Team/DELETE_TEAM'
export const SET_STATUS = 'pernix-cental/Team/SET_STATUS'
export const CREATE_REQUEST = 'pernix-cental/Team/CREATE_REQUEST'
export const CREATE_SUCCESS = 'pernix-cental/Team/CREATE_SUCCESS'

export const getInitialState = {
  all: null,
  currentTeam: null,
  status: null
}

// Reducer
const teamReducer = handleActions(
  {
    [SET_ALL_TEAMS](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_TEAM](state, action) {
      const { currentTeam } = action.payload
      return {
        currentTeam
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    },
  },
  getInitialState,
)

export default teamReducer

// Action Creators
export const getTeam = (teamId) => {
  return createAction(GET_TEAM)({
    teamId
  })
}

export const getAllTeams = () => {
  return createAction(GET_ALL_TEAMS)({})
}

export const setAllTeams = (response) => {
  const { all } = response
  return createAction(SET_ALL_TEAMS)({
    all
  })
}

export const setTeam = (response) => {
  const { currentTeam } = response
  return createAction(SET_TEAM)({
    currentTeam
  })
}

export const setTeamModuleStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const editTeam = (editedTeam) => {
  return createAction(EDIT_TEAM)({
    editedTeam
  })
}

export const deleteTeam = (teamToDelete) => {
  return createAction(DELETE_TEAM)({
    teamToDelete
  })
}

// Action Creators
export const createTeam = (teamData) => {
  return createAction(CREATE_REQUEST)({
    teamData
  })
}

export const createSuccess = (currentTeam) => {
  return createAction(CREATE_SUCCESS)({
    currentTeam
  })
}

// Fetch API Sagas
export function* createTeamSaga(action) {
  try {
    const { teamData } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(createTeamRequest, { teamData, authToken })
    yield put(setTeamModuleStatus(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setTeamModuleStatus(status))
  }
}
// Fetch API Sagas
export function* getTeamRequestSaga(action) {
  try {
    const { teamId } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const currentTeam = yield call(getTeamRequest, {
      teamId, authToken
    })
    yield put(setTeam({ currentTeam }))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setTeamModuleStatus(status))
  }
}

export function* getAllTeamsRequestSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const all = yield call(getAllTeamsRequest, authToken)
    yield put(setAllTeams({ all }))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setTeamModuleStatus(status))
  }
}

export function* editTeamRequestSaga(action) {
  try {
    const { payload: { editedTeam } } = action
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(editTeamRequest, {
      editedTeam, authToken
    })
    yield put(setTeamModuleStatus(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setTeamModuleStatus(status))
  }
}

export function* deleteTeamSaga(action) {
  try {
    const { payload: { teamToDelete } } = action
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(destroyTeamRequest, {
      teamToDelete, authToken
    })
    yield put(setTeamModuleStatus(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setTeamModuleStatus(status))
  }
}

// Saga whatcher
export function* teamSaga() {
  yield all([
    takeEvery(GET_TEAM, getTeamRequestSaga),
    takeEvery(EDIT_TEAM, editTeamRequestSaga),
    takeEvery(GET_ALL_TEAMS, getAllTeamsRequestSaga),
    takeEvery(CREATE_REQUEST, createTeamSaga),
    takeEvery(DELETE_TEAM, deleteTeamSaga),
  ])
}
