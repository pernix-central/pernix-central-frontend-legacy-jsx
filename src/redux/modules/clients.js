import { call, put, takeEvery, select, all} from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { getAllClientsRequest, createClientRequest, editClientRequest, getClientRequest } from '../../api/clientsRequests';
import { getTokenFromGlobalState } from '../sagaHelpers';
import { closeSession } from "./auth"
// Actions
export const GET_ALL_CLIENTS = 'pernix-cental/Clients/GET_ALL_CLIENTS'
export const SET_ALL_CLIENTS = 'pernix-cental/Clients/SET_ALL_CLIENTS'
export const SET_STATUS = 'pernix-cental/Clients/SET_STATUS'
export const CREATE_CLIENT = 'pernix-cental/Clients/CREATE_CLIENT'
export const GET_CLIENT = 'pernix-cental/Clients/GET_CLIENT'
export const SET_CLIENT = 'pernix-cental/Clients/SET_CLIENT'
export const EDIT_CLIENT = 'pernix-cental/Clients/EDIT_CLIENT'


export const getInitialState = {
  all: null,
  currentClient: null,
  status: null
}

// Reducer
const clientReducer = handleActions(
  {
    [SET_ALL_CLIENTS](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        ...state,
        status,
      }
    },
    [SET_CLIENT](state, action) {
      const { currentClient } = action.payload
      return {
        currentClient
      }
    },
  },
  getInitialState,
)

export default clientReducer

// Action Creators
export const getAllClients = () => {
  return createAction(GET_ALL_CLIENTS)({})
}

export const setAllClients = (response) => {
  const { all } = response
  return createAction(SET_ALL_CLIENTS)({
    all
  })
}

export const setStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const createClient = (newClient) => {
  return createAction(CREATE_CLIENT)({
    newClient
  })
}

export const setClient = (currentClient) => {
  return createAction(SET_CLIENT)({
    currentClient
  })
}

export const editClient = (editedClient) => {
  return createAction(EDIT_CLIENT)({
    editedClient
  })
}

export const getClient = (clientId) => {
  return createAction(GET_CLIENT)({
    clientId
  })
}

// Fetch API Sagas
export function* getAllClientsRequestSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const all = yield call(getAllClientsRequest, authToken)
    yield put(setAllClients({ all }))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

// Fetch API Sagas
export function* createClientSaga(action) {
  try {
    const { newClient } = action.payload
    const token = yield select(getTokenFromGlobalState)
    const status = yield call(createClientRequest,{
      token,
      newClient
    })
    yield put(setStatus(status))
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* getClientSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { clientId } = action.payload
    const client = yield call(getClientRequest, { authToken, clientId })
    if (client) {
      yield put(setClient(client));
    }
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* editClientSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { editedClient } = action.payload
    const status = yield call(editClientRequest, { authToken, editedClient })
    yield put(setClient(editedClient))
    yield put(setStatus(status))
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

// Saga whatcher
export function* clientSaga() {
  yield all([
    takeEvery(GET_ALL_CLIENTS, getAllClientsRequestSaga),
    takeEvery(CREATE_CLIENT, createClientSaga),
    takeEvery(GET_CLIENT, getClientSaga),
    takeEvery(EDIT_CLIENT, editClientSaga),
  ])
}
