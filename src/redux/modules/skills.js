import { call, put, takeLatest, select, all } from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { createSkillRequest, getAllSkillsRequest, getSkillRequest, editSkillRequest } from '../../api/skillsRequests'
import { getTokenFromGlobalState } from '../sagaHelpers'
import { closeSession } from './auth';

// Actions
export const SET_STATUS = 'pernix-cental/SkillS/SET_STATUS'
export const CREATE_SKILL = 'pernix-cental/SkillS/CREATE_SKILL'
export const GET_ALL_SKILLS = 'pernix-cental/Skills/GET_ALL_SKILLS'
export const SET_ALL_SKILLS = 'pernix-cental/Skills/SET_ALL_SKILLS'
export const GET_SKILL = 'pernix-cental/Skill/GET_SKILL'
export const SET_SKILL = 'pernix-cental/Skill/SET_SKILL'
export const EDIT_SKILL = 'pernix-cental/Skill/EDIT_SKILL'

export const getInitialState = {
  all: null,
  currentSkill: null,
  status: null
}

// Reducer
const skillReducer = handleActions(
  {
    [SET_ALL_SKILLS](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    },
    [SET_SKILL](state, action) {
      const { currentSkill } = action.payload
      return {
        currentSkill
      }
    },
  },
  getInitialState,
)


// Action Creators
export default skillReducer

// Action Creators
export const getAllSkills = () => {
  return createAction(GET_ALL_SKILLS)({})
}

export const setAllSkills = (response) => {
  const { all } = response
  return createAction(SET_ALL_SKILLS)({
    all
  })
}

export const setStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const createSkill = (newSkill) => {
  return createAction(CREATE_SKILL)({
    newSkill
  })
}

export const getSkill = (skillId) => {
  return createAction(GET_SKILL)({
    skillId
  })
}

export const setSkill = (response) => {
  const { currentSkill } = response
  return createAction(SET_SKILL)({
    currentSkill
  })
}

export const editSkill = (editedSkill) => {
  return createAction(EDIT_SKILL)({
    editedSkill
  })
}


// Fetch API Sagas 
export function* createSkillSaga(action) {
  try {
    const { newSkill } = action.payload
    const token = yield select(getTokenFromGlobalState)
    const status = yield call(createSkillRequest,{
      token,
      newSkill
    })
    yield put(setStatus(status))
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* getAllSkillsRequestSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const all = yield call(getAllSkillsRequest, authToken)
    yield put(setAllSkills({ all }))
  } catch(error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* getSkillRequestSaga(action) {
  try {
    const { skillId } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const currentSkill = yield call(getSkillRequest, {
      skillId , authToken
    })
    yield put(setSkill({ currentSkill }))
  } catch(error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

export function* editSkillRequestSaga(action) {
  try {
    const { payload:{ editedSkill }} = action
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(editSkillRequest, {
      editedSkill , authToken
    })
    yield put(setStatus(status))
  } catch(error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatus(status))
  }
}

// Saga whatcher
export function* skillSaga() {
  yield all([
    takeLatest(CREATE_SKILL, createSkillSaga),
    takeLatest(GET_ALL_SKILLS, getAllSkillsRequestSaga),
    takeLatest(GET_SKILL, getSkillRequestSaga),
    takeLatest(EDIT_SKILL, editSkillRequestSaga),
  ])
}
