import { call, put, takeEvery, select, all } from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { editCommentsRequest, addNewCommentRequest } from '../../api/commentsRequests';
import { getTokenFromGlobalState } from '../sagaHelpers';
import { closeSession } from "./auth"
import { setProjectModuleStatus } from './projects';
import { setTeamModuleStatus } from './teams';

// Actions
export const GET_COMMENTS = 'pernix-cental/Comments/GET_COMMENTS'
export const SET_COMMENTS = 'pernix-cental/Comments/SET_COMMENTS'
export const SET_STATUS = 'pernix-cental/Comments/SET_STATUS'
export const ADD_NEW_COMMENT = 'pernix-cental/Comments/ADD_NEW_COMMENT'
export const EDIT_COMMENT = 'pernix-cental/Comments/EDIT_COMMENT'


export const getInitialState = {
  all: null,
}

// Reducer
const commentReducer = handleActions(
  {
    [SET_COMMENTS](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    }
  },
  getInitialState,
)

export default commentReducer

// Action Creators
export const getAllComments = () => {
  return createAction(GET_COMMENTS)({})
}

export const setAllComments = (response) => {
  const { all } = response
  return createAction(SET_COMMENTS)({
    all
  })
}

export const setCommentStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const addNewComment = (newComment) => {
  return createAction(ADD_NEW_COMMENT)({
    newComment
  })
}

export const editComment = (editedComment) => {
  return createAction(EDIT_COMMENT)({
    editedComment
  })
}


// Fetch API Sagas
export function* editCommentRequestSaga(action) {
  try {
    const { editedComment:{ comment, projectId} } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(editCommentsRequest, { comment, projectId, authToken })
    yield put(setCommentStatus(status))
    yield put(projectId ? setProjectModuleStatus(status): setTeamModuleStatus(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setCommentStatus(status))
  }
}

// Fetch API Sagas
export function* addNewCommentSaga(action) {
  try {
    const { newComment } = action.payload
    const { projectId } = newComment
    const token = yield select(getTokenFromGlobalState)
    const status = yield call(addNewCommentRequest, {
      token,
      newComment
    })
    yield put(setCommentStatus(status))
    yield put(projectId ? setProjectModuleStatus(status) : setTeamModuleStatus(status))
  } catch (error) {
    const { response: { request: { status } } } = error
    status === 422 ? yield put(closeSession()) : yield put(setCommentStatus(status))
  }
}


// Saga whatcher
export function* commentSaga() {
  yield all([
    takeEvery(EDIT_COMMENT, editCommentRequestSaga),
    takeEvery(ADD_NEW_COMMENT, addNewCommentSaga),
  ])
}
