import {call,put, takeLatest, all} from 'redux-saga/effects'
import { handleActions, createAction} from 'redux-actions'
import { loginRequest } from '../../api/authRequests'
import { persistReducer } from 'redux-persist';
import { persistConfig} from '../persistConfig'

// Actions
export const AUTH_REQUEST = 'pernix-cental/Auth/AUTH_REQUEST'
export const AUTH_SUCCESS = 'pernix-cental/Auth/AUTH_SUCCESS'
export const AUTH_FAILURE = 'pernix-cental/Auth/AUTH_FAILURE'
export const UPDATE_USER = 'pernix-cental/Auth/UPDATE_USER'
export const CLOSE_SESSION = 'pernix-cental/Auth/CLOSE_SESSION'

export const getInitialState = {
  authToken: null,
  user: null,
  error: null
}

// Reducer
const authReducer = handleActions({
    [AUTH_REQUEST](state, action) {
      const {email,password} = action.payload
      return {
        email,
        password,
      }
    },
    [AUTH_SUCCESS](state, action) {
      const {
        authToken,
        user
      } = action.payload
      return {
        authToken,
        user
      }
    },
    [AUTH_FAILURE](state, action) {
      const {
        error
      } = action.payload
      return {
        error,
      }
    },
    [UPDATE_USER](state, action) {
      const { user } = action.payload
      return {
        ...state,
        user
      }
    },
    [CLOSE_SESSION](state, action) {
      return getInitialState
    }
  },
  getInitialState,
)

export default persistReducer(persistConfig, authReducer);

// Action Creators
export const authRequest = (email, password) => {
  return createAction(AUTH_REQUEST)({
    email,
    password
  })
}

export const authSuccess = (authToken,user) => {
  return createAction(AUTH_SUCCESS)({
    authToken,
    user
  })
}

export const updateUser = (user) => {
  return createAction(UPDATE_USER)({
    user
  })
}

export const authFailure = (error) => {
  return createAction(AUTH_FAILURE)({
    error
  })
}

export const closeSession = () => {
  return createAction(CLOSE_SESSION)()
}

// Fetch API Sagas
export function* authRequestSaga(action) {
  try {
    const { authToken, user } = yield call(loginRequest, action.payload)
    if (authToken){
      yield put(authSuccess(authToken,user ))
    }
  } catch(error) {
    const { response: { request } } = error
    const { statusText, status, response } = request
    const { message } = JSON.parse(response)
    yield put(authFailure({ statusText, status, message }))
  }
}

// Saga whatcher
export function* authSaga() {
  yield all([takeLatest(AUTH_REQUEST, authRequestSaga)])
}
