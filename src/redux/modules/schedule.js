import {call,put, takeEvery, select, all} from 'redux-saga/effects'
import { handleActions, createAction} from 'redux-actions'
import { getScheduleRequest, editScheduleRequest } from '../../api/scheduleRequests';
import { getTokenFromGlobalState } from '../sagaHelpers';
import { closeSession } from './auth';

// Actions
export const GET_SCHEDULE = 'pernix-cental/Schedule/GET_SCHEDULE'
export const SET_SCHEDULE = 'pernix-cental/Schedule/SET_SCHEDULE'
export const EDIT_SCHEDULE = 'pernix-cental/Schedule/EDIT_SCHEDULE'
export const SET_STATUS = 'pernix-cental/Schedule/SET_STATUS'

export const getInitialState = {
  currentSchedule: null,
  status:null
}

// Reducer
const scheduleReducer = handleActions(
  {
    [SET_SCHEDULE](state, action) {
      const { currentSchedule } = action.payload
      return {
        currentSchedule
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    },
  },
  getInitialState,
)

export default scheduleReducer

// Action Creators
export const getSchedule = (employeeId,scheduleId) => {
  return createAction(GET_SCHEDULE)({
    employeeId,
    scheduleId
  })
}

export const setSchedule = (response) => {
  const { currentSchedule } = response
  return createAction(SET_SCHEDULE)({
    currentSchedule
  })
}

export const setStatusCode = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const editSchedule = (employeeId,editedSchedule) => {
  return createAction(EDIT_SCHEDULE)({
    employeeId,
    editedSchedule
  })
}


// Fetch API Sagas
export function* getScheduleRequestSaga(action) {
  try {
    const { employeeId, scheduleId } = action.payload
    const authToken = yield select(getTokenFromGlobalState)
    const currentSchedule = yield call(getScheduleRequest, {
      employeeId, scheduleId , authToken
    })
    yield put(setSchedule({ currentSchedule }))
  } catch(error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

export function* editScheduleRequestSaga(action) {
  try {
    const { payload:{ employeeId, editedSchedule }} = action
    const authToken = yield select(getTokenFromGlobalState)
    const status = yield call(editScheduleRequest, {
      employeeId ,editedSchedule , authToken
    })
    yield put(setStatusCode(status))
  } catch(error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setStatusCode(status))
  }
}

// Saga whatcher
export function* scheduleSaga() {
  yield all ([
    takeEvery(GET_SCHEDULE, getScheduleRequestSaga),
    takeEvery(EDIT_SCHEDULE, editScheduleRequestSaga)
  ])
}
