import reducer, {
  getInitialState,
  setAllClients,
  setStatus
} from '../../clients';


const initialState = getInitialState

const clientMock = {
  "id": 1,
  "name": "Daniela",
  "last_name": "Ferrer",
  "contact_email": "dferrer@hotmail.com",
  "company_name": "Flatirons",
  "phone_number": "45694930",
  "country": "US"
}
const all = [ {...clientMock,id:2}, {...clientMock,id:3}, {...clientMock,id:4} ]

describe("Test for client reducer", () => {

  it("handles when setAllClients is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setAllClients(all))).toMatchSnapshot()
  })

  it("handles when setClient is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setAllClients(clientMock))).toMatchSnapshot()
  })

  it("handles when setStatusCode is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setStatus(200))).toMatchSnapshot()
  })

})
