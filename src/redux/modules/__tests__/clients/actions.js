import {
  setAllClients,
  getAllClients,
  setStatus,
  getClient,
  setClient,
  editClient
} from '../../clients'

const clientMock = {
  "id": 1,
  "name": "Daniela",
  "last_name": "Ferrer",
  "contact_email": "dferrer@hotmail.com",
  "company_name": "Flatirons",
  "phone_number": "45694930",
  "country": "US"
}

const clientListMock =  [
  clientMock,
  clientMock,
  clientMock
]

describe("Test for clients module action creators", () => {

  it("setAllClients action creator", () => {
    expect(setAllClients(clientListMock)).toMatchSnapshot()
  })

  it("getClient by id action creator", () => {
    expect(getClient(1)).toMatchSnapshot()
  })

  it("setClient action creator", () => {
    expect(getClient(clientMock)).toMatchSnapshot()
  })

  it("editClient action creator", () => {
    expect(getClient(clientMock)).toMatchSnapshot()
  })

  it("getAllClients action creator", () => {
    expect(getAllClients()).toMatchSnapshot()
  })

  it("setStatus action creator", () => {
    expect(setStatus(200)).toMatchSnapshot()
  })

})