import reducer, {
  setAllTeams,
  setStatusCode,
  setTeam,
  getInitialState
} from '../../teams'


const teamMock = {
  "employee": {
    "name": "Pernix Central",
    "employeeids": [1, 2]
  }
}

const teamsMock = {
  teamMock,
  teamMock,
}

const initialState = getInitialState

describe("teams reducer", () => {

  it("handles when setAllTeams is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setAllTeams(teamsMock))).toMatchSnapshot()
  })

  it("handles when setTeam is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setTeam(teamMock))).toMatchSnapshot()
  })

  it("handles when setStatusCode is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setStatusCode(200))).toMatchSnapshot()
  })

})
