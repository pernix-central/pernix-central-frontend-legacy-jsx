import {
  setAllTeams,
  setStatusCode,
  getTeam,
  createTeam,
  setTeam,
  getAllTeams
} from '../../teams'

const teamMock = {
  "employee": {
    "id": 1,
    "name": "Pernix Central",
    "employeeIds": [1, 2],
  }
}

const teamsMock = {
  teamMock,
  teamMock,
  teamMock
}

describe("teams action creators", () => {

  it("setTeam action creator", () => {
    expect(setTeam(teamMock)).toMatchSnapshot()
  })

  it("setAllTeams action creator", () => {
    expect(setAllTeams(teamsMock)).toMatchSnapshot()
  })

  it("setStatus action creator", () => {
    expect(setStatusCode(200)).toMatchSnapshot()
  })

  it("createTeam action creator", () => {
    expect(createTeam(teamMock)).toMatchSnapshot()
  })

  it("setAllTeams action creator", () => {
    expect(getAllTeams()).toMatchSnapshot()
  })

  it("getTeam action creator", () => {
    expect(getTeam()).toMatchSnapshot()
  })
})
