import reducer, {
  setAllProjects,
  setStatus,
  setProject,
  getInitialState
} from '../../projects'

const projectsMock =  [
  {
    "id": 1,
    "name": "Pernix Central",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  },
  {
    "id": 2,
    "name": "PFA",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  }]

const projectMock = {
    "id": 1,
    "name": "Pernix Central",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  }

const initialState = getInitialState

describe("projects reducer", () => {

  it("handles when setAllProjects is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setAllProjects(projectsMock))).toMatchSnapshot()
  })

  it("handles when setProject is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setProject(projectMock))).toMatchSnapshot()
  })

  it("handles when setStatus is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setStatus(200))).toMatchSnapshot()
  })

})