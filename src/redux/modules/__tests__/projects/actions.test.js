import {
  setAllProjects,
  setStatus,
  getProject,
  createProject,
  setProject,
  getAllProjects
} from '../../projects'

const projectsMock =  [
  {
    "id": 1,
    "name": "Pernix Central",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  },
  {
    "id": 2,
    "name": "PFA",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  }]
const projectMock = {
    "id": 1,
    "name": "Pernix Central",
    "description": "--",
    "start_date": "--",
    "end_date": "--",
    "status": "--"
  }

describe("projects action creators", () => {

  it("setProject action creator", () => {
    expect(setProject(projectMock)).toMatchSnapshot()
  })

  it("setAllProjects action creator", () => {
    expect(setAllProjects(projectsMock)).toMatchSnapshot()
  })

  it("setStatus action creator", () => {
    expect(setStatus(200)).toMatchSnapshot()
  })

  it("createProject action creator", () => {
    expect(createProject(projectMock)).toMatchSnapshot()
  })
  
  it("setAllProjects action creator", () => {
      expect(getAllProjects()).toMatchSnapshot()
  })

  it("getProject action creator", () => {
    expect(getProject()).toMatchSnapshot()
  })
})