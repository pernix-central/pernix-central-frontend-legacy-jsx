import {
  authRequest,
  authSuccess,
  authFailure
} from '../../auth'

describe("auth action creators", () => {

  it("authRequest", () => {
    expect(authRequest("brad@gamail.com", "password")).toMatchSnapshot()
  })

  it("authSuccess", () => {
    expect(authSuccess("token")).toMatchSnapshot()
  })

  it("authFailure", () => {
    expect(authFailure()).toMatchSnapshot()
  })
  
})