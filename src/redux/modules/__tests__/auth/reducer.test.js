import reducer, {
  getInitialState,
  authRequest,
  authSuccess,
  authFailure,
} from '../../auth'


const initialState = getInitialState

describe("auth reducer", () => {

  it("handles authRequest", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, authRequest("brad@gmail.com", "password"))).toMatchSnapshot()
  })

  it("handles success request", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, authSuccess("eyJhbGciOiJIUzI1NiJ9"))).toMatchSnapshot()
  })

  it("handles error request", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, authFailure({
      statusText: "Error",
      status: 401,
      message: "invalid credentials"
    }))).toMatchSnapshot()
  })

})
