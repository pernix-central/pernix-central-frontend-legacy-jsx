import {
  setAllEmployees,
  setStatusCode,
  getEmployee,
  registerRequest,
  setEmployee,
  getAllEmployees
} from '../../employee'

const employeeMock = {
  "first_name": 'test',
  "last_name": 'test',
  "birthdate": 'test',
  "email": 'test',
  "password": 'test',
  "password_confirmation": 'test',
  "has_car": 'test',
  "blog_url": 'test',
  "computer_serial_number": 'test',
  "mac_address": 'test',
  "hire_date": 'test',
  "on_vacation": 'test',
  "can_be_mentor": 'test',
  "role": 'test'
}

const employeesMock =  [
  employeeMock,
  employeeMock,
  employeeMock
]

describe("Employees action creators", () => {

  it("setEmployee action creator", () => {
    expect(setEmployee(employeeMock)).toMatchSnapshot()
  })

  it("setAllEmployees action creator", () => {
    expect(setAllEmployees(employeesMock)).toMatchSnapshot()
  })

  it("setStatus action creator", () => {
    expect(setStatusCode(200)).toMatchSnapshot()
  })

  it("Register a new user action creator", () => {
    expect(registerRequest(employeeMock)).toMatchSnapshot()
  })
  
  it("getAllEmployees action creator", () => {
      expect(getAllEmployees()).toMatchSnapshot()
  })

  it("getEmployee action creator", () => {
    expect(getEmployee()).toMatchSnapshot()
  })
})