import reducer, {
  getInitialState,
  setAllEmployees,
  setEmployee,
  setStatusCode
} from '../../employee';


const initialState = getInitialState
const employee = {
  birthdate: "2016-10-30",
  blogUrl: "string",
  canBeMentor: false,
  computerSerialNumber: "string",
  email: "test@test.com",
  firstName: "string",
  hasCar: false,
  hireDate: "2017-10-28",
  lastName: "string",
  macAddress: "string",
  onVacation: false,
  password: "string",
  passwordConfirmation: "string",
  role: "Apprentice"
}
const all = [ employee, employee, employee ]

describe("employee reducer", () => {

  it("handles when getEmployee is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setAllEmployees(all))).toMatchSnapshot()
  })

  it("handles when setEmployee is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setEmployee(employee))).toMatchSnapshot()
  })

  it("handles when setStatusCode is dispatched", () => {
    const state = {
      ...initialState
    }
    expect(reducer(state, setStatusCode(200))).toMatchSnapshot()
  })

})
