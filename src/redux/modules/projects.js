import { call, put, takeLatest, select, all} from 'redux-saga/effects'
import { handleActions, createAction } from 'redux-actions'
import { getAllProjectsRequest, getProjectRequest, createProjectRequest, editProjectRequest } from '../../api/projectsRequests'
import { getTokenFromGlobalState } from '../sagaHelpers'
import { closeSession } from './auth';

// Actions
export const GET_ALL_PROJECTS = 'pernix-cental/Projects/GET_ALL_PROJECTS'
export const SET_ALL_PROJECTS = 'pernix-cental/Projects/SET_ALL_PROJECTS'
export const SET_STATUS = 'pernix-cental/Projects/SET_STATUS'
export const GET_PROJECT = 'pernix-cental/Projects/GET_PROJECT'
export const SET_PROJECT = 'pernix-cental/Projects/SET_PROJECT'
export const CREATE_PROJECT = 'pernix-cental/Projects/CREATE_PROJECT'
export const EDIT_PROJECT = 'pernix-cental/Projects/EDIT_PROJECT'

export const getInitialState = {
  all: null,
  currentProject: null,
  status: null
}

// Reducer
const projectsReducer = handleActions(
  {
    [SET_ALL_PROJECTS](state, action) {
      const { all } = action.payload
      return {
        all
      }
    },
    [SET_STATUS](state, action) {
      const { status } = action.payload
      return {
        status,
      }
    },
    [SET_PROJECT](state, action) {
      const { currentProject } = action.payload
      return {
        currentProject
      }
    },
  },
  getInitialState,
)

export default projectsReducer

// Action Creators
export const getAllProjects = () => {
  return createAction(GET_ALL_PROJECTS)({})
}

export const setAllProjects = (all) => {
  return createAction(SET_ALL_PROJECTS)({
    all
  })
}

export const setProjectModuleStatus = (status) => {
  return createAction(SET_STATUS)({
    status
  })
}

export const getProject = (projectId) => {
  return createAction(GET_PROJECT)({
    projectId
  })
}

export const createProject = (newProject) => {
  return createAction(CREATE_PROJECT)({
    newProject
  })
}

export const setProject = (currentProject) => {
  return createAction(SET_PROJECT)({
    currentProject
  })
}

export const editProject = (editedProject) => {
  return createAction(EDIT_PROJECT)({
    editedProject
  })
}

// Fetch API Sagas
export function* getAllProjectsSaga(action) {
  try {
    const token = yield select(getTokenFromGlobalState)
    const all = yield call(getAllProjectsRequest, token)
    if (all) {
      yield put(setAllProjects(all))
    }
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setProjectModuleStatus(status))
  }
}

export function* getProjectSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { projectId } = action.payload
    const project = yield call(getProjectRequest, { authToken, projectId })
    if (project) {
      yield put(setProject(project));
    }
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setProjectModuleStatus(status))
  }
}

export function* createProjectSaga(action) {
  try {
    const { newProject } = action.payload
    const token = yield select(getTokenFromGlobalState)
    const status = yield call(createProjectRequest,{
      token,
      newProject
    })
    yield put(setProjectModuleStatus(status))
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setProjectModuleStatus(status))
  }
}

export function* editProjectSaga(action) {
  try {
    const authToken = yield select(getTokenFromGlobalState)
    const { editedProject } = action.payload
    const status = yield call(editProjectRequest, { authToken, editedProject })
    yield put(setProjectModuleStatus(status));
  } catch (error) {
    const { response:{ request: { status }}} = error
    status === 422 ? yield put(closeSession()) : yield put(setProjectModuleStatus(status))
  }
}

// Saga whatcher
export function* projectSaga() {
  yield all([
    takeLatest(GET_ALL_PROJECTS, getAllProjectsSaga),
    takeLatest(GET_PROJECT, getProjectSaga),
    takeLatest(CREATE_PROJECT, createProjectSaga),
    takeLatest(EDIT_PROJECT, editProjectSaga)
  ])
}
