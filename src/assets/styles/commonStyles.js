const commonStyles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    margin: '20px 40px',
    position: 'relative',
    height: "calc(100% - 40px)",
    [theme.breakpoints.down('xs')]: {
      margin: 0,
    }
  },
  '@global': {
    body: {
      margin: '0',
    },
  },
  successBackground: {
    backgroundColor: PERNIX_GREEN_HOVER,
  },
  containerPaper: {
    height: "calc(100vh - 121px)",
    display: 'inline-table',
    width: '100%'
  },
  flexEnd:{
    justifyContent: 'flex-end'
  }
})

export const PERNIX_BLUE = '#448ECC'
export const PERNIX_ORANGE = '#ec6535'
export const PERNIX_GREEN = '#53ac4e'
export const PERNIX_GREEN_HOVER = '#458e41'
export const PERNIX_BLUE_HOVER = '#447ccc'
export const ERROR = '#a72c1e'
export const ERROR_HOVER = '#7c2116'
export const GOLD = '#d69c34'
export const SILVER = '#b3b3b2'
export const BRONCE = '#ca743a'

export default commonStyles
