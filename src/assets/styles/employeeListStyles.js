import { PERNIX_BLUE, PERNIX_GREEN } from "./commonStyles";

const LIGHT_GRAY = '#E5E5DB'

const employeeListStyles = theme => ({
  newEmployee: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px',
    alignItems: 'center'
  },
  paper: {
    marginTop: '20px',
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  dateTableCell: {
    width: '15%'
  },
  panel: {
    display: 'block',
    [theme.breakpoints.down('xs')]: {
      margin: 0,
      padding: 5
    }
  },
  button: {
    size: '0px'
  },
  leftAlign: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
  softSkill: {
    background: PERNIX_BLUE
  },
  techSkill: {
    background: PERNIX_GREEN
  },
  whiteAvatar: {
    background: 'white'
  },
  simpleTable: {
    height: '250px',
    overflowY: 'auto'
  },
  tableBorder: {
    borderStyle: 'solid',
    borderColor: LIGHT_GRAY,
  },
  searchBar: {
    border: 'solid gray 1px',
    display: 'flex',
    borderRadius: '5px',
    height: '30px',
    width: '45%',
    alignItems: 'center'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
    border: '0',
    marginLeft: '10px',
    fontFamily: 'Montserrat',
    '&:focus': {
      outline: 'none'
    },
  },
  searchResultsContainer: {
    zIndex: 1,
    position: 'absolute',
    backgroundColor: 'white',
    width: '100%'
  }
});

export default employeeListStyles
