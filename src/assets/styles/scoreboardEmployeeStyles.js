const scoreboardEmployeeStyles = theme => ({

  avatarBorder: {
    borderRadius: '100%',
  },
  avatarPosition: {
    position: 'relative',
    bottom: '37px',
    zIndex: '5',
    color:'white',
    textAlign: 'center',
    fontWeight: '600'
  },
  avatarFullName: {
    bottom: '34px',
    textAlign: 'center',
    position: 'relative',
    zIndex: '2',
    fontWeight: 'bold'
  },
  center: {
    textAlign: 'center'
  },
  avatarRibbon: {
    maxWidth: '271px',
    position: 'relative',
    bottom: '114px'
  },
  points:{
    position: 'relative',
    bottom: '115px',
    fontWeight: 'bold'
  },
})

export default scoreboardEmployeeStyles
