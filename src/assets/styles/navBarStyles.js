const navBarStyles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    width: 60,
    height: 60,
    marginRight: 20,
  },
  appBarIcon: {
    width: '100%',
  },
  navLiks: {
    width: '95%',
    paddingLeft: '0%',
  },
  navItem: {
    paddingLeft: '2%',
    textDecoration: 'none',
    color: 'gray'
  },
  selected: {
    color: 'black'
  },
  showToolbar: {
    width: '100%',
    height: '60px',
    transition:'all 0.5s ease',
  },
  hideToolbar: {
    width: '100%',
    height: '0px',
    transition:'all 0.5s ease',
  },
  iconLine: {
    height: '50px',
    borderLeft: 'groove gray',
    borderWidth: '1px'

  },
  userOptions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  roleLabel: {
    marginTop: 6,
    marginLeft: 18,
    width: '50px',
    color: 'gray',
    textDecoration: 'none',
    textAlign: 'center',
    fontSize: '0.8em'
  },
  userIcon: {
    marginLeft: '26%'
  },
  sticky: {
    top: 0,
    width: '100%'
  },
  accountIcon: {
    borderRadius: '100%',
    border: 'black 2px solid',
    width: '34px',
    height: '34px'
  },
}

export default navBarStyles
