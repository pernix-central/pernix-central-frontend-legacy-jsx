import { PERNIX_ORANGE, PERNIX_BLUE, GOLD, SILVER, BRONCE } from './commonStyles'

const minHeight = 630

const scoreboardPresentationStyles = theme => ({
  firstPositionsContainer: {
    height: 'calc(64vh - 45px)',
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    fontFamily: 'Montserrat, sans-serif',
  },
  nameContainer: {
    fontWeight: 'bold',
    minHeight: '35px',
  },
  pointsContainer: {
    fontWeight: 'bold',
  },
  avatarP1: {
    width: '75%',
    marginTop: '12%',
    boxShadow: `0 0 4px 16px ${GOLD}`,
    [theme.breakpoints.up('xl')]: {
      width: '100%',
      marginTop: '20%',
    }
  },
  avatarP2: {
    width: '70%',
    marginTop: '18%',
    boxShadow: `0 0 4px 16px ${SILVER}`,
    [theme.breakpoints.up('xl')]: {
      width: '90%',
      marginTop: '30%',
    }
  },
  avatarP3: {
    width: '65%',
    marginTop: '23%',
    boxShadow: `0 0 4px 16px ${BRONCE}`,
    [theme.breakpoints.up('xl')]: {
      width: '85%',
      marginTop: '35%',
    }
  },
  lastPositionsContainer: {
    height: 'calc(36vh - 64px)',
    backgroundColor: PERNIX_BLUE,
    color: 'white',
    fontFamily: 'Montserrat, sans-serif'
  },
  avatar: {
    verticalAlign: 'top',
    width: '43%',
    borderColor: PERNIX_ORANGE,
    borderStyle: 'solid',
    borderRadius: '100%',
  },
  positionBadge: {
    fontWeight: 'bold',
    fontSize: '160%',
    backgroundColor: PERNIX_ORANGE,
    borderRadius: '100%',
    padding: '35%',
  },
  avatarItem: {
    width: '12%',
    padding: '1%',
    float: 'left',
  },
  avatarName: {
    textAlign: 'center',
    fontSize: '0.9em',
  },
  ul: {
    listStyleType: 'none',
    margin: '0',
    padding: '30px 5% 0% 5%',
    overflow: 'hidden',
    [theme.breakpoints.up('xl')]: {
      padding: '60px 5% 0% 5%',
    }
  },
  li: {
    textAlign: 'center',
  },
  badgeAlign: {
    position: 'reliative',
    display: 'none',
  },
  root: {
    display: 'initial',
    position: 'relative',
    verticalAlign: 'middle',
  },

  [`@media (height: ${minHeight}px)`]: {
    avatarP1: {
      width: '75%',
      marginTop: '56%',
      boxShadow: `0 0 4px 16px ${GOLD}`,
    },
    avatarP2: {
      width: '70%',
      marginTop: '50%',
      boxShadow: `0 0 4px 16px ${SILVER}`,
    },
    avatarP3: {
      width: '65%',
      marginTop: '60%',
      boxShadow: `0 0 4px 16px ${BRONCE}`,
    },
    positionBadge: {
      fontWeight: 'bold',
      fontSize: '130%',
      backgroundColor: PERNIX_ORANGE,
      borderRadius: '100%',
      padding: '30%',
    },
    ul: {
      listStyleType: 'none',
      margin: '0',
      padding: '1% 5% 0% 5%',
      overflow: 'hidden',
    },
  }
})

export default scoreboardPresentationStyles
