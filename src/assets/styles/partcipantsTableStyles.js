const LIGHT_GRAY = '#E5E5DB'

const participantsTableStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  simpleTable: {
    height: 'calc(51vh)',
    overflowY: 'auto'
  },
  tableBorder: {
    borderStyle: 'solid',
    borderColor: LIGHT_GRAY,
  },
  item: {
    width: '60%',
    textAlign: 'center',
  },
  itemHead: {
    width: '36%'
  }
})

export default participantsTableStyles
