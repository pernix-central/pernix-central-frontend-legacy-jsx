import { PERNIX_BLUE, PERNIX_GREEN, PERNIX_GREEN_HOVER, PERNIX_BLUE_HOVER, ERROR } from "./commonStyles"

const projectsProfileStyles = theme => ({
  root: {
    flexGrow: 1,
    margin: '3% 5%',
    padding: '3%'
  },
  paper: {
    padding: '10px 0px',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    boxShadow: 'none'
  },
  left: {
    textAlign: 'left',
    paddingLeft: '15%'
  },
  leftAlign: {
    textAlign: 'left',
  },
  infoContainer: {
    paddingLeft: '5%'
  },
  borderRight: {
    borderRight: 'solid gray',
  },
  tableContainer: {
    marginLeft: 19
  },
  card: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: '30%',
  },
  controls: {
    display: 'flex',
    alignItems: 'right',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  editBtn: {
    color: 'white',
    marginLeft: '6%',
    marginTop: '39%',
    background: PERNIX_BLUE,
    '&:hover': {
      background: PERNIX_BLUE_HOVER,
    }
  },
  swichStatusActive: {
    backgroundColor: PERNIX_GREEN,
    color: 'white'
  },
  swichStatusInActive: {
    backgroundColor: ERROR,
    color: 'white'
  },
  submitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    width: '40%',
    marginLeft: '1%',
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      width: '50%'
    }
  },
  actionsContainer: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  clientsList: {
    paddingTop: '0px',
  },
  clientsItemsDiv: {
    paddingLeft: '0px',
    paddingTop: '0px',
    paddingBottom: '0px'
  },
  clientsItemsText: {
    paddingTop: '5px',
  },
})

export default projectsProfileStyles
