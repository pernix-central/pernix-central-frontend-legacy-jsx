import {
  PERNIX_BLUE,
  PERNIX_GREEN,
  PERNIX_BLUE_HOVER,
  PERNIX_GREEN_HOVER,
  ERROR,
  ERROR_HOVER
} from './commonStyles'

const profileStyles = theme => ({
  root: {
    flexGrow: 1,
    [theme.breakpoints.down('xs')]: {
      margin: '3% 5%',
    }
  },
  mainContainer: {
    padding: '3%'
  },
  submitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER,
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%'
    }
  },
  cancelButton: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: ERROR,
    '&:hover': {
      background: ERROR_HOVER,
    }
  },
  paper: {
    padding: '10px 0px',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    boxShadow: 'none'
  },
  label: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
  labelsContainer: {
    display: 'flex',
    justifyContent: 'start',
    marginLeft: '5px',
  },
  attributeButton: {
    borderColorBottom: 'transparent'
  },
  infoContainer: {
    paddingLeft: '5%'
  },
  fullWith: {
    width: '100%',
  },
  scheduleContainer: {
    width: '100%',
    marginBottom: '80px',
  },
  card: {
    display: 'flex',
    boxShadow: 'none',
    padding: '0 5%',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    }
  },
  cardInfo: {
    display: 'flex',
    paddingLeft: '7%',
    boxShadow: 'none',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    }
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
    height: 151,
    marginLeft: '7%',
    [theme.breakpoints.down('xs')]: {
      width: 80,
      height: 80,
      margin: '0 auto',
    }
  },
  coverCircle: {
    width: 151,
    height: 151,
    marginLeft: '7%',
    borderRadius: '100%',
    [theme.breakpoints.down('xs')]: {
      width: 80,
      height: 80,
      margin: '0 auto',
    }
  },
  chip: {
    margin: 5,
  },
  active: {
    background: PERNIX_GREEN,
    color: 'white',
  },
  editBtn: {
    color: 'white',
    background: PERNIX_BLUE,
    '&:hover': {
      background: PERNIX_BLUE_HOVER,
    }
  },
  stylesLink: {
    textDecorration: 'none',
    color: 'white'
  },
  btnDanger: {
    color: 'white',
    fontWeight: 'bold',
    '&:hover': {
      background: '#B71233',
    }
  },
  profileActions: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  profileActionsUser: {
    float: 'right',
  },
  actionsMargin: {
    marginLeft: '10px',
    background: ERROR,
    color: 'white'
  },
  switchPosition: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: '10px 0',
  },
  teamMemberList: {
    margin: '10px 75px',
    [theme.breakpoints.down('xs')]: {
      margin: '0',
    }
  },
  cardActions: {
    justifyContent: 'space-evenly',
    width: '50%',
    marginLeft: '60%',
  },
  parentDiv: {
    width: '50%',
  },
  tabsRoot: {
    borderBottom: `1px solid #e8e8e8 ${PERNIX_BLUE_HOVER} `,
  },
  tabsIndicator: {
    backgroundColor: PERNIX_BLUE_HOVER,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing.unit * 4,
    fontFamilly: 'Monserrat',
    '&:hover': {
      color: PERNIX_BLUE,
      opacity: 1,
    },
    '&$tabSelected': {
      color: PERNIX_BLUE_HOVER,
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: PERNIX_BLUE,
    },
  },
  tabSelected: {},
  typography: {
    padding: theme.spacing.unit * 3,
  },
})

export default profileStyles
