const sideListStyles = {
  list: {
    width: '250px',
  },
  fullList: {
    width: 'auto',
  },
  sideBarItem: {
    color: 'gray',
    padding: '3% 3% 3% 8% ',
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none'
  },
  sidebarLabel: {
    fontSize: '16px',
    marginLeft: '4px',
    fontFamilly: 'Monserrat',
  },
  selected: {
    color: 'black',
  }
}

export default sideListStyles