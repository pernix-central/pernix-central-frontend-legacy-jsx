import { PERNIX_BLUE, PERNIX_GREEN, PERNIX_GREEN_HOVER, PERNIX_BLUE_HOVER } from "./commonStyles"
const activityStyles = theme => ({
  root: {
    flexGrow: 1,
    margin: '3% 5%',
    padding: '3%',
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      padding: '1%'
    }
  },
  paper: {
    padding: '10px 0px',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    boxShadow: 'none'
  },
  left: {
    textAlign: 'left',
    paddingLeft: '15%'
  },
  leftAlign: {
    textAlign: 'left',
  },
  infoContainer: {
    paddingLeft: '5%'
  },
  borderRight: {
    borderRight: 'solid gray',
  },
  tableContainer: {
    marginLeft: 19
  },
  card: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: '30%',
  },
  controls: {
    display: 'flex',
    alignItems: 'right',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  editBtn: {
    color: 'white',
    marginLeft: '10%',
    background: PERNIX_BLUE,
    '&:hover': {
      background: PERNIX_BLUE_HOVER,
    }
  },
  submitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    width: '48%',
    marginLeft: '1%',
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      width: '50%'
    }
  },
  secondTable: {
    marginTop: '74px',
  },
  buttons: {
    marginTop: '2%',
    marginLeft: '67%',
    marginRight: '4%',
  },
  tablesMargin: {
    justifyContent: 'center',
  },
  actions:{
    marginRight: '40px'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  newActivity: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  form: {
    marginBottom: '40px',
  }
})

export default activityStyles
