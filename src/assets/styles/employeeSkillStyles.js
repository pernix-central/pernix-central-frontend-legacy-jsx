import { PERNIX_GREEN, PERNIX_GREEN_HOVER, PERNIX_BLUE, PERNIX_BLUE_HOVER } from './commonStyles';

const employeeSkillStyles = theme => ({
  skillItem: {
    width: '100%'
  },
  formContainer: {
    height: '100%'
  },
  skillsContainer: {
    justifyContent: 'center',
    height: 'calc(100% - 125.4px)',
  },
  skillListContainer:{
    paddingTop: 5,
    justifyContent: 'center'
  },
  submitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    width: '49%',
    marginLeft: '1%',
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      width: '50%'
    }
  },
  resetButton: {
    backgroundColor: PERNIX_BLUE,
    color: 'white',
    fontWeight: 'bold',
    width: '49%',
    marginRight: '1%',
    '&:hover': {
      backgroundColor: PERNIX_BLUE_HOVER
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      width: '50%'
    }
  },
  buttons: {
    marginTop: '10px',
  },
})
export default employeeSkillStyles