import { PERNIX_GREEN } from "./commonStyles";

const scheduleStyles = theme => ({
  mainContainer:{
    height: '100%'
  },
  fullWith:{
    width: '100%',
  },
  periodInputs:{
    flexDirection: 'row',
    flexGrowth: 1,
    width: '100%',
    margin: '0px 5px'
  },
  colunmGrid:{
    marginRight: 33
  },
  periods:{
    marginRight: '5%',
    width: '80%',
  },
  noMinWidth:{
    width: '18%'
  },
  badge: {
    display: 'initial'
  },
  deletePeriod: {
    cursor: 'pointer',
    fontSize: 14
  },
  active: {
    background: PERNIX_GREEN,
    color: 'white',
  },
  scheduleContainer:{
    height: 'calc(100% - 125.4px)',
  }

})

export default scheduleStyles
