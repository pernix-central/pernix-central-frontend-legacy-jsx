const pernixFooterStyles = theme => ({
  footer: {
    height: '35px',
    bottom: '0',
    position: 'relative',
    paddingLeft: '15px',
    paddingTop: '5px',
    background: 'white',
    borderTop: '1px solid black',
  }
});

export default pernixFooterStyles
