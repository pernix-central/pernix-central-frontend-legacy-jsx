import { PERNIX_BLUE, PERNIX_GREEN, GOLD, SILVER, BRONCE } from "./commonStyles";
const LIGHT_GRAY = '#c7c7be'

const pagedTableStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    fontFamily: 'Montserrat'
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  dateTableCell: {
    width: '15%'
  },
  panel: {
    display: 'block',
    [theme.breakpoints.down('xs')]: {
      margin: 0,
      padding: 5
    }
  },
  button: {
    size: '0px'
  },
  leftAlign: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
  softSkill: {
    background: PERNIX_BLUE
  },
  techSkill: {
    background: PERNIX_GREEN
  },
  whiteAvatar: {
    background: 'white'
  },
  simpleTable: {
    height: 'calc(51vh)',
    overflowY: 'auto'
  },
  participantsTable: {
    height: 'calc(51vh)',
    overflowY: 'auto'
  },
  tableBorder: {
    borderStyle: 'solid',
    borderColor: LIGHT_GRAY,
  },
  scoreboardPoints: {
    width: '82%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scoreboardEmployeeInfo:{
    width: '16%'
  },
  scoreboardEmployeePosition: {
    width: '2%',
    textAlign: 'center',
    padding: '1%',
  },
  noPadding: {
    padding: 0
  },
  positionBorder:{
    borderLeftStyle: 'solid',
    borderLeftWidth: '14px',
  },
  position1: {
    borderLeftColor: GOLD,
  },
  position2: {
    borderLeftColor: SILVER,
  },
  position3: {
    borderLeftColor: BRONCE,
  },
  otherPositions: {
    borderLeftColor: 'white',
  },
})

export default pagedTableStyles
