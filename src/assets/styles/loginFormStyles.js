const pernixBlue = '#448ECC'
const loginFormStyle = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  centerRows: {
    textAlign: 'center'
  },
  formControl: {
    width: '80%',
  },
  card: {
    boxShadow: 'none',
    margin: '3% 32.5%',
    width: '35%',
    textAlign: 'center',
    [theme.breakpoints.down('md')]: {
      width: '90%',
      margin: '0 auto'
    }
  },
  cardContent: {
    padding: '2% 8%'
  },
  media: {
    width: '50%',
    margin: '5% 25%',
    [theme.breakpoints.down('xs')]: {
      width: '70%',
      margin: '5% 15%'
    }

  },
  forgotPassword: {
    textDecoration: 'underline',
    color: pernixBlue,
    fontWeight: 'bold',
    margin: '5%',
    display: 'block'
  },
  loginButton: {
    border: '1px solid',
    boxShadow: '5px 5px ',
    color: 'gray'
  }
});

export default loginFormStyle
