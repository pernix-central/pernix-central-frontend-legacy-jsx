const sidebarStyles = {
  image: {
    width: '80%',
    height: '20%',
    padding: '8.5%',
    border: 'gray solid '
  },
  icon: {
    marginRight: '15%'
  },
  sideBarHeader: {
    marginTop: '10%',
    borderBottom: 'solid gray'
  },
  sticky: {
    position: 'fixed',
    top: 0,
    width: '100%'
  },
  avatar: {
    width: '50px',
    height: '50px'
  }
};

export default sidebarStyles
