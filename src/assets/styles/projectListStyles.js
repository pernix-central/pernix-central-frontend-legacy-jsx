import { PERNIX_BLUE, PERNIX_GREEN } from "./commonStyles";

const LIGHT_GRAY = '#E5E5DB'

const projectListStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  newProject: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: '10px'
  },
  paper: {
    marginTop: '20px',
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  dateTableCell: {
    width: '15%'
  },
  panel: {
    display: 'block',
    [theme.breakpoints.down('xs')]: {
      margin: 0,
      padding: 5
    }
  },
  button: {
    size: '0px'
  },
  leftAlign: {
    textAlign: 'left',
    padding: 5,
    marginTop: 2,
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    }
  },
  softSkill: {
    background: PERNIX_BLUE
  },
  techSkill: {
    background: PERNIX_GREEN
  },
  whiteAvatar: {
    background: 'white'
  },
  simpleTable: {
    height: '250px',
    overflowY: 'auto'
  },
  tableBorder: {
    borderStyle: 'solid',
    borderColor: LIGHT_GRAY,
  }
})

export default projectListStyles
