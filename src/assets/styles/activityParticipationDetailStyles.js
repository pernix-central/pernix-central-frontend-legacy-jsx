const activityParticipationDetailStyles = theme => ({
  detailContainer: {
    display: 'inline-flex',
    width: '64%',
  },
  activityName:{
    width: '20%',
  },
  dottedSparatorLine:{
    width: '70%',
    borderStyle: 'none none dotted none',
  },
  activityPoints:{
    width: '10%',
    textAlign: 'right'
  }
})

export default activityParticipationDetailStyles