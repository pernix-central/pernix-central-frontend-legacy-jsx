import { PERNIX_BLUE, PERNIX_GREEN, PERNIX_GREEN_HOVER, PERNIX_BLUE_HOVER } from "./commonStyles"
const actionsButtonsStyle = theme => ({
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    float: 'right',
    width: '64%',
    margin: '30px 0'
  },
  action:{
    color: 'white',
    width: '30%',
    margin: '0 10px',
    borderStyle: 'none none dotted none',
  },
  back: {
    backgroundColor: PERNIX_BLUE,
    '&:hover': {
      backgroundColor: PERNIX_BLUE_HOVER
    },
  },
  submit: {
    backgroundColor: PERNIX_GREEN,
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER
    },
  }
})

export default actionsButtonsStyle
