import { PERNIX_GREEN, PERNIX_GREEN_HOVER } from "./commonStyles"

const formStyles = theme => ({
  marginNormal: {
    marginTop: '2%',
    marginLeft: '4%',
  },
  imagePreview: {
    margin: '10% 10% 0 10%',
    height : '80%',
    width: '80%',
    borderRadius: '100%'
  },
  marginTable: {
    marginTop: '5%',
  },
  arrowsMargin: {
    marginTop: '5%',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '90%',
  },
  fullWidthInput: {
    width: '100%',
  },
  submitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    width: '49%',
    marginLeft: '1%',
    '&:hover': {
      backgroundColor: PERNIX_GREEN,
    },
    [theme.breakpoints.down('sm')]: {
      margin: '1%',
      width: '50%',
    }
  },
  topSubmitButton: {
    backgroundColor: PERNIX_GREEN,
    color: 'white',
    fontWeight: 'bold',
    width: '50%',
    marginTop: '1%',
    marginLeft: '31%',
    '&:hover': {
      backgroundColor: PERNIX_GREEN_HOVER,
    },
  },
  checkbox: {
    color: PERNIX_GREEN,
  },
  buttons: {
    width: '50%',
    marginLeft: '45%',
    marginTop: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      margin: '5%',
    }
  },
  teamFormButtons: {
    width: '40%',
    marginLeft: '66%',
    marginTop: '3%',
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      margin: '5%',
    }
  },
  button: {
    width: '50%',
    marginLeft: '25%',
    marginTop: '5%',
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      margin: '5%',
    }
  },
  formActions: {
    marginRight: '7%',
    marginTop: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      margin: '0 auto',
    }
  },
  formActionsRight:{
    display: 'flex',
    width: '44%',
    marginLeft: '56%',
    marginTop: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      margin: 0,
    }
  },
  formContainer: {
    height: '100%',
  },
  alignCenter: {
    textAlign: 'center',
  },
  formControl: {
    margin: theme.spacing.unit,
    width: '90%',
  },
  container: {
    height: 'calc(100% - 66.4px)',
  },
  inputContainer: {
    height: 'calc(100% - 58px)',
  },
  blockFormContainer: {
    display: 'block',
  },
  flexFormContainer: {
    display: 'flex',
  },
  searchMembers: {
    width: '100%',
  },
  toggleMembers: {
    height: 'calc(100% - 249.4px)'
  },
  fileChooser:{
    display: 'flex',
    justifyContent: 'start',
    marginTop: 25
  },
  mleft :{
    margin: '0 4px'
  },
  secondTable: {
    marginTop: '5%',
  },
});

export default formStyles
