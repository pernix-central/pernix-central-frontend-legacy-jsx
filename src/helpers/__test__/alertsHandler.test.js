import { messageHandler, WARNING, SUCCESS, ERROR }  from '../alertsHandler'

function getMockMessage(title,message,variant) {
  return {
    title,
    message,
    variant
  };
}

describe('Test suite for server request message handler', () => {

  it('When the user send create employee request and server returns 200', () => {
    expect(messageHandler(200, { model: 'employee', action: 'create'}))
    .toEqual(getMockMessage(
      'Success',
      `The employee has been create successfuly`,
      SUCCESS
    ));
  });

  it('When the user send create employee request and server returns 422', () => {
    expect(messageHandler(422,{ model: 'employee', action: 'create'}))
    .toEqual(getMockMessage(
      'Session expired',
      `You need to refresh your access token to be able to create a employee successfuly`,
      WARNING
    ));
  });

  it('When the user send create employee request and server returns 500', () => {
    expect(messageHandler(500,{ model: 'employee', action: 'create'}))
    .toEqual(getMockMessage(
      'Error',
      `An error has ocurr the create employee can't be complete`,
      ERROR
    ));
  });
});
