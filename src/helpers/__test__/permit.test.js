import permit from '../permit';

describe('Permit fuction test for admin users', () => {

  var mockUser = getMockUserWithRole('Admin');

  it('Admin user request permition', () => {
    ['Employee', 'Project', 'Scoreboard'].forEach(route => {
      expect(permit(mockUser, route, 'index')).toEqual(true)
    });
  })
});

describe('Permit fuction test for crafter users', () => {

  var mockUser = getMockUserWithRole('Crafter');

  it('Crafter user request permition to Employe', () => {
    expect(permit(mockUser, 'Employee', 'index')).toEqual(false);
  });

  it('Crafter user request permition to Project', () => {
    expect(permit(mockUser, 'Project', 'index')).toEqual(true);
  });

  it('Crafter user request permition to Scoreboard', () => {
    expect(permit(mockUser, 'Scoreboard', 'index')).toEqual(true);
  });
});

describe('Permit fuction test for apprentice users', () => {

  var mockUser = getMockUserWithRole('Apprentice');

  it('Apprentice user request permition to Employe', () => {
    expect(permit(mockUser, 'Employee', 'index')).toEqual(false);
  });

  it('Apprentice user request permition to Project', () => {
    expect(permit(mockUser, 'Project', 'index')).toEqual(false);
  });

  it('Apprentice user request permition to Scoreboard', () => {
    expect(permit(mockUser, 'Scoreboard', 'index')).toEqual(true);
  });
});

function getMockUserWithRole(role) {
  return {
    name: 'brad',
    email: 'brad@gamail.com',
    role
  };
}