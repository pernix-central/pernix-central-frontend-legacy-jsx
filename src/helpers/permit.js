const permissions = {
  Employee: {
    index: ['admin'],
    create: ['admin'],
    update: ['admin'],
    show: ['admin']
  },
  Scoreboard: {
    index: ['admin', 'crafter', 'apprentice']
  },
  Project: {
    index: ['admin', 'crafter'],
    create: ['admin'],
    update: ['admin'],
    show: ['admin', 'mentor', 'crafter']
  },
  Team: {
    index: ['admin', 'crafter'],
    create: ['admin'],
    show: ['admin', 'crafter'],
    update: ['admin', 'crafter']
  },
  Skill: {
    index: ['admin', 'crafter'],
    create: ['admin'],
    show: ['admin', 'crafter'],
    update: ['admin', 'crafter']
  },
  Client: {
    index: ['admin', 'crafter'],
    create: ['admin'],
    show: ['admin', 'crafter'],
    update: ['admin', 'crafter']
  },
  Activity: {
    index: ['admin', 'crafter'],
    create: ['admin'],
    show: ['admin', 'crafter'],
    update: ['admin', 'crafter']
  }
};

const permit = (user, model, action) => {
  if (!user) {
    return false;
  }
  return permissions[model] && permissions[model][action]
    && permissions[model][action].includes(user.role.toLowerCase())
}

export default permit

