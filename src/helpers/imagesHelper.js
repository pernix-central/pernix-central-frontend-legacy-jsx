export const getImage = (images) => {
  if (images) {
    let imagesSize = images.length;
    return imagesSize > 0 ? images[0].square : null
  }
  return null
}

export const getFirstImage = (images) => {
  if (images) {
    let imagesSize = images.length
    return imagesSize > 0 ? images[0].square : null
  }
  return null
}

export const insertPayloadInform = (employee, isEditable) => {
  const { firstName, lastName, birthdate, email, password, passwordConfirmation, hasCar, blogUrl, canBeMentor, onVacation, computerSerialNumber, macAddress, hireDate, role, imagesAttributes } = employee
  var form = new FormData()
  form.append('employee[first_name]', firstName)
  form.append('employee[last_name]', lastName)
  form.append('employee[birthdate]', birthdate)
  form.append('employee[email]', email)
  if (!isEditable) {
    form.append('employee[password]', password)
    form.append('employee[password_confirmation]', passwordConfirmation)
  }
  form.append('employee[first_name]', firstName)
  form.append('employee[has_car]', hasCar)
  form.append('employee[blog_url]', blogUrl)
  form.append('employee[can_be_mentor]', canBeMentor)
  form.append('employee[on_vacation]', onVacation)
  form.append('employee[computer_serial_number]', computerSerialNumber)
  form.append('employee[mac_address]', macAddress)
  form.append('employee[hire_date]', hireDate)
  form.append('employee[role]', role)
  if (imagesAttributes) {
    form.append('employee[images_attributes][0][attachment]', imagesAttributes)
  }
  return form
}
