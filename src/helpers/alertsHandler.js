export const WARNING = 'warning'
export const SUCCESS = 'success'
export const ERROR = 'error'
export const INFO = 'info'

export const messageHandler = (status, { model, action }) => {
  let title, message, variant = ''
  switch (status) {
    case 200:
      title = 'Success'
      message = `The ${model} has been ${action} successfuly`
      variant = SUCCESS
      break
    case 201:
      title = 'Success'
      message = `The ${model} has been ${action} successfuly`
      variant = SUCCESS
      break
    case 422:
      title = 'Session expired'
      message = `You need to log in again to continue`
      variant = WARNING
      break
    case 500:
      title = 'Error'
      message = `An error has occurred the ${action} ${model} can't be complete`
      variant = ERROR
      break
    default:
      title = 'Error'
      message = `An error has occurred the ${action} ${model} can't be complete`
      variant = ERROR
      break
  }
  return {
    title,
    message,
    variant
  }
}
