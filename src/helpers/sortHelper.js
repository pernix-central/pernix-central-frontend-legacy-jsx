const ASCENDIENT_ORDER = 'asc'
const DESCENDIENT_ORDER = 'desc'

const compareValues = (a, b, orderBy) => {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

export const getSortingOrder = (order, orderBy) => {
  return order === DESCENDIENT_ORDER  ? (a, b) => compareValues(a, b, orderBy) : (a, b) => -compareValues(a, b, orderBy)
}

export const handleOrderDirection = (currentOrder,currentProperty,incomingProperty) => {
  const orderByProperty = incomingProperty
  let orderDirection = DESCENDIENT_ORDER
  if (currentProperty === incomingProperty && currentOrder === orderDirection) {
    orderDirection = ASCENDIENT_ORDER
  }
  return { orderDirection, orderByProperty }
}
