export const subtractLists = (list1, list2) => {
  let l2 = JSON.parse(JSON.stringify(list2));
  let index = 0;
  list1.forEach(list1Element => {
    index = l2.map(list2Element => list2Element.id).indexOf(list1Element.id);
    if (index > -1) {
      l2.splice(index, 1)
    }
  });
  return l2;
}

export const filterEmployeesBySkills = (employeeList, skillFilters) => {
  var results = []
  employeeList.forEach(employee => {
    const { tcpSkills } = employee
    var haveAllSkills = true
    skillFilters.forEach(skill => {
      if (!validateIfEmployeeHaveSkill(tcpSkills, skill.id)){
        haveAllSkills = false
      } 
    })
    if (haveAllSkills){
      results.push(employee)
    }
  })
  return results
}

const validateIfEmployeeHaveSkill = (tcpSkills, skillId) => {
  var flag = false
  tcpSkills.forEach( skill => {
    if(skill.skillId === skillId){
      flag = true
    }
   })
  return flag
}

export const searchSkill = (filter, list) => {
  return list.filter( skill => skill.name.toLowerCase().includes(filter.toLowerCase()))
}

export const subtractSkillLists = (employeeSkills, availableSkills) => {
  let l2 = JSON.parse(JSON.stringify(availableSkills));
  let index = 0;
  employeeSkills.forEach(employeeSkill => {
    index = l2.map(availableSkill => availableSkill.id).indexOf(employeeSkill.skill.id);
    if (index > -1) {
      l2.splice(index, 1)
    }
  });
  return l2;
}

