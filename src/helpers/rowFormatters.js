export const formatTcpSkill = (tcpSkill,) => {
  const { level, skill: { id, skillType, name } } = tcpSkill
  var object = {
    id: id,
    avatar: level,
    tcpSkillId: tcpSkill.id,
    textPrimary: name,
    textSecondary: skillType,
  }
  return object
}

export const formatTcpSkillWithoutAction = (tcpSkill) => {
  const { level, skill: { id, skillType, name } } = tcpSkill
  var object = {
    id: id,
    avatar: level,
    tcpSkillId: null,
    textPrimary: name,
    textSecondary: skillType,
  }
  return object
}

export const formatSkill = (skill) => {
  const { id, name, skillType } = skill
  var object = {
    id: id,
    tcpSkillId: null,
    avatar: skillType[0].toUpperCase(),
    textPrimary: name,
    textSecondary: skillType
  }
  return object
}