import React from 'react'
import { connect } from 'react-redux'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import HomePage from '../components/pages/HomePage'
import LoginPage from '../components/pages/LoginPage'
import Employees from '../components/pages/Employees'
import Scoreboard from '../components/pages/Scoreboard'
import permit from '../helpers/permit'
import RegisterUserPage from '../components/pages/RegisterUserPage'
import PrivateRoute from "./PrivateRoute"
import ProjectProfile from '../components/pages/ProjectProfile'
import Projects from '../components/pages/Projects'
import EditEmployee from '../components/pages/EditEmployee'
import CreateProjectPage from '../components/pages/CreateProjectPage'
import ShowEmployeePage from '../components/pages/ShowEmployeePage'
import EditProjectPage from '../components/pages/EditProjectPage'
import CreateTeamPage from '../components/pages/CreateTeamPage'
import EditTeamPage from '../components/pages/EditTeamPage'
import ShowTeamPage from '../components/pages/ShowTeamPage'
import Teams from '../components/pages/Teams'
import CreateSkillPage from '../components/pages/CreateSkillPage'
import Skills from '../components/pages/Skills'
import EditSkillPage from '../components/pages/EditSkillPage'
import Clients from '../components/pages/Clients'
import CreateClientPage from '../components/pages/CreateClientPage'
import EditClientPage from '../components/pages/EditClientPage'
import ShowClientPage from '../components/pages/ShowClientPage'
import Activities from '../components/pages/Activities'
import CreateActivityPage from '../components/pages/CreateActivityPage'
import EditActivityPage from '../components/pages/EditActivityPage'
import ShowActivityPage from '../components/pages/ShowActivityPage'

class ApplicationRouter extends React.Component {

  isUserEditingOwnProfile = () =>{
    const { match: { params: { employeeId } }, user} = this.props;
    return employeeId === user.id
  }

  render() {
    const { user, location: { pathname } } = this.props
    return (
      <Switch>
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/signup' component={RegisterUserPage} />
        <PrivateRoute exact path='/' component={HomePage} authenticated={!!user} />
        {permit(user, "Employee", "index") && <Route exact path="/employees" component={Employees} authenticated={!!user} />}
        {permit(user, "Employee", "show") && <Route exact path="/employees/:employeeId" component={ShowEmployeePage} authenticated={!!user} />}
        {(permit(user, "Employee", "update")||this.isUserEditingOwnProfile) && <Route path="/employees/:employeeId/update" component={EditEmployee} />}
        {permit(user, "Scoreboard", "index") && <Route exact path="/scoreboard/presentation" component={Scoreboard} authenticated={!!user} />}
        {permit(user, "Scoreboard", "index") && <Route exact path="/scoreboard/list" component={Scoreboard} authenticated={!!user} />}
        {permit(user, "Project", "index") && <Route exact path="/projects" component={Projects} authenticated={!!user} />}
        {permit(user, "Project", "create") && <Route exact path="/projects/create" component={CreateProjectPage} authenticated={!!user} />}
        {permit(user, "Project", "show") && <Route exact path="/projects/:projectId" component={ProjectProfile} authenticated={!!user} />}
        {permit(user, "Project", "update") && <Route exact path="/projects/:projectId/update" component={EditProjectPage} authenticated={!!user} />}
        {permit(user, "Team", "index") && <Route exact path="/teams" component={Teams} authenticated={!!user} />}
        {permit(user, "Team", "create") && <Route exact path='/teams/create' component={CreateTeamPage} authenticated={!!user} />}
        {permit(user, "Team", "show") && <Route exact path="/teams/:teamId" component={ShowTeamPage} authenticated={!!user} />}
        {permit(user, "Team", "update") && <Route exact path='/teams/:teamId/update' component={EditTeamPage} authenticated={!!user} />}
        {permit(user, "Skill", "index") && <Route exact path="/skills" component={Skills} authenticated={!!user} />}
        {permit(user, "Skill", "create") && <Route exact path="/skills/create" component={CreateSkillPage} authenticated={!!user} />}
        {permit(user, "Skill", "update") && <Route exact path="/skills/:skillId/update" component={EditSkillPage} authenticated={!!user} />}
        {permit(user, "Client", "index") && <Route exact path="/clients" component={Clients} authenticated={!!user}/> }
        {permit(user, "Client", "create") && <Route exact path="/clients/create" component={CreateClientPage} authenticated={!!user}/> }
        {permit(user, "Client", "update") && <Route exact path="/clients/:clientId/update" component={EditClientPage} authenticated={!!user}/> }
        {permit(user, "Client", "show") && <Route exact path="/clients/:clientId" component={ShowClientPage} authenticated={!!user}/> }
        {permit(user, "Activity", "index") && <Route exact path="/activities" component={Activities} authenticated={!!user}/> }
        {permit(user, "Activity", "create") && <Route exact path="/activities/create" component={CreateActivityPage} authenticated={!!user}/> }
        {permit(user, "Activity", "update") && <Route exact path="/activities/:activityId/update" component={EditActivityPage} authenticated={!!user}/> }
        {permit(user, "Activity", "show") && <Route exact path="/activities/:activityId" component={ShowActivityPage} authenticated={!!user}/> }
        <Redirect from={pathname} exact to="/" />
      </Switch>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
  }
}

export default withRouter(connect(mapStateToProps)(ApplicationRouter))
